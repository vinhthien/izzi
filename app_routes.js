


exports.applyRoutes = function (app) {

    app.use('/cap-nhat-slug', require('./routes/cap-nhat-slug'));
    app.use('/', require('./routes/index'));
    app.use('/gioi-thieu', require('./routes/gioi-thieu'));
    app.use('/san-pham', require('./routes/san-pham'));
    app.use('/chinh-sach', require('./routes/chinh-sach'));
    app.use('/he-thong-nha-phan-phoi', require('./routes/he-thong-nha-phan-phoi'));
    app.use('/khuyen-mai', require('./routes/khuyen-mai'));
    app.use('/cam-nang-mon-ngon', require('./routes/cam-nang-mon-ngon'));
    app.use('/tin-tuc-su-kien', require('./routes/tin-tuc-su-kien'));
    app.use('/lien-he', require('./routes/lien-he'));
    app.use('/danh-muc', require('./routes/danh-muc'));
    app.use('/chi-tiet-san-pham', require('./routes/chi-tiet-san-pham'));
    app.use('/gio-hang', require('./routes/gio-hang'));
    app.use('/chon-dia-chi-giao-hang', require('./routes/chon-dia-chi-giao-hang'));
    app.use('/chon-phuong-thuc-thanh-toan', require('./routes/chon-phuong-thuc-thanh-toan'));
    app.use('/thanh-toan-qua-the', require('./routes/thanh-toan-qua-the'));
    app.use('/thong-tin-tai-khoan', require('./routes/thong-tin-tai-khoan'));
    app.use('/so-dia-chi', require('./routes/so-dia-chi'));
    app.use('/lich-su-mua-hang', require('./routes/lich-su-mua-hang'));
    app.use('/thay-doi-so-dia-chi', require('./routes/thay-doi-so-dia-chi'));
    app.use('/san-pham-moi', require('./routes/san-pham-moi'));
    app.use('/chi-tiet-mon-an-ngon', require('./routes/chi-tiet-mon-an-ngon'));
    app.use('/chi-tiet-tin-tuc', require('./routes/chi-tiet-tin-tuc'));
    app.use('/chi-tiet-lich-su-don-hang', require('./routes/chi-tiet-lich-su-mua-hang'));
    app.use('/ket-qua-tim-kiem', require('./routes/ket-qua-tim-kiem'));

    app.use('/admin/utility', require('./routes_backend/utility'));

    app.use('/admin', require('./routes_backend/utility'));
    app.use('/admin/login', require('./routes_backend/login'));
    app.use('/admin/resetPasswordUser', require('./routes_backend/resetPasswordUser'));


    app.use('/admin/danh-sach-tin-tuc-su-kien', require('./routes_backend/danh-sach-tin-tuc-su-kien'));
    app.use('/admin/them-tin-tuc-su-kien', require('./routes_backend/them-tin-tuc-su-kien'));
    app.use('/admin/sua-tin-tuc-su-kien', require('./routes_backend/sua-tin-tuc-su-kien'));
    app.use('/admin/chinh-sua-banner-tin-tuc-su-kien', require('./routes_backend/chinh-sua-banner-tin-tuc-su-kien'));
    app.use('/admin/quan-ly-meta-tin-tuc-su-kien', require('./routes_backend/quan-ly-meta-tin-tuc-su-kien'));
    app.use('/admin/quan-ly-bai-viet-tin-tuc-su-kien', require('./routes_backend/quan-ly-bai-viet-tin-tuc-su-kien'));


    app.use('/admin/danh-sach-mon-an-ngon', require('./routes_backend/danh-sach-mon-an-ngon'));
    app.use('/admin/them-mon-an-ngon', require('./routes_backend/them-mon-an-ngon'));
    app.use('/admin/sua-mon-an-ngon', require('./routes_backend/sua-mon-an-ngon'));
    app.use('/admin/danh-sach-chinh-sach-ban-hang', require('./routes_backend/danh-sach-chinh-sach-ban-hang'));
    app.use('/admin/them-chinh-sach-ban-hang', require('./routes_backend/them-chinh-sach-ban-hang'));
    app.use('/admin/sua-chinh-sach-ban-hang', require('./routes_backend/sua-chinh-sach-ban-hang'));
    app.use('/admin/danh-sach-san-pham', require('./routes_backend/danh-sach-san-pham'));
    app.use('/admin/danh-sach-san-pham-don-vi', require('./routes_backend/danh-sach-san-pham-don-vi'));
    app.use('/admin/danh-sach-san-pham-combo', require('./routes_backend/danh-sach-san-pham-combo'));
    app.use('/admin/danh-sach-danh-muc-san-pham', require('./routes_backend/danh-sach-danh-muc-san-pham'));
    app.use('/admin/them-san-pham', require('./routes_backend/them-san-pham'));
    app.use('/admin/them-san-pham-don-vi', require('./routes_backend/them-san-pham-don-vi'));
    app.use('/admin/them-san-pham-combo', require('./routes_backend/them-san-pham-combo'));
    app.use('/admin/sua-san-pham', require('./routes_backend/sua-san-pham'));
    app.use('/admin/sua-san-pham-don-vi', require('./routes_backend/sua-san-pham-don-vi'));
    app.use('/admin/sua-san-pham-combo', require('./routes_backend/sua-san-pham-combo'));
    app.use('/admin/sua-chinh-sach-ban-hang', require('./routes_backend/sua-chinh-sach-ban-hang'));
    app.use('/admin/sua-danh-muc-san-pham', require('./routes_backend/sua-danh-muc-san-pham'));
    app.use('/admin/danh-sach-slider-home', require('./routes_backend/danh-sach-slider-home'));
    app.use('/admin/them-slider-home', require('./routes_backend/them-slider-home'));
    app.use('/admin/sua-slider-home', require('./routes_backend/sua-slider-home'));
    app.use('/admin/danh-sach-slider-mon-an-ngon', require('./routes_backend/danh-sach-slider-mon-an-ngon'));
    app.use('/admin/them-slider-mon-an-ngon', require('./routes_backend/them-slider-mon-an-ngon'));
    app.use('/admin/sua-slider-mon-an-ngon', require('./routes_backend/sua-slider-mon-an-ngon'));
    app.use('/admin/danh-sach-chuong-trinh-khuyen-mai', require('./routes_backend/danh-sach-chuong-trinh-khuyen-mai'));
    app.use('/admin/danh-sach-thuong-hieu', require('./routes_backend/danh-sach-thuong-hieu'));
    app.use('/admin/them-chuong-trinh-khuyen-mai', require('./routes_backend/them-chuong-trinh-khuyen-mai'));
    app.use('/admin/them-thuong-hieu', require('./routes_backend/them-thuong-hieu'));
    app.use('/admin/sua-chuong-trinh-khuyen-mai', require('./routes_backend/sua-chuong-trinh-khuyen-mai'));
    app.use('/admin/sua-thuong-hieu', require('./routes_backend/sua-thuong-hieu'));

    app.use('/admin/gioi-thieu', require('./routes_backend/gioi-thieu'));
    app.use('/admin/insert-script-tag', require('./routes_backend/insert-script-tag'));
    app.use('/admin/cong-ty-co-phan-opera', require('./routes_backend/cong-ty-co-phan-opera'));
    app.use('/admin/chinh-sach-chat-luong', require('./routes_backend/chinh-sach-chat-luong'));
    app.use('/admin/su-menh', require('./routes_backend/su-menh'));
    app.use('/admin/tam-nhin', require('./routes_backend/tam-nhin'));
    app.use('/admin/them-danh-muc-san-pham', require('./routes_backend/them-danh-muc-san-pham'));
    app.use('/admin/sua-slider-mon-an-ngon', require('./routes_backend/sua-slider-mon-an-ngon'));
    app.use('/admin/sua-slider-home', require('./routes_backend/sua-slider-home'));
    app.use('/admin/chinh-sua-banner-san-pham', require('./routes_backend/chinh-sua-banner-san-pham'));
    app.use('/admin/chinh-sua-banner-chi-nhanh', require('./routes_backend/chinh-sua-banner-chi-nhanh'));
    app.use('/admin/chinh-sua-banner-khuyen-mai', require('./routes_backend/chinh-sua-banner-khuyen-mai'));
    app.use('/admin/chinh-sua-banner-mon-an-ngon', require('./routes_backend/chinh-sua-banner-mon-an-ngon'));
    app.use('/admin/danh-sach-lien-he', require('./routes_backend/danh-sach-lien-he'));
    app.use('/admin/chinh-sua-lien-he', require('./routes_backend/chinh-sua-lien-he'));
    app.use('/admin/notification', require('./routes_backend/notification'));
    app.use('/admin/them-chi-nhanh-cua-hang', require('./routes_backend/them-chi-nhanh-cua-hang'));
    app.use('/admin/sua-chi-nhanh-cua-hang', require('./routes_backend/sua-chi-nhanh-cua-hang'));
    app.use('/admin/danh-sach-chi-nhanh-cua-hang', require('./routes_backend/danh-sach-chi-nhanh-cua-hang'));
    app.use('/admin/danh-sach-nhap-kho', require('./routes_backend/danh-sach-nhap-kho'));
    app.use('/admin/thong-tin-lien-lac-cong-ty', require('./routes_backend/quan-ly'));
    app.use('/admin/danh-sach-don-hang', require('./routes_backend/danh-sach-don-hang'));
    app.use('/admin/quan-ly-san-pham-ban-chay', require('./routes_backend/quan-ly-san-pham-ban-chay'));
    app.use('/admin/quan-ly-san-pham-qua-tang', require('./routes_backend/quan-ly-san-pham-qua-tang'));
    app.use('/admin/quan-ly-meta-trang-chu', require('./routes_backend/quan-ly-meta-trang-chu'));
    app.use('/admin/quan-ly-meta-tong-quan-san-pham', require('./routes_backend/quan-ly-meta-tong-quan-san-pham'));
    app.use('/admin/quan-ly-meta-tong-quan-mon-ngon', require('./routes_backend/quan-ly-meta-tong-quan-mon-ngon'));
    app.use('/admin/quan-ly-meta-tong-quan-khuyen-mai', require('./routes_backend/quan-ly-meta-tong-quan-khuyen-mai'));
    app.use('/admin/quan-ly-bai-viet-mon-an-ngon', require('./routes_backend/quan-ly-bai-viet-mon-an-ngon'));
    app.use('/admin/quan-ly-toc-do-slider-trang-chu', require('./routes_backend/quan-ly-toc-do-slider-trang-chu'));
    app.use('/admin/quan-ly-toc-do-slider-mon-an-ngon', require('./routes_backend/quan-ly-toc-do-slider-mon-an-ngon'));
    app.use('/admin/quan-ly-toc-do-slider-tin-tuc', require('./routes_backend/quan-ly-toc-do-slider-tin-tuc'));
    app.use('/admin/tao-coupon', require('./routes_backend/tao-coupon'));
    app.use('/admin/danh-sach-chuong-trinh-coupon', require('./routes_backend/danh-sach-chuong-trinh-coupon'));
    app.use('/admin/danh-sach-coupon', require('./routes_backend/danh-sach-coupon'));
    app.use('/admin/sua-chuong-trinh-coupon', require('./routes_backend/sua-chuong-trinh-coupon'));
    app.use('/admin/upload-hinh', require('./routes_backend/upload-hinh'));
    app.use('/admin/quan-ly-thong-tin-truyen-thong', require('./routes_backend/quan-ly-thong-tin-truyen-thong'));
    app.use('/admin/quan-ly-lien-he-footer', require('./routes_backend/quan-ly-lien-he-footer'));
    app.use('/admin/quan-ly-danh-muc-cap-2-footer', require('./routes_backend/quan-ly-danh-muc-cap-2-footer'));
    app.use('/admin/quan-ly-san-pham-khuyen-mai', require('./routes_backend/quan-ly-san-pham-khuyen-mai'));
    app.use('/admin/quan-ly-popup-khuyen-mai', require('./routes_backend/quan-ly-popup-khuyen-mai'));
    app.use('/admin/soan-thao-email', require('./routes_backend/soan-thao-email'));
    app.use('/admin/soan-thao-email-xac-nhan-dat-hang', require('./routes_backend/soan-thao-email-xac-nhan-dat-hang'));
    app.use('/admin/bao-cao-ban-hang', require('./routes_backend/bao-cao-ban-hang'));
    app.use('/admin/cau-hinh-email', require('./routes_backend/cau-hinh-email'));
    app.use('/admin/test', require('./routes_backend/test'));
    app.use('/admin/thong-ke-ban-hang', require('./routes_backend/thong-ke-ban-hang'));
    app.use('/admin/bao-cao-doanh-so-theo-nganh-hang', require('./routes_backend/bao-cao-doanh-so-theo-nganh-hang'));
    app.use('/admin/thong-ke-ban-hang-chi-tiet', require('./routes_backend/thong-ke-ban-hang-chi-tiet'));
    app.use('/admin/them-user-admin', require('./routes_backend/them-user-admin'));
    app.use('/admin/danh-sach-user-admin', require('./routes_backend/danh-sach-user-admin'));
    app.use('/admin/sua-user-admin', require('./routes_backend/sua-user-admin'));
    app.use('/admin/bao-cao-ban-hang-day-du', require('./routes_backend/bao-cao-ban-hang-day-du'));
    app.use('/admin/them-quyen', require('./routes_backend/them-quyen'));
    app.use('/admin/sua-quyen', require('./routes_backend/sua-quyen'));
    app.use('/admin/danh-sach-quyen', require('./routes_backend/danh-sach-quyen'));
    app.use('/admin/danh-sach-khach-hang-than-thiet', require('./routes_backend/danh-sach-khach-hang-than-thiet'));
    app.use('/admin/doi-mat-khau', require('./routes_backend/doi-mat-khau'));
    app.use('/admin/insert-ship-time', require('./routes_backend/insert-ship-time'));


}

