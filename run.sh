#!/bin/sh

forever stop opera-mart
rm -rf ~/.forever/opera-mart.log.previous
mv ~/.forever/opera-mart.log opera-mart.log.previous
forever --uid opera-mart -o out.server -e error.server start app.js
forever list
tail -f ~/.forever/opera-mart.log