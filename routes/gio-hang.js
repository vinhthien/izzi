var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');

var order_process = require('../businesses/order_process');

router.get('/', function(req, res, next) {
    res.render('gio-hang', {
        i18n: res
    });

});

router.post('/removeCart', function(req, res, next) {

    var product_id = req.body.product_id;
    if (req.session.cart == null) {
        req.session.cart = [];
    }

    for (var i = req.session.cart.length - 1; i >= 0 ; i--) {
        if (req.session.cart[i].product_id == product_id) {
            req.session.cart.splice(i, 1);
            res.json(api.getResponse(api.SUCC_EXEC, {
                product_length: req.session.cart.length
            }, ""));
            return;
        }
    }

});

module.exports = router;
