var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var moment = require('moment');
var api = require('../api/response');


router.get('/', function(req, res, next) {
    if (req.session.enduser_user_id == null) {
        res.redirect("san-pham");
        return;
    }
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;
    var currentTime = new Date();
    var current_year = currentTime.getFullYear();
    db.getPersonById(user_id,function (error, person) {
        if(error){
            return;
        }
        var day = "1";
        var month = "1";
        var year = "1999";
        if(person && person[0] && person[0].birthday){
            var date = api.revertDate(person[0].birthday);
            date = moment(date, 'dd/MM/YYYY').format('l');
            date = date.split("/");
            var day = date[1];
            var month = date[0];
            var year = date[2];
        }
        var dateObject = {
          day: day,
          month: month,
          year: year
        };
        res.render("thong-tin-tai-khoan", {
            i18n: res,
            person: person[0],
            current_year: current_year,
            dateObject: dateObject
        });
    });
});

router.post('/update', function(req, res, next) {
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;
    var obj = req.body;
    var new_username = obj.username;
    var email = obj.email;
    var phoneNumber =  obj.phoneNumber;
    var gender = obj.gender;
    var day = obj.day;
    var month = obj.month;
    var year = obj.year;
    var date = month+"/"+day+"/"+year;
    var formatted_date = moment(date, 'M/D/YYYY').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
    var updated_at = api.getNow();
    db.getPersonById(user_id,function (error, person) {
        if(error){
            return;
        }
        db.updatePerson(null,new_username,null,formatted_date,
            person[0].points,phoneNumber,gender,person[0].email,
            updated_at,user_id,person[0].entity_code,1,user_id,function (error1,a) {
                if(error1){
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, "", ""));

            })
    })


});

module.exports = router;
