var express = require('express');
var router = express.Router();
var db = require('../databases/database');


router.get('/', function(req, res, next) {
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;
    var order_id = req.query.id;

    db.getPersonById(user_id,function (error, person) {
        if(error) return;
        db.getPurchaseOrderDetails(order_id, req.session.locale, function (error1, order_detail) {
            for(var index=0;index<order_detail.length;index++){
                var obj = order_detail[index];
                var promotion_price = 0;
                if(obj.order_detail_price_promotion){
                    promotion_price = obj.order_detail_price_promotion;
                }else {
                    promotion_price = 0;
                }
                var cal_price = (obj.order_detail_total_amount - promotion_price) / obj.order_detail_quantity;
                var cal_total = obj.order_detail_total_amount - promotion_price;
                obj.cal_price = cal_price;
                obj.cal_total = cal_total;
            }
            res.render("chi-tiet-lich-su-mua-hang", {
                i18n: res,
                person: person[0],
                order_detail: order_detail
            });
        })


    })


});

module.exports = router;
