var express = require('express');
var router = express.Router();

var api = require('../api/response');
var db = require('../databases/database');

router.get('/:slug_intro_title', function(req, res, next) {

    var slug_intro_title = req.params.slug_intro_title;
    console.log(slug_intro_title + slug_intro_title)
    db.getGioiThieuContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }
        var mainContent = null;
        for (var index = 0; index < contentList.length; index++) {
            if (slug_intro_title == null) {
                mainContent = contentList[index];
                break;
            }
            if (contentList[index].content_slug == slug_intro_title) {
                mainContent = contentList[index];
                break;
            }
        }
        console.log("mainContent "+ JSON.stringify(mainContent));

        res.render("gioi-thieu", {
            mainContent: mainContent,
            contentList: contentList,
            i18n: res
        });

    });

});

router.post('/getMeta', function(req, res, next) {
    var id = req.body.content_id;
    // var id = "8D8FC659-006D-4BDF-A93F-895EFE3DFFE6";
    // var id = "2CD0E485-122F-490C-866D-37BC3F4234A6";

    db.getMucDuLieu(api.ENUM_ENTITY_CODE.GTU,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});

router.get('/', function(req, res, next) {

    var contentID = req.query.id;
    db.getGioiThieuContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }

        var mainContent = null;
        for (var index = 0; index < contentList.length; index++) {
            if (contentID == null) {
                mainContent = contentList[index];
                break;
            }
            if (contentList[index].content_id == contentID) {
                mainContent = contentList[index];
                break;
            }

        }

        res.render("gioi-thieu", {
            mainContent: mainContent,
            contentList: contentList,
            i18n: res
        });

    });

});

module.exports = router;
