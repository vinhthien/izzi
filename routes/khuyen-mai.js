var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/', function(req, res, next) {

    db.countAllProductsPromotions(function(error, result) {
        if (error) {
            return;
        }

        var sum = result[0].SUM;
        var nbItemsOfPage = 12;
        var promotionNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        res.render('khuyen-mai', {
            promotionTotalPages: promotionNumberOfPages,
            nbItemsOfPage: nbItemsOfPage,
            i18n: res
        });

    });

});

router.post('/getMeta', function(req, res, next) {
    var id = "1d3c3224-4e8b-11e8-9c2d-fa7ae01bbebc";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});

router.post('/getPromotionList', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;

    db.getAllProductsPromotionPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});
router.post('/getBanner', function(req, res, next) {

    db.getBannerKhuyenMai(function(error, result) {
        if (error) {
            return;
        }

        var bannerItem = null;

        if (result != null && result.length > 0) {
            bannerItem = result[0];
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            bannerItem: bannerItem
        }, ""));
    });


});
module.exports = router;
