var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/', function(req, res, next) {


    db.countAllProductsPromotions(function(error, result) {
        if (error) {
            return;
        }

        var sum = result[0].SUM;
        var nbItemsOfPage = 8;
        var promotionNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        db.getBestSellProductOption(function (erro2, option) {
            if (erro2) return;

            if(option[0].value == 1){
                db.countAllBestSellers(function(error1, result1) {
                    if (error1) {
                        return;
                    }

                    var sum = result1[0].SUM;
                    var bestSellersNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;
                    res.render('san-pham', {
                        bestSellersTotalPages: bestSellersNumberOfPages,
                        promotionTotalPages: promotionNumberOfPages,
                        nbItemsOfPage: nbItemsOfPage,
                        i18n: res
                    });
                });
            }else {
                db.countAllBestSellProduct(function(error1, result1) {
                    if (error1) {
                        return;
                    }

                    var sum = parseInt(result1[0].count);
                    var bestSellersNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;
                    res.render('san-pham', {
                        bestSellersTotalPages: bestSellersNumberOfPages,
                        promotionTotalPages: promotionNumberOfPages,
                        nbItemsOfPage: nbItemsOfPage,
                        i18n: res
                    });
                });
            }
        })


    });

});

router.post('/getMeta', function(req, res, next) {
    var id = "4cc75d0a-4e83-11e8-9c2d-fa7ae01bbebc";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});
router.post('/getPromotionList', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;

    db.getAllProductsPromotionPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getBestSellerList', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;
    db.getBestSellProductOption(function (erro2, option) {
        if (erro2) return;

        if (option[0].value == 1) {
            db.getAllBestSellersPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, function (error1, listContent) {
                if (error1) {
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, {
                    contentList: listContent
                }, ""));
            });
        }else {
            db.getCustomAllBestSellersPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, function (error1, listContent) {
                if (error1) {
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, {
                    contentList: listContent
                }, ""));
            });
        }
    })

});

router.post('/getBanner', function(req, res, next) {

        db.getBannerSanPham(function(error, result) {
            if (error) {
                return;
            }

            if (!result || result.length == 0) {
                res.json(api.getResponse(api.SUCC_EXEC, {
                    banner_image: null,
                    banner_link: null
                }, ""));
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, {
                banner_image: result[0].content_image,
                banner_link: result[0].content_link
            }, ""));
        });


});

module.exports = router;
