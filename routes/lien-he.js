var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var html2pug = require('html2pug');
var svgCaptcha = require('svg-captcha');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');

var email_process = require('../businesses/email_process');

router.get('/', function(req, res, next) {

    var sitemap = require('express-sitemap');
    sitemap = sitemap();
    sitemap.generate4(router);
    // sitemap.tickle();

    sitemap.toFile();

    db.getLienHeContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }

        var content = null;
        if(contentList.length > 0) {
            contentList[0].contentHTML = contentList[0].contentHTML.replace(/(\r\n\t|\n|\r\t)/gm, "");
            content = contentList[0];
        }

        res.render("lien-he", {
            content: content,
            i18n: res
        });

    });

});



router.post('/getBanner', function(req, res, next) {
    db.getBannerLienHe(function(error, result) {
        if (error) {
            return;
        }

        var bannerItem = null;

        if (result != null && result.length > 0) {
            bannerItem = result[0];
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            bannerItem: bannerItem
        }, ""));
    });
});

router.post('/lienhe_content', function(req, res, next) {
    db.getLienHeContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }

        var content = null;
        if(contentList.length > 0) {
            contentList[0].contentHTML = contentList[0].contentHTML.replace(/(\r\n\t|\n|\r\t)/gm, "");
            content = contentList[0];
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            content: content
        }, ""));

    });
});

router.post('/captcha', function(req, res, next) {
    var captcha = svgCaptcha.create();
    captcha.data = captcha.data.replace(/(\r\n\t|\n|\r\t)/gm,"");
    captcha.data = captcha.data.replace(/\s+/g," ");

    res.json(api.getResponse(api.SUCC_EXEC, {
        captcha: captcha
    }, ""));
});

router.post('/', function(req, res, next) {

    var obj = req.body;
    var name = obj.name;
    var email =  obj.email;
    var address = obj.address;
    var tel = obj.tel;
    var content = obj.content;
    var id = uuidv1();
    var created_date = api.getNow();
    var status = 1;
    db.insertLienHe(id,email,tel,content,created_date,email,address,name,status,function (error, result) {
        if (error) {
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, null, ""));


            db.getUserAdmin(function (error3, adminList) {
                if(error3){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                    return;
                }

                for(var index=0;index<adminList.length;index++){
                    var obj = adminList[index];
                    var notif_id = uuidv1();
                    var titleNotification = "Nhận 01 liên hệ";
                    var descriptionNotification = "Người dùng [" + email + "] vừa gửi 1 liên hệ";

                    var icon = "glyphicon glyphicon-envelope";
                    db.insertNotification(notif_id,titleNotification,icon,
                                        descriptionNotification,obj.id,created_date,
                                        "System","danh-sach-lien-he",0,1,id,"",1,function (error1, resultA) {
                        if(error1){
                            return;
                        }
                    });

                    // var socket = req.app.get('socket');
                    // if(socket) {
                    //     socket.nsp.connected[obj.socket_id].emit('on-notification', titleNotification, descriptionNotification, icon, "danh-sach-lien-he");
                    // }
                }
                email_process.sendEmail(email_process.info_email_server, "Nhận 01 liên hệ từ " + email,
                    "Tên: " + name + " \n" +
                    "Địa chỉ: " + address + " \n" +
                    "Email: " + email + " \n" +
                    "Điện thoại: " + tel + " \n" +
                    "Nội dung: " + content + " \n");

            })
    });
});
router.post('/getMeta', function(req, res, next) {
    var id = "a551e55a-3e3c-11e8-b467-0ed5f89f718b";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.ELH,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});
module.exports = router;
