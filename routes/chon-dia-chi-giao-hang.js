var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');
const uuidv1 = require('uuid/v1');
var moment = require('moment');

var promotion_process = require('../businesses/promotion_process');
var order_process = require('../businesses/order_process');
var email_process = require('../businesses/email_process');
var notification_process = require('../businesses/notification_process');


router.get('/', function (req, res, next) {

    var name = "ship-time-text";
    db.getConfig(name,function (error, config) {
        if (error) {
            return;
        }
        res.render('chon-dia-chi-giao-hang', {
            i18n: res,
            ship_time: config[0].value
        });
    });


});


router.post('/get_district', function (req, res, next) {
    var obj = req.body;
    var city_id = obj.city_id;
    var entity_code = api.ENUM_ENTITY_CODE.QUH;
    db.getLookupData(city_id, entity_code, function (error, district_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            district_list: district_list
        }, ""));
    })

});

router.post('/get_ward', function (req, res, next) {
    var obj = req.body;
    var district_id = obj.district_id;
    var entity_code = api.ENUM_ENTITY_CODE.PHU;
    db.getLookupData(district_id, entity_code, function (error, ward_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            ward_list: ward_list
        }, ""));
    })

});
router.post('/create_order', function (req, res, next) {
    var obj = req.body;

    var diff = obj.diff;
    var email = obj.email;
    var name = obj.name;
    var address = obj.address;
    var phone = obj.phone;
    var city_id = obj.city_id;
    var district_id = obj.district_id;
    var ward_id = "";
    var coupon = obj.coupon;

    if (req.session.cart == null) {
        req.session.cart = [];
    }

    order_process.insertPurchaseOrder(req.session.locale, req.session.cart, diff, email, name, address, phone, city_id,
        district_id, ward_id, coupon, function(order_id) {
            //res.json(api.getResponse(api.SUCC_EXEC, order_id, ""));
            order_process.getOrderInformation(order_id, req.session.locale, function(total_amount_after_product_promotion, ship_fee,
                                                                                     couponApplied, coupon_discount, orderDetails, orderInfo) {

                total_amount_after_product_promotion = ~~total_amount_after_product_promotion;
                var client_name = orderInfo.order_fullname;
                var client_email = orderInfo.order_email;
                var address = orderInfo.order_address_delivery;
                var phone = orderInfo.order_phone;
                var code = orderInfo.order_code;
                var now = api.getNowEmail();
                var baseUrl = api.getServerAddressUpload(req);
                // var baseUrl = "https://f7eda720.ngrok.io/upload/";
                var oriUrl = api.getServerAddressBase(req);
                // var oriUrl = "https://f7eda720.ngrok.io/";
                var payment_method = order_process.getPaymentMethodString(1);
                var name = "ship-time-text";
                db.getConfig(name,function (error, config) {
                    if (error) {
                        return;
                    }
                    var ship_time =  config[0].value;
                    res.render('admin/email-confirm-order', {
                        client_name: client_name,
                        payment_method: payment_method,
                        code: code,
                        now: now,
                        address: address,
                        phone: phone,
                        products: orderDetails,
                        total_amount_after_product_promotion: total_amount_after_product_promotion,
                        ship_fee: ship_fee,
                        ship_time: ship_time,
                        base_url: baseUrl,
                        ori_url: oriUrl,
                        discount_voucher: coupon_discount,
                        final_total_amount: total_amount_after_product_promotion - ship_fee - ~~coupon_discount
                    }, function (err, html) {
                        email_process.sendEmail(client_email, "Thái Long xác nhận đặt hàng", html);
                        email_process.sendEmail(email_process.info_email_server, "Nhận 01 đơn hàng từ [" + client_email + "]", html);
                    });

                    notification_process.sendNotification(order_id, client_email, req.app.get('socket'));

                });

            });



            req.session.cart = null;
            res.json(api.getResponse(api.SUCC_EXEC, "san-pham", ""));
    });



    // db.checkExistPersonAddress(name, email, city_id, district_id, address, phone, function (error, rs) {
    //     if (error) return;
    //     if (rs.length == 0) {
    //         var person_id = req.session.enduser_user_id;
    //         var new_id = uuidv1();
    //         var created_at = api.getNow();
    //         var created_by = name;
    //         db.insertPersonAddress(new_id, person_id, name, city_id, district_id, ward_id, "", email, phone, address, 0, created_by, created_at, 1, function (e, a) {
    //             // nothing
    //         })
    //     }
    // })




});




router.post('/getProductGiftPromotion', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;
    db.findPrductGiftByPromotionId(promotion_id, function (error11, productList) {
        if (error11) {
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            gift_products: productList
        }, ""));
    })

});

router.post('/activeCoupon', function (req, res, next) {

    var obj = req.body;
    var coupon = obj.coupon;

    if (req.session.cart == null) {
        req.session.cart = [];
    }

    promotion_process.getCouponPromotion(coupon, req.session.locale, req.session.cart,
            function(coupon_promotion_id, coupon_promotion_name, coupon_percent_promotion, total_promotion) {
        res.json(api.getResponse(api.SUCC_EXEC, {
            promotion: total_promotion
        }, ""));
    }, function() {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, "", "Lỗi mạng"));
    });


});

router.post('/checkLoaded', function(req, res, next) {

    if (req.session.cart == null) {
        req.session.cart = [];
    }
    if(req.session.cart.length == 0){
        res.json(api.getResponse(api.SUCC_EXEC, true, ""));
    }else {
        res.json(api.getResponse(api.SUCC_EXEC, false, ""));
    }

});

router.post('/get_user_info', function (req, res, next) {

    var person_id = req.session.enduser_user_id;
    if (!person_id) {
        db.getLookupData(null, api.ENUM_ENTITY_CODE.TTP, function (error2, city_list) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, city_list, ""));
        });
        return;
    }


    function get_info(person_id, callback) {
        db.getPersonAddressDefault(person_id, function (error, personAddress) {
            if (error) {
                return;
            }
            if (personAddress.length == 0) {
                db.getPersonAddressByPersonId(person_id, function (error6, rs) {
                    if (error6) return;
                    if (rs.length == 0) {
                        db.getPersonById(person_id, function (error7, person) {
                            if (error7) return;
                            callback(person);

                        })
                    } else {
                        callback(rs);
                    }
                })
            } else {
                callback(personAddress);
            }


        })
    }

    get_info(person_id, function (personAddress) {
        db.getLookupData(null, api.ENUM_ENTITY_CODE.TTP, function (error2, city_list) {
            var entity_code_district = api.ENUM_ENTITY_CODE.QUH;
            var entity_code_ward = api.ENUM_ENTITY_CODE.PHU;
            if (personAddress.length == 0) {
                res.json(api.getResponse(api.SUCC_EXEC, {
                    personAddress: personAddress[0],
                    district_list: [],
                    ward_list: [],
                    city_list: city_list
                }, ""));

            } else {
                db.getLookupData(personAddress[0].city_id, entity_code_district, function (error3, district_list) {
                    if (error3) {
                        return;
                    }
                    db.getLookupData(personAddress[0].district_id, entity_code_ward, function (error4, ward_list) {
                        if (error4) {
                            return;
                        }

                        res.json(api.getResponse(api.SUCC_EXEC, {
                            personAddress: personAddress[0],
                            district_list: district_list,
                            ward_list: ward_list,
                            city_list: city_list
                        }, ""));

                    })
                })
            }


        })

    })


});


module.exports = router;
