var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');


router.get('/', function(req, res, next) {
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;

    function get_lookup(city_id,entity_code_district,district_id,entity_code_ward,index,callback) {
        db.getLookupDataById(city_id,function (error3, city) {
            if(error3){
                return;
            }
            db.getLookupDataById(district_id,function (error4, district) {
                if(error4){
                    return;
                }

                callback({city: city[0],district: district[0]},index);


            })
        })
    }

    function get_data(user_id,callback) {
        db.getPersonAddressByPersonId(user_id,function (error, personAddress) {
            if(error){
                return;
            }

            db.getPersonById(user_id,function (error1, person) {
                if(error1){
                    return;
                }

                db.getLookupData(null,api.ENUM_ENTITY_CODE.TTP,function (error2, city_list) {
                    if(error2){
                        return;
                    }

                    if(personAddress.length == 0){
                        res.render("so-dia-chi", {
                            i18n: res,
                            personAddress: personAddress,
                            user_id: user_id,
                            username: person[0].name,
                            city_list: city_list
                        });
                    }else if(personAddress.length > 0){
                        var district_ward_list = [];

                        for(var index = 0;index<personAddress.length;index++){

                            var obj = personAddress[index];
                            var city_id = obj.city_id;
                            var district_id = obj.district_id;
                            var entity_code_district = api.ENUM_ENTITY_CODE.QUH;
                            var entity_code_ward = api.ENUM_ENTITY_CODE.PHU;
                            if(index == (personAddress.length - 1)){
                                get_lookup(city_id,entity_code_district,district_id,entity_code_ward,index,function (obj,i) {
                                    if(i == (personAddress.length-1)){
                                        district_ward_list.push(obj);
                                        callback(personAddress,person,city_list,district_ward_list);

                                    }

                                })
                            }else {
                                get_lookup(city_id,entity_code_district,district_id,entity_code_ward,index,function (obj,i) {
                                    district_ward_list.push(obj);
                                })
                            }

                        }

                    }


                })

            })


        });
    }

    get_data(user_id,function (personAddress,person,city_list,district_ward_list) {
        res.render("so-dia-chi", {
            i18n: res,
            personAddress: personAddress,
            user_id: user_id,
            username: person[0].name,
            city_list: city_list,
            district_ward_list: district_ward_list
        });
    })


});


router.post('/get_district', function(req, res, next) {
    var obj = req.body;
    var city_id = obj.city_id;
    var entity_code = api.ENUM_ENTITY_CODE.QUH;
    db.getLookupData(city_id,entity_code,function (error, district_list) {
        if(error){
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            district_list: district_list
        }, ""));
    })

});

router.post('/remove_person_address', function(req, res, next) {
    var obj = req.body;
    var person_address_id = obj.person_address_id;
    var person_id = req.session.enduser_user_id;

    var created_at = api.getNow();
    var created_by = person_id;
    db.removePersonAddress(person_address_id,created_at,created_by, function (error, district_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));
    })

});

router.post('/add_info', function(req, res, next) {
    var obj = req.body;
    var fullName = obj.fullName;
    var phoneNumber = obj.phoneNumber;
    var email = obj.email;
    var city_id = obj.city_id;
    var district_id = obj.district_id;
    var ward_id = "";
    var street = "";
    var address = obj.address;
    var is_default = obj.is_default;
    var person_id = req.session.enduser_user_id;

    function get_default(is_default,callback) {
        if(is_default){
            db.updatePersonAddressDefault(person_id,function (e, re) {
                if(e){
                    return;
                }
                callback(1);
            })
        }else {
            callback(0);
        }

    }
    get_default(is_default,function (is_default) {
        var entity_code = api.ENUM_ENTITY_CODE.QUH;
        var id = uuidv1();
        var created_at = api.getNow();
        var created_by = person_id;
        db.insertPersonAddress(id,person_id,fullName,city_id,district_id,ward_id,street,email,phoneNumber,address,is_default,created_by,created_at,1,function (error, rs) {
            if(error){
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, id, ""));
        })
    })


});

router.post('/update_info', function(req, res, next) {
    var obj = req.body;
    var fullName = obj.fullName;
    var phoneNumber = obj.phoneNumber;
    var email = obj.email;
    var city_id = obj.city_id;
    var district_id = obj.district_id;
    var ward_id = obj.ward_id;
    var street = obj.street;
    var address = obj.address;
    var is_default = obj.is_default;
    var person_address_id = obj.person_address_id;
    if(is_default){
        is_default = 1;
    }else {
        is_default = 0;
    }
    var person_id = req.session.enduser_user_id;
    var created_at = api.getNow();
    var created_by = person_id;
    db.updatePersonAddress(person_id,fullName,city_id,district_id,ward_id,street,email,phoneNumber,address,is_default,created_at,created_by,1,person_address_id,function (error, rs) {
        if(error){
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, rs, ""));
    })

});

router.post('/get_ward', function(req, res, next) {
    var obj = req.body;
    var district_id = obj.district_id;
    var entity_code = api.ENUM_ENTITY_CODE.PHU;
    db.getLookupData(district_id,entity_code,function (error, ward_list) {
        if(error){
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            ward_list: ward_list
        }, ""));
    })

});
module.exports = router;
