var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/', function(req, res, next) {


    db.countAllProductsWithLatest(function(error, result) {
        if (error) {
            return;
        }

        var sum = result[0].SUM;
        var nbItemsOfPage = 12;
        var productNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        res.render('san-pham-moi', {
            productTotalPages: productNumberOfPages,
            nbItemsOfPage: nbItemsOfPage,
            i18n: res
        });

    });

});


router.post('/getProductList', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;

    db.getAllProductsPagingWithLanguageAndLatest((page - 1) * nbItemsOfPage, nbItemsOfPage,
                                                    req.session.locale, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

module.exports = router;
