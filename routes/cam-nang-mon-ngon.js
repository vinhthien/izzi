var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/', function(req, res, next) {

    db.countAllMonNgonTop(function(error, result) {
        if (error) {
            return;
        }
        var countImageInSlider = result[0].total;
        db.countAllMonAnNgonContent(req.session.locale, function(error, result) {
            if (error) {
                return;
            }
            var sum = result[0].SUM;
            var nbItemsOfPage = 9;
            var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;
            res.render('cam-nang-mon-ngon', {
                totalPages: numberOfPages,
                nbItemsOfPage: nbItemsOfPage == 0 ? 1 : nbItemsOfPage,
                countImageInSlider: countImageInSlider == 0 ? 1 : countImageInSlider,
                i18n: res
            });


        });
    });

});

router.post('/getMeta', function(req, res, next) {
    var id = "d90efbe6-4e7f-11e8-9c2d-fa7ae01bbebc";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});


router.post('/getBanner', function(req, res, next) {

    db.getBannerMonNgon(req.session.locale, function(error, result) {
        if (error) {
            return;
        }

        var bannerItem = null;

        if (result != null && result.length > 0) {
            bannerItem = result[0];
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            bannerItem: bannerItem
        }, ""));
    });

});

router.post('/getContentList', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;

    db.getAllMonAnNgonContentPagingWithLanguage((page - 1) * nbItemsOfPage,
                            nbItemsOfPage, req.session.locale, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }
        for(var index=0;index<contentList.length;index++){
            contentList[index].content_created_date = api.revertDatePromotionFrontEnd(contentList[index].content_created_date);
        }


        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getImageInSlider', function(req, res, next) {

    var order = req.body.order - 1;

    db.getTopMonAnNgonSlidersByOrder(req.session.locale, order, function(error, result) {
        if (error) {
            return;
        }

        var sliderItem = null;

        if (result != null && result.length > 0) {
            sliderItem = result;
        }

        db.getMonAnNgonSliderSpeed(function (error, obj) {
            if (error){
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, {
                sliderItem: sliderItem,
                tocDo: obj[0].value
            }, ""));
        });


    });

});

module.exports = router;
