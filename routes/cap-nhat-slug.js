var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var api = require('../api/response');


router.get('/', function (req, res, next) {
    db.getAllDataDirectory(function(error, dataDirectories) {
       if (error) {
           return;
       }
       if (dataDirectories && dataDirectories.length > 0) {
            dataDirectories.forEach(function(item) {
                var slug = api.getSlug(item.data_directory_title);
                db.updateSlugs(slug, item.data_directory_id, function() {
                    // nothing
                })
            });
       }
    });
    res.render('gioi-thieu', {});
});


router.post('/getMeta', function(req, res, next) {
    var id = "8D8FC659-006D-4BDF-A93F-895EFE3DFFE6";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});

module.exports = router;
