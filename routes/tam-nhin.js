var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render("tam-nhin", {
        i18n: res
    });
});

module.exports = router;
