var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');
var uuidv1 = require('uuid/v1');
var nodemailer = require('nodemailer');
var moment = require('moment');

var order_process = require('../businesses/order_process');
var email_process = require('../businesses/email_process');
var promotion_process = require('../businesses/promotion_process');



router.get('/', function(req, res, next) {
    console.log("ina")
    db.getAllHomeSliders(function(error, listSlider) {
        if (error) {
            return;
        }
        if (listSlider == null) {
            listSlider = [];
        }
        console.log('listSlider')
        console.log(listSlider)
        res.render("trang-chu",{
            sliderList: listSlider,
            number: listSlider.length,
            i18n: res
        });
    });

});

router.post('/getPromotionPopupInfo', function(req, res, next) {
    console.log("req.session.promotion_popup_shown "+ req.session.promotion_popup_shown);

    if (req.session.promotion_popup_shown != null) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Popup is shown!"));
        return;
    }
    req.session.promotion_popup_shown = true;

    var promotion_popup_delay_show = "promotion_popup_delay_show";
    var promotion_popup_content = "promotion_popup_content";
    var promotion_popup_show = "promotion_popup_show";

    db.getConfig(promotion_popup_delay_show,function (error, delay_show) {
        if(error) {
            return;
        }
        db.getConfig(promotion_popup_content,function (error1, contentHTML) {
            if (error1) {
                return;
            }
            db.getConfig(promotion_popup_show,function (error1, show) {
                if (error1) {
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, {
                    delayShow: delay_show[0].value,
                    contentHTML: contentHTML[0].value,
                    show: show[0].value
                }, ""));
            });
        });
    });

});

router.post('/getLatestContentList', function(req, res, next) {

    var content_id = req.body.content_id;
    if(!content_id){
        content_id = "";
    }
    db.getAllLatestMonAnNgonContentWithLanguage(req.session.locale,content_id, function (error1, listContent) {
        if (error1) {
            return;
        }

        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        for(var index=0;index<contentList.length;index++){
            contentList[index].content_created_date = api.revertDatePromotionFrontEnd(contentList[index].content_created_date);
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));

    });
});

router.get('/getCartBadge', function(req, res, next) {
    var productID = req.body.product_id;
    var quantity = req.body.quantity;

    if (req.session.cart == null) {
        req.session.cart = [];
    }
    res.json(api.getResponse(api.SUCC_EXEC, {
        badge: req.session.cart.length
    }, ""));
});

router.get('/getScript', function (req, res, next) {
    var entity_code = api.ENUM_ENTITY_CODE.SHT;
    db.getScript(entity_code,function (error, headscript) {
        if (error) {

            return;
        }

        var headscript=headscript[0];
        res.json(api.getResponse(api.SUCC_EXEC, headscript, ""));
    });
});


router.get('/getTongDaiContact', function(req, res, next) {

    var call_name = "front_end_call_number_footer";
    var sms_name = "front_end_sms_number_footer";
    var lat_name = "front_end_map_latitude_footer";
    var long_name = "front_end_map_longitude_footer";

    db.getConfig(call_name,function (error, call_number) {
        if(error) return;
        db.getConfig(sms_name,function (error1, sms_number) {
            if (error1) return;
            db.getConfig(lat_name, function (error2, lat) {
                if (error2) return;
                db.getConfig(long_name, function (error3, long) {
                    if (error3) return;
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        call_number: call_number[0].value,
                        sms_number: sms_number[0].value,
                        lat: lat[0].value,
                        long: long[0].value
                    }, ""));

                })
            })
        })
    })

});


router.post('/getSliderSpeed', function(req, res, next) {
    console.log("in")
    db.getHomeSliderSpeed(function (error, speed) {
        if(error){
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, speed[0], ""));

    })

});

router.post('/getMeta', function(req, res, next) {
    var id = "6ca82f8e-4a03-11e8-842f-0ed5f89f718b";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});

router.get('/getQuanly', function(req, res, next) {

    var id = "f0bde88c-444e-11e8-842f-0ed5f89f718b";
    var lan = req.session.locale;

    db.getMucDuLieu(api.ENUM_ENTITY_CODE.AAA, lan, id, function (error4, content) {
        if (error4) {
            return;
        }

        if (content.length == 0) {
            res.json(api.getResponse(api.SUCC_EXEC, "", ""));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, content[0].contentHTML, ""));

    });

});

router.post('/addToCart', function(req, res, next) {
    var productID = req.body.product_id;
    var quantity = req.body.quantity;

    if (req.session.cart == null) {
        req.session.cart = [];
    }

    db.getWarehouseByProductId(productID, function(error, result) {
        if (error) {
            return;
        }
        if (result.length > 0) {

            var limit_quantity = 0;
            if (result[0].root_product_id != null) {
                limit_quantity = result[0].root_product_quantity ? result[0].root_product_quantity : 0;
                limit_quantity = parseInt(limit_quantity / result[0].product_quantity_unit);
            } else {
                limit_quantity = result[0].product_quantity ? result[0].product_quantity : 0;
            }

            for (var i = req.session.cart.length - 1; i >= 0 ; i--) {
                if (productID == req.session.cart[i].product_id) {
                    req.session.cart[i].quantity = req.session.cart[i].quantity + quantity;
                    var endQuantity = req.session.cart[i].quantity;
                    if (endQuantity == 0) {
                        req.session.cart.splice(i, 1);
                    } else {
                        if (limit_quantity < endQuantity) {
                            req.session.cart[i].quantity = limit_quantity;
                            res.json(api.getResponse(api.SUCC_EXEC, {
                                new_quantity: endQuantity,
                                reach_limit: true,
                                limit_quantity: limit_quantity
                            }, ""));
                            return;
                        }
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        new_quantity: endQuantity
                    }, ""));
                    return;
                }
            }

            if (quantity != 0) {
                if (limit_quantity < quantity) {
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        new_quantity: quantity,
                        reach_limit: true,
                        limit_quantity: limit_quantity
                    }, ""));
                    return;
                }
                req.session.cart.push({
                    product_id: productID,
                    quantity: quantity
                });
            }
            
            res.json(api.getResponse(api.SUCC_EXEC, {
                new_quantity: quantity
            }, ""));
        }
    });
});

router.post('/getProductCartLength', function(req, res, next) {

    if (req.session.cart == null) {
        req.session.cart = [];
    }
    
    res.json(api.getResponse(api.SUCC_EXEC, {
        cart_length: req.session.cart.length
    }, ""));
});

router.post('/contact', function(req, res, next) {

    var name = req.body.name;
    var facebook_name = "contact-facebook";
    var youtube_name = "contact-youtube";
    var zalo_name = "contact-zalo";
    var temp = null;
    if(name == "facebook"){
        temp = facebook_name;
    }else if(name == "youtube"){
        temp = youtube_name;
    }else if(name == "zalo"){
        temp = zalo_name;
    }
    db.getConfig(temp,function (error,rs) {
        if(error) return;
        var link = rs[0].value;
        
        res.json(api.getResponse(api.SUCC_EXEC, link, ""));
    });


});

router.post('/updateProductToCart', function(req, res, next) {
    var productID = req.body.product_id;
    var quantity = req.body.quantity;
    if (req.session.cart == null) {
        req.session.cart = [];
    }

    db.getWarehouseByProductId(productID, function(error, result) {
        if (error) {
            return;
        }

        if (result.length > 0) {

            var limit_quantity = 0;
            if (result[0].root_product_id != null) {
                limit_quantity = result[0].root_product_quantity ? result[0].root_product_quantity : 0;
                limit_quantity = parseInt(limit_quantity / result[0].product_quantity_unit);
            } else {
                limit_quantity = result.product_quantity ? result.product_quantity : 0;
            }



            for (var i = 0; i < req.session.cart.length; i++) {
                if (productID == req.session.cart[i].product_id) {
                    req.session.cart[i].quantity = quantity;
                    if (quantity == 0) {
                        req.session.cart.splice(i, 1);
                    } else {
                        if (limit_quantity < quantity) {
                            req.session.cart[i].quantity = limit_quantity;
                            res.json(api.getResponse(api.SUCC_EXEC, {
                                new_quantity: limit_quantity,
                                reach_limit: true,
                                limit_quantity: limit_quantity
                            }, ""));
                            return;
                        }
                    }
                    
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        new_quantity: quantity,
                        reach_limit: false,
                        limit_quantity: limit_quantity
                    }, ""));


                    return;
                }
            }

            if (quantity != 0) {

                if (limit_quantity < quantity) {
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        new_quantity: quantity,
                        reach_limit: true,
                        limit_quantity: limit_quantity
                    }, ""));
                    return;
                }
                req.session.cart.push({
                    product_id: productID,
                    quantity: quantity
                });
            }

            
            res.json(api.getResponse(api.SUCC_EXEC, {
                new_quantity: quantity,
                reach_limit: false,
                limit_quantity: limit_quantity
            }, ""));


        }
    });


});


router.post('/getProductCategoriesRoot', function(req, res, next) {
    db.getAllProductCategoriesRootWithLanguage(req.session.locale, function(error, listContent) {
        if (error) {
            return;
        }

        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }
        
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getProductCategoriesChildren', function(req, res, next) {
    var id = req.body.parent_id;

    db.getAllProductCategoriesChildrenWithLanguage(id, req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }
        
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList,
            root_category_id: id
        }, ""));

    });

});

router.post('/getBanner', function(req, res, next) {
    var product_id = req.body.product_id;
    var category_id = req.body.category_id;
    var root_category_id = req.body.root_category_id;

    if(category_id!= null && category_id.length>0){
        db.getDanhMucSanPhamById(category_id,function (error, danhmuc) {
            if(error) return;
            if(danhmuc.length == 0 || danhmuc[0].banner_image == null || danhmuc[0].banner_image.length==0){
                db.getDanhMucSanPhamById(danhmuc[0].parent_id,function (error1, rootDanhMuc) {
                    if(error1) return;
                    if (rootDanhMuc.length == 0 || rootDanhMuc[0].banner_image == null || rootDanhMuc[0].banner_image.length == 0) {
                        db.getBannerSanPham(function (error, result) {
                            if (error) {
                                return;
                            }

                            var content_image = null;
                            if (result && result.length > 0) {
                                content_image = result[0].content_image;
                            }
                            
                            res.json(api.getResponse(api.SUCC_EXEC, {
                                banner_image: content_image
                            }, ""));
                        });
                    } else {
                        
                        res.json(api.getResponse(api.SUCC_EXEC, {
                            banner_image: rootDanhMuc[0].banner_image
                        }, ""));
                    }
                })
            }else {
                
                res.json(api.getResponse(api.SUCC_EXEC, {
                    banner_image: danhmuc[0].banner_image
                }, ""));
            }
        })
    }else if(root_category_id!= null && root_category_id.length>0){
        db.getDanhMucSanPhamById(root_category_id,function (error, danhmuc) {
            if(error) return;
            if (danhmuc.length == 0 || danhmuc[0].banner_image == null || danhmuc[0].banner_image.length == 0) {
                db.getBannerSanPham(function (error, result) {
                    if (error) {
                        return;
                    }
                    
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        banner_image: result[0].content_image
                    }, ""));
                });
            } else {
                
                res.json(api.getResponse(api.SUCC_EXEC, {
                    banner_image: danhmuc[0].banner_image
                }, ""));
            }
        })
    }else {

        db.getBannerSanPham(function(error, result) {
            if (error) {
                return;
            }
            var content_image = null;
            var content_link = null;
            if (result && result.length > 0) {
                content_image = result[0].content_image;
                content_link = result[0].content_link;
            }
            
            res.json(api.getResponse(api.SUCC_EXEC, {
                banner_image: content_image,
                banner_link: content_link
            }, ""));
        });
    }


});

router.post('/getCart', function (req, res, next) {

    if (req.session.cart == null) {
        req.session.cart = [];
    }

    db.getPriceTransport(function(err, phivanchuyen) {
        if (err) {
            return err;
        }
        if (phivanchuyen.length > 0) {

            var ship_fee = parseInt(phivanchuyen[0].value);
            order_process.getProductInfoForCart(req.session.locale, req.session.cart, function(products, totalCustomerHasToPay) {

                if (!products) {
                    products = [];
                }

                res.json(api.getResponse(api.SUCC_EXEC, {
                    products: products,
                    total_amount_after_product_promotion: totalCustomerHasToPay,
                    phi_van_chuyen: ship_fee,
                    final_total_amount: totalCustomerHasToPay + ship_fee
                }, ""));

            }, function() {
                // nothing
            });

        }
    });
});

router.post('/loadProductGift', function(req, res, next) {

    var product_id = req.body.product_id;
    promotion_process.getProductGiftByProductId(product_id, req.session.locale, function(productGifts, available) {

        if (!productGifts) {
            productGifts = [];
        }
        
        res.json(api.getResponse(api.SUCC_EXEC, {
            product_gifts: productGifts,
            available: available
        }, ""));

    }, function() {
        // nothing
    });

});

router.get('/getAllBranches', function(req, res, next) {
    db.getAllBranches(req.session.locale, function(error, branches) {
        if (error) {
            return;
        }
        
        res.json(api.getResponse(api.SUCC_EXEC, {
            branches: branches
        }, ""));

    });

});



router.post('/dang-ky-social', function(req, res, next) {
    var obj = req.body;
    var url = obj.url;
    var name =  obj.name;
    var email = obj.email;
    var platform = obj.platform;
    var social_user_client_id = obj.client_id;
    db.validateSocialClientEmail(email,function (error, user) {
        if(error){
            return;
        }
        if(user.length > 0){
            var id = user[0].id;
            var now = api.getNow();
            db.updateUserLogin(id, now,function (e, r) {
                if(r){
                    req.session.enduser_username = user[0].user_name;
                    req.session.enduser_user_id = user[0].id;
                    req.session.save(function(err) {
                        // session saved
                        res.json(api.getResponse(api.SUCC_EXEC, "", ""));

                    })
                }
            });
            return;
        }

        var id = uuidv1();
        var user_name = "user_name"+id;
        var password = null;
        var is_confirm = 1;
        var type = 2;
        var login_fail = 0;
        var last_login = api.getNow();
        var is_locked = 0;
        var created_date = api.getNow();
        var created_by = id;
        var entity_code = api.ENUM_ENTITY_CODE.PER;
        var socket_id = null;

        db.insertUser(id,user_name,password,is_confirm,
            type,login_fail,last_login,is_locked,
            created_date,created_by,entity_code,
            socket_id,function (error1,result) {
            if(error1){
                return;
            }

                var social_user_id = uuidv1();
            var social_user_title = user_name;
            var social_user_code = null;
            var social_user_email = email;
            var social_user_image = url;
            var social_user_created_by = id;
            var social_user_created_date = api.getNow();
            var social_user_status = 1;
            db.insertUserSocial(social_user_id,id,social_user_title,
                social_user_code,social_user_client_id,social_user_email,
                social_user_image,social_user_created_by,social_user_created_date,
                social_user_status,platform,function (error2, result1) {
                    if (error2){
                        return;
                    }

                    var person_id = id;
                    var person_identification = null;
                    var person_name = name;
                    var person_avatar = null;
                    var person_birthday = api.getNow();
                    var person_points = 0;
                    var person_phone = "";
                    var person_gender = 1;
                    var person_email = social_user_email;
                    var person_created_date = api.getNow();
                    var person_updated_by = id;
                    var person_entity_code = api.ENUM_ENTITY_CODE.PER;
                    var person_status = 1;
                    db.insertPerson(person_id,person_identification,
                        person_name,person_avatar,person_birthday,
                        person_points,person_phone,person_gender,
                        person_email,person_created_date,
                        person_updated_by,person_entity_code,person_status,
                        function (error3, result4) {
                            if (error3) {
                                return;
                            }

                            req.session.enduser_username = user_name;
                            req.session.enduser_user_id = id;
                            req.session.save(function(err) {
                                var subject_name = "email_subject_editor";
                                var content_name = "content_subject_editor";
                                var promotion_for_user_signup_first_time = "promotion_for_user_signup_first_time";

                                db.getConfig(subject_name, function (error, subjectObj) {
                                    if (error) {
                                        return;
                                    }
                                    db.getConfig(content_name, function (error1, contentObj) {
                                        if (error1) {
                                            return;
                                        }
                                        db.getConfig(promotion_for_user_signup_first_time, function (error3, promotionObject) {
                                            if (error3) {
                                                return;
                                            }

                                            var html = contentObj[0].value;
                                            html = html.replace("/upload/", api.getServerAddressUpload(req));
                                            html = html.replace("&lt;CLIENT_NAME&gt;", name);
                                            if (!promotionObject[0].value) {
                                                html = html.replace("&lt;MA_GIAM_GIA&gt;", "");
                                                html = html.replace("&lt;GIAM_GIA_TEXT&gt;", "");
                                                email_process.sendEmail(social_user_email, subjectObj[0].value, html);
                                            } else {
                                                var promotion_id = promotionObject[0].value;
                                                db.getCouponListForSignup(promotion_id, function (error, promotionList) {
                                                    if (error) {
                                                        return;
                                                    }
                                                    if (promotionList.length == 0) {
                                                        html = html.replace("&lt;MA_GIAM_GIA&gt;", "");
                                                        html = html.replace("&lt;GIAM_GIA_TEXT&gt;", "");
                                                        email_process.sendEmail(social_user_email, subjectObj[0].value, html);
                                                    } else {
                                                        var coupon = promotionList[0].coupon;
                                                        db.updateCouponForSignup(promotionList[0].id, function (error1, rs) {
                                                            if (error1) {
                                                                return;
                                                            }
                                                            html = html.replace("&lt;MA_GIAM_GIA&gt;", coupon);
                                                            html = html.replace("&lt;GIAM_GIA_TEXT&gt;", "Mã giảm giá dành cho thành viên mới");
                                                            email_process.sendEmail(social_user_email, subjectObj[0].value, html);
                                                        });

                                                    }
                                                });
                                            }
                                        });
                                    });

                                });
                            })

                    });
            });
                res.json(api.getResponse(api.SUCC_EXEC, { redirect: "/thong-tin-tai-khoan" }, ""));
        });

    });

});


router.get('/getDanhMucCap2InFooter', function(req, res, next) {
    db.getDanhMucInFooterWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }
        
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, "Không thể thêm vào mục dữ liệu tiếng anh"));

    });

});

router.get('/getChinhSachBanHangLinks', function(req, res, next) {
    db.getAllChinhSachTitlesWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, "Không thể thêm vào mục dữ liệu tiếng anh"));

    });

});

router.post('/front_logout', function(req, res, next) {
    req.session.enduser_username = null;
    req.session.enduser_user_id = null;

    res.json(api.getResponse(api.SUCC_EXEC, null, ""));

});


router.post('/get_platform', function(req, res, next) {
    var id = req.session.enduser_user_id;
    db.getUserSocialPlatformByUserId(req.session.enduser_user_id,function (e3,rs) {
     if(e3){
         return;
     }

     if(rs.length>0){
         res.json(api.getResponse(api.SUCC_EXEC, rs[0].usersocial_platform, ""));
     }else {
         res.json(api.getResponse(api.SUCC_EXEC, "FACEBOOK", ""));
     }


    });

});

router.get('/updateUsername', function(req, res, next) {

    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;
    if(!user_id){
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, ""));
        return;
    }
    db.getPersonById(user_id,function (error, person) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, ""));
            return;
        }
        if(person.length > 0){
            res.json(api.getResponse(api.SUCC_EXEC, person[0].name, ""));
        }else {
            res.json(api.getResponse(api.SUCC_EXEC, "Người dùng", ""));
        }
    });
});

router.post('/getTimeShipping', function (req, res, next) {
    var time = moment().format("HH:mm:ss");
    var date = moment().add('days', 3).format("dd/MM/YYYY");
    res.json(api.getResponse(api.SUCC_EXEC, {
        time: time,
        date: date
    }, ""));

});


router.post('/lang', function(req, res, next) {
    var langCode = req.body.lang;
    req.session.locale = langCode;
    res.json(api.getResponse(api.SUCC_EXEC, null, "Không thể thêm vào mục dữ liệu tiếng anh"));

});

router.get('/get-lang', function(req, res, next) {
    res.json(req.session.locale);
});

module.exports = router;
