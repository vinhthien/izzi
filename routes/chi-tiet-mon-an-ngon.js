var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');

router.get('/:slug_handbook_title', function(req, res, next) {

    var slug_handbook_title = req.params.slug_handbook_title;
    db.getHandbookBySlug( req.session.locale, slug_handbook_title, function(error, contentList) {
        if (error) {
            return;
        }

        if (contentList && contentList.length > 0) {
            res.render('chi-tiet-mon-an-ngon', {
                content_id: contentList[0].content_id,
                i18n: res
            });
        }

    });

});

router.get('/', function(req, res, next) {

    var content_id = req.query.id;

    res.render('chi-tiet-mon-an-ngon', {
        content_id: content_id,
        i18n: res
    });
});

router.post('/getMeta', function(req, res, next) {
    var content_id = req.body.content_id;
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,req.session.locale,content_id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});


router.post('/getLatestContentList', function(req, res, next) {
    var content_id = req.body.id;
    if(!content_id){
        content_id = "";
    }
    db.getAllMon(req.session.locale, content_id, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }
        for(var index=0;index<contentList.length;index++){
            contentList[index].content_created_date = api.revertDatePromotionFrontEnd(contentList[index].content_created_date);
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getContent', function(req, res, next) {
    var content_id = req.body.id;

    db.getContentByLanguageCode( req.session.locale, content_id, function(error, contentList) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            contentDetails: contentList[0]
        }, ""));

    });

});

router.post('/getBanner', function(req, res, next) {
    var content_id = req.body.content_id;

    db.getBannerChiTietBaiViet(req.session.locale, content_id, function(error1, result1) {
        if (error1) {
            return;
        }

        if (result1 && result1.length > 0) {

            if (result1[0].content_image_banner) {
                res.json(api.getResponse(api.SUCC_EXEC, {
                    bannerItem: result1[0]
                }, ""));
                return;
            }



            db.getBannerMonNgonTrongChiTiet(function (error, result) {
                if (error) {
                    return;
                }

                var bannerItem = null;

                if (result != null && result.length > 0) {
                    bannerItem = result[0];
                }

                res.json(api.getResponse(api.SUCC_EXEC, {
                    bannerItem: bannerItem
                }, ""));
            });
        }
    });

});




module.exports = router;
