var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');

router.get('/r/:slug_category_title', function(req, res, next) {
    var slug_category_title = req.params.slug_category_title;
    db.countAllProductsWithCategoriesRootBySlug(req.session.locale, slug_category_title, function(error, result) {
        if (error) {
            return;
        }
        var sum = result[0].SUM;
        var nbItemsOfPage = 12;
        var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        var rootCategoryID = 0;
        if (result && result.length > 0) {
            rootCategoryID = result[0].product_category_id;
        }


        if (sum == 0) {
            db.getAllProductCategoryTitleWithLanguageAndSlug(req.session.locale, slug_category_title, function(error, listContent) {
                if (error) {
                    return;
                }

                var tempRootCategoryID = 0;
                if (listContent && listContent.length > 0) {
                    tempRootCategoryID = listContent[0].product_category_id;
                }

                res.render('danh-muc', {
                    totalPages: numberOfPages,
                    nbItemsOfPage: nbItemsOfPage,
                    root_category_id: tempRootCategoryID,
                    i18n: res
                });
            });
        } else {
            res.render('danh-muc', {
                totalPages: numberOfPages,
                nbItemsOfPage: nbItemsOfPage,
                root_category_id: rootCategoryID,
                i18n: res
            });
        }

    });
});

router.get('/:slug_category_title', function(req, res, next) {
    var slug_category_title = req.params.slug_category_title;
    db.countAllProductsWithCategoriesBySlug(req.session.locale, slug_category_title, function(error, result) {
        if (error) {
            return;
        }

        var sum = result[0].SUM;
        var nbItemsOfPage = 12;
        var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        var categoryID = 0;
        var category_title = "";
        if (result && result.length > 0) {
            categoryID = result[0].product_category_id;
            category_title = result[0].product_category_title;
        }

        if (sum == 0) {
            db.getAllProductCategoryTitleWithLanguageAndSlug(req.session.locale, slug_category_title, function(error, listContent) {
                if (error) {
                    return;
                }

                var tempCategoryID = 0;
                if (listContent && listContent.length > 0) {
                    tempCategoryID = listContent[0].product_category_id;
                }

                res.render('danh-muc', {
                    totalPages: numberOfPages,
                    nbItemsOfPage: nbItemsOfPage,
                    category_id: tempCategoryID,
                    i18n: res
                });
            });
        } else {
            res.render('danh-muc', {
                totalPages: numberOfPages,
                nbItemsOfPage: nbItemsOfPage,
                category_id: categoryID,
                i18n: res
            });
        }

    });
});

router.get('/', function(req, res, next) {

    var categoryID = req.query.id;
    var rootCategoryID = req.query.root_id;
    if (categoryID != null) {
        db.countAllProductsWithCategories(req.session.locale, categoryID, function(error, result) {
            if (error) {
                return;
            }

            var sum = result[0].SUM;
            var nbItemsOfPage = 12;
            var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

            res.render('danh-muc', {
                totalPages: numberOfPages,
                nbItemsOfPage: nbItemsOfPage,
                category_id: categoryID,
                i18n: res
            });

        });
    } else {
        db.countAllProductsWithCategoriesRoot(req.session.locale, rootCategoryID, function(error, result) {
            if (error) {
                return;
            }
            var sum = result[0].SUM;
            var nbItemsOfPage = 12;
            var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;
            res.render('danh-muc', {
                totalPages: numberOfPages,
                nbItemsOfPage: nbItemsOfPage,
                root_category_id: rootCategoryID,
                i18n: res
            });
        });
    }
});

router.post('/getProductByCategoryID', function(req, res, next) {
    var categoryID = req.body.category_id;
    var rootCategoryID = req.body.root_category_id;
    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;
    console.log("getProductByCategoryID")
    console.log(categoryID)
    console.log(rootCategoryID)
    console.log(page)
    console.log(nbItemsOfPage)
    if (categoryID != null && categoryID != "") {
        db.getAllProductsWithCategories((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, categoryID, function(error, contentList) {
            if (error) {
                return;
            }

            res.json(api.getResponse(api.SUCC_EXEC, {
                contentList: contentList
            }, ""));
        });
    } else {

        db.getAllProductsWithCategoriesRoot((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, rootCategoryID, function(error, contentList) {
            if (error) {
                return;
            }

            res.json(api.getResponse(api.SUCC_EXEC, {
                contentList: contentList
            }, ""));

        });
    }


});

router.post('/getMeta', function(req, res, next) {
    var content_id = req.body.content_id;
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.CAT,req.session.locale,content_id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});


module.exports = router;
