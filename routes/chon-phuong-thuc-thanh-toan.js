var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');
var uuidv1 = require('uuid/v1');
var moment = require('moment');

var order_process = require('../businesses/order_process');
var email_process = require('../businesses/email_process');
var notification_process = require('../businesses/notification_process');

router.get('/', function(req, res, next) {

    var name = "ship-time-text";
    db.getConfig(name,function (error, config) {
        if (error) {
            return;
        }
        var id = req.query.id;
        res.render('chon-phuong-thuc-thanh-toan', {
            i18n: res,
            order_id: id,
            ship_time: config[0].value

        });
    });


});

router.post('/getOrder', function(req, res, next) {
    var obj = req.body;
    var order_id = obj.order_id;

    order_process.getOrderInformation(order_id, req.session.locale, function (total_amount_after_product_promotion, ship_fee,
                                                          couponApplied, coupon_discount, orderDetails, orderInfo) {
        if (!orderDetails || orderDetails.length == 0) {
            res.json(api.getResponse(api.SUCC_EXEC, {
                products: [],
                total: 0
            }, ""));
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            products: orderDetails,
            total_amount_after_product_promotion: total_amount_after_product_promotion,
            phi_van_chuyen: ship_fee,
            couponApplied: couponApplied,
            coupon_discount: coupon_discount,
            final_total_amount: total_amount_after_product_promotion + ship_fee - coupon_discount
        }, ""));

    });
});

router.post('/checkLoaded', function(req, res, next) {

    var obj = req.body;
    var order_id = obj.order_id;

    db.getPurchaseOrderByID(order_id, function(error, orders) {
        if (error) {
            console.log(error);
            return;
        }

        if (orders != null && orders.length > 0) {
            if(orders[0].status != 5){
                res.json(api.getResponse(api.SUCC_EXEC, true, ""));
            }else {
                res.json(api.getResponse(api.SUCC_EXEC, false, ""));
            }

        }else {
            res.json(api.getResponse(api.SUCC_EXEC, false, ""));
        }
    });

});

router.post('/update', function(req, res, next) {

    var obj = req.body;
    var order_id = obj.id;
    var pay_method = obj.pay_method;

    var status = 1;
    if (pay_method == 2 || pay_method == 3) {
        status = 6;
    }
    db.updatePurchaseOrder(status,pay_method,order_id,function (error, rs) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, "", "Lỗi mạng"));
            return;
        }
        order_process.getOrderInformation(order_id, req.session.locale, function(total_amount_after_product_promotion, ship_fee,
                                                             couponApplied, coupon_discount, orderDetails, orderInfo) {


            var client_name = orderInfo.order_fullname;
            var client_email = orderInfo.order_email;
            var address = orderInfo.order_address_delivery;
            var phone = orderInfo.order_phone;
            var code = orderInfo.order_code;
            var now = api.getNowEmail();
            // var baseUrl = api.getServerAddressUpload(req);
            var baseUrl = "https://f7eda720.ngrok.io/upload/";
            // var oriUrl = api.getServerAddressBase(req);
            var oriUrl = "https://f7eda720.ngrok.io/";
            var payment_method = order_process.getPaymentMethodString(pay_method);
            var name = "ship-time-text";
            console.log("000")
            db.getConfig(name,function (error, config) {
                if (error) {
                    return;
                }
                var ship_time =  config[0].value;
                console.log("111")
                res.render('admin/email-confirm-order', {
                    client_name: client_name,
                    payment_method: payment_method,
                    code: code,
                    now: now,
                    address: address,
                    phone: phone,
                    products: orderDetails,
                    total_amount_after_product_promotion: total_amount_after_product_promotion,
                    ship_fee: ship_fee,
                    ship_time: ship_time,
                    base_url: baseUrl,
                    ori_url: oriUrl,
                    discount_voucher: coupon_discount,
                    final_total_amount: total_amount_after_product_promotion - ship_fee - coupon_discount
                }, function (err, html) {
                    email_process.sendEmail(client_email, "Thái Long xác nhận đặt hàng", html);
                    email_process.sendEmail(email_process.info_email_server, "Nhận 01 đơn hàng từ [" + client_email + "]", html);
                });

                notification_process.sendNotification(order_id, client_email, req.app.get('socket'));

            });

        });



        req.session.cart = null;
        res.json(api.getResponse(api.SUCC_EXEC, "san-pham", ""));


    })

});


module.exports = router;
