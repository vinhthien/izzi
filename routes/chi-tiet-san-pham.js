var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/:slug_product_title', function(req, res, next) {

    var slug_product_title = req.params.slug_product_title;
    console.log("slug_product_title "+ slug_product_title)
    db.getProductDetailsBySlug( req.session.locale, slug_product_title, function(error, contentList) {
        if (error) {
            return;
        }
        var content = contentList[0];
        if(content){
            content.order_quantity = 0;
        }

        if (req.session != null && req.session.cart != null && req.session.cart.length > 0) {
            for (var i = 0; i < req.session.cart.length; i++) {
                if (content.product_id == req.session.cart[i].product_id) {
                    content.order_quantity = req.session.cart[i].quantity
                }
            }
        }
        db.getProductImageList(content.product_id, function (error, contentList) {
            if (error) {
                return;
            }

            var link = api.getServerAddressBase(req) + "chi-tiet-san-pham/" + slug_product_title;
            res.render('chi-tiet-san-pham', {
                productDetails: content,
                imageList: contentList,
                product_id: content.product_id,
                i18n: res,
                link: link
            });

        });
    });

});

router.get('/', function(req, res, next) {

    var product_id = req.query.id;

    db.getProductDetailsByProductId( req.session.locale, product_id, function(error, contentList) {
        if (error) {
            return;
        }

        if (contentList && contentList.length > 0) {
            res.redirect("/chi-tiet-san-pham/" + contentList[0].product_slug);
        }

    });

});



router.post('/countAllMonAnNgonLienQuanSanPham', function(req, res, next) {

    var product_id = req.body.product_id;


    db.countAllMonAnNgonLienQuanSanPham(product_id, function(error, result) {
        if (error) {
            return;
        }

        var sum = result[0].SUM;
        var nbItemsOfPage = 4;
        var numberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentTotalPages: numberOfPages,
            nbItemsOfPage: nbItemsOfPage
        }, ""));

    });
});

router.post('/getContentList', function(req, res, next) {

    var product_id = req.body.product_id;
    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;
    db.getAllMonAnNgonLienQuanSanPhamPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, product_id,  function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
            for(var index=0;index<contentList.length;index++){
                contentList[index].content_created_date = api.revertDatePromotionFrontEnd(contentList[index].content_created_date);
            }
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getRelatedProducts', function(req, res, next) {

    var product_id = req.body.product_id;
    var product_category_id = req.body.product_category_id;


    db.getAllSanPhamLienQuan(req.session.locale, product_category_id, product_id, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getWishProducts', function(req, res, next) {

    if (req.session.seenProducts == null) {
        req.session.seenProducts = [];
    }

    var productIDs = ['']; // avoid syntax in sql
    for (var i = 0; i < req.session.seenProducts.length; i++) {
        productIDs.push(req.session.seenProducts[i].product_id);
    }

    db.getAllWishProducts(req.session.locale, productIDs, function (error1, listContent) {
        if (error1) {
            return;
        }
        var contentList = [];

        if (listContent != null && listContent.length > 0) {
            contentList = listContent;
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            contentList: contentList
        }, ""));
    });
});

router.post('/getProduct', function(req, res, next) {
    var product_id = req.body.id;

    db.getProductDetailsByProductId( req.session.locale, product_id, function(error, contentList) {
        if (error) {
            return;
        }

        var content = contentList[0];
        content.order_quantity = 0;
        for (var i = 0; i < req.session.cart.length; i++) {
            if (content.product_id == req.session.cart[i].product_id) {
                content.order_quantity = req.session.cart[i].quantity
            }
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            productDetails: content
        }, ""));

    });

});

router.post('/getMeta', function(req, res, next) {
    var product_id = req.body.product_id;
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.PRD,req.session.locale,product_id,function (error, meta) {
       if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});

router.post('/getCategory', function(req, res, next) {
    var categoryID = req.body.id;
    db.getCategoryWithLanguage(categoryID, req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }
        console.log("contentList "+ JSON.stringify(contentList));
        res.json(api.getResponse(api.SUCC_EXEC, {
            category : contentList[0]
        }, ""));

    });

});


module.exports = router;
