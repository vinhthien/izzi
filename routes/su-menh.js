var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render("su-menh", {
        i18n: res
    });
});

module.exports = router;
