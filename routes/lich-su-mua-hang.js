var express = require('express');
var router = express.Router();
var db = require('../databases/database');


router.get('/', function(req, res, next) {
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;

    db.getPersonById(user_id,function (error, person) {
        if(error) return;
        var email = person[0].email;
        db.getPurchaseOrderHistory(email,function (error1, history) {
            if(error1) return;
            res.render("lich-su-mua-hang", {
                i18n: res,
                history: history,
                person: person[0]
            });

        })

    })


});

module.exports = router;
