var express = require('express');
var router = express.Router();
var api = require('../api/response');
var db = require('../databases/database');


router.get('/', function(req, res, next) {

    var tags = req.query.tags;
    var option = req.query.option;
    if(option == 0){ // all products
        db.countSearchAllProducts(tags, function (error, result) {
            if (error) {
                console.log(error);
                return;
            }

            var sum = result[0].SUM;
            var nbItemsOfPage = 12;
            var promotionNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;


            res.render('ket-qua-tim-kiem', {
                i18n: res,
                tags: tags,
                option: option,
                promotionTotalPages: promotionNumberOfPages,
                nbItemsOfPage: nbItemsOfPage

            });
        })
    }else if (option == 1){ // promotion only
        db.countSearchAllProductsPromotions(tags, function(error, result) {
            if (error) {
                console.log(error);
                return;
            }

            var sum = result[0].SUM;
            var nbItemsOfPage = 12;
            var promotionNumberOfPages = parseInt((sum - 1) / nbItemsOfPage) + 1;

            res.render('ket-qua-tim-kiem', {
                i18n: res,
                tags: tags,
                option: option,
                promotionTotalPages: promotionNumberOfPages,
                nbItemsOfPage: nbItemsOfPage

            });

        });
    }



});


router.post('/search', function(req, res, next) {

    var page = req.body.page;
    var nbItemsOfPage = req.body.nbItemsOfPage;
    var option = req.body.option;
    var tags = req.body.tags;
    if(option == 0){ // all products
        db.searchAllProductPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, tags, function (error1, listContent) {
            if (error1) {
                console.log(error1);
                return;
            }
            var contentList = [];
            if (listContent != null && listContent.length > 0) {
                contentList = listContent;
            }

            res.json(api.getResponse(api.SUCC_EXEC, {
                contentList: contentList
            }, ""));
        });
    }else if(option == 1){ // promotion only
        db.searchAllProductsPromotionPagingWithLanguage((page - 1) * nbItemsOfPage, nbItemsOfPage, req.session.locale, tags, function (error1, listContent) {
            if (error1) {
                console.log(error1);
                return;
            }
            var contentList = [];
            if (listContent != null && listContent.length > 0) {
                contentList = listContent;
            }

            res.json(api.getResponse(api.SUCC_EXEC, {
                contentList: contentList
            }, ""));
        });
    }

});



module.exports = router;
