var express = require('express');
var router = express.Router();
var db = require('../databases/database');

var api = require('../api/response');

router.get('/:slug_policy_title', function(req, res, next) {
    var slug_policy_title = req.params.slug_policy_title;
    db.getChinhSachContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }

        var mainContent = null;
        for (var index = 0; index < contentList.length; index++) {

            if (slug_policy_title == null) {
                mainContent = contentList[index];
                break;
            }
            if (contentList[index].content_slug == slug_policy_title) {
                mainContent = contentList[index];
                break;
            }

        }
        res.render("chinh-sach", {
            mainContent: mainContent,
            contentList: contentList,
            i18n: res
        });

    });

});
router.post('/getMeta', function(req, res, next) {
    var content_id = req.body.content_id;
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MAN,req.session.locale,content_id,function (error, meta) {
        if(error) return;
        res.json(api.getResponse(api.SUCC_EXEC, meta[0], ""));
    });

});


router.get('/', function(req, res, next) {
    var contentID = req.query.id;
    db.getChinhSachContentWithLanguage(req.session.locale, function(error, contentList) {
        if (error) {
            return;
        }

        var mainContent = null;
        for (var index = 0; index < contentList.length; index++) {

            if (contentID == null) {
                mainContent = contentList[index];
                break;
            }
            if (contentList[index].content_id == contentID) {
                mainContent = contentList[index];
                break;
            }

        }
        res.render("chinh-sach", {
            mainContent: mainContent,
            contentList: contentList,
            i18n: res
        });

    });

});

module.exports = router;
