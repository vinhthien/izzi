var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');


router.get('/', function (req, res, next) {
    var username = req.session.enduser_username;
    var user_id = req.session.enduser_user_id;
    var person_id = req.query.id;
    db.getLookupData(null, api.ENUM_ENTITY_CODE.TTP, function (error2, city_list) {
        db.getPersonAddressById(person_id, function (error, personAddress) {
            var entity_code_district = api.ENUM_ENTITY_CODE.QUH;
            var entity_code_ward = api.ENUM_ENTITY_CODE.PHU;

            db.getLookupData(personAddress[0].city_id,entity_code_district,function (error3, district_list) {
                if(error3){
                    return;
                }
                db.getLookupData(personAddress[0].district_id,entity_code_ward,function (error4, ward_list) {
                    if(error4){
                        return;
                    }
                    if(JSON.parse(JSON.stringify(personAddress[0].is_default)).data == 1){
                        personAddress[0].is_default = 1;
                    }else {
                        personAddress[0].is_default = 0;
                    }
                    res.render("thay-doi-so-dia-chi", {
                        i18n: res,
                        personAddress: personAddress[0],
                        user_id: user_id,
                        city_list: city_list,
                        district_list: district_list,
                        ward_list: ward_list
                    });

                })
            })



        })
    });


});


router.post('/get_district', function (req, res, next) {
    var obj = req.body;
    var city_id = obj.city_id;
    var entity_code = api.ENUM_ENTITY_CODE.QUH;
    db.getLookupData(city_id, entity_code, function (error, district_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            district_list: district_list
        }, ""));
    })

});

router.post('/remove-person-address', function (req, res, next) {
    var obj = req.body;
    var person_address_id = obj.person_address_id;
    var person_id = req.session.enduser_user_id;

    var created_at = api.getNow();
    var created_by = person_id;
    db.removePersonAddress(person_address_id,created_at,created_by, function (error, district_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));
    })
});

router.post('/add_info', function (req, res, next) {
    var obj = req.body;
    var fullName = obj.fullName;
    var phoneNumber = obj.phoneNumber;
    var email = obj.email;
    var city_id = obj.city_id;
    var district_id = obj.district_id;
    var ward_id = obj.ward_id;
    var street = obj.street;
    var address = obj.address;
    var is_default = obj.is_default;
    if (is_default) {
        is_default = 1;
    } else {
        is_default = 0;
    }
    var entity_code = api.ENUM_ENTITY_CODE.QUH;
    var id = uuidv1();
    var person_id = req.session.enduser_user_id;
    var created_at = api.getNow();
    var created_by = person_id;
    db.insertPersonAddress(id, person_id, fullName, city_id, district_id, ward_id, street, email, phoneNumber, address, is_default, created_by, created_at, 1, function (error, rs) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, rs, ""));
    })

});

router.post('/delete_info', function (req, res, next) {
    var obj = req.body;
    var person_address_id = obj.person_address_id;
    var person_id = req.session.enduser_user_id;

    var created_at = api.getNow();
    var created_by = person_id;
    db.removePersonAddress(person_address_id, created_at, created_by, person_address_id, function (error, rs) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, rs, ""));
    })




});

router.post('/update_info', function (req, res, next) {
    var obj = req.body;
    var fullName = obj.fullName;
    var phoneNumber = obj.phoneNumber;
    var email = obj.email;
    var city_id = obj.city_id;
    var district_id = obj.district_id;
    var ward_id = "";
    var street = "";
    var address = obj.address;
    var is_default = obj.is_default;
    var person_address_id = obj.person_address_id;
    var person_id = req.session.enduser_user_id;

    function update_default(is_default,callback) {

        if (is_default) {
            db.updatePersonAddressDefault(person_id,function (error0, rs) {
                if(error0){
                    return;
                }
                callback(1);
            })
        } else {
            callback(0);
        }
    }
    update_default(is_default,function (is_default) {
        var created_at = api.getNow();
        var created_by = person_id;
        db.updatePersonAddress(person_id, fullName, city_id, district_id, ward_id, street, email, phoneNumber, address, is_default, created_at, created_by, 1, person_address_id, function (error, rs) {
            if (error) {
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, rs, ""));
        })
    })




});

router.post('/get_ward', function (req, res, next) {
    var obj = req.body;
    var district_id = obj.district_id;
    var entity_code = api.ENUM_ENTITY_CODE.PHU;
    db.getLookupData(district_id, entity_code, function (error, ward_list) {
        if (error) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            ward_list: ward_list
        }, ""));
    })

});
module.exports = router;
