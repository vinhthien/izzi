var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var nodemailer = require('nodemailer');

// GET home page
router.get('/', function (req, res, next) {
    var promotion_popup_delay_show = "promotion_popup_delay_show";
    var promotion_popup_content = "promotion_popup_content";
    var promotion_popup_show = "promotion_popup_show";

    db.getConfig(promotion_popup_delay_show,function (error, delay_show) {
        if(error) {
            return;
        }
        db.getConfig(promotion_popup_content,function (error1, contentHTML) {
            if (error1) {
                return;
            }

            db.getConfig(promotion_popup_show,function (error2, show) {
                if (error2) {
                    return;
                }

                res.render('admin/quan-ly-popup-khuyen-mai', {
                    delayShow: delay_show[0].value,
                    contentHTML: contentHTML[0].value,
                    hienPopup: show[0].value
                });
            });

        });
    });


});

router.post('/sua', function (req, res, next) {

    var obj = req.body;
    var delayStart = obj.delayStart;
    var content = obj.content;
    var show = obj.show;

    var promotion_popup_delay_show = "promotion_popup_delay_show";
    var promotion_popup_content = "promotion_popup_content";
    var promotion_popup_show = "promotion_popup_show";

    db.updateConfig(delayStart,promotion_popup_delay_show,function (error, rs) {
        if(error) {
            return;
        }
        db.updateConfig(content,promotion_popup_content,function (error1, rs1) {
            if (error1) {
                return;
            }
            db.updateConfig(show,promotion_popup_show,function (error1, rs1) {
                if (error1) {
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            });
        });
    });

});

module.exports = router;
