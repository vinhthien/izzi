var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    db.getHomeSliderSpeed(function (error, obj) {
        if (error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, ""));
            return;
        }
        res.render('admin/quan-ly-toc-do-slider-trang-chu', {
            speed: obj[0]
        });

    })
});





router.post('/capnhat', function (req, res, next) {

    var obj = req.body;
    var speed = obj.speed;
    db.updateHomeSliderSpeed(speed, function (error, rs) {
        if (error) return;

        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })



    


});

module.exports = router;
