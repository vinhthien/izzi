var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();


// GET home page
router.get('/', function (req, res, next) {

    res.render('admin/chinh-sach-chat-luong', {});

});

router.post('/uploader', multipartMiddleware, function(req, res) {

    fs.readFile(req.files.upload.path, function (err, data) {
        var form = new formidable.IncomingForm(),
            files = [],
            fields = [];

        form.uploadDir = "upload";
        var newPath = form.uploadDir + "/" + req.files.upload.name;
        fs.writeFile(newPath, data, function (err) {
            if (err) console.log({err: err});
            else {
                html = "";
                html += "<script type='text/javascript'>";
                html += "    var funcNum = " + req.query.CKEditorFuncNum + ";";
                html += "    var url     = \"/upload/" + req.files.upload.name + "\";";
                html += "    var message = \"Uploaded file successfully\";";
                html += "";
                html += "    window.parent.CKEDITOR.tools.callFunction(funcNum, url, message);";
                html += "</script>";
                res.send(html);
            }
        });
    });
});
router.post('/sua', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết session làm việc"));
        return;
    }
    var obj = req.body;
    var html_vi = obj.html_vi;
    var html_en = obj.html_en;
    var title = obj.title;
    var image = obj.image;
    var id = obj.id;
    var entity_code = api.ENUM_ENTITY_CODE.GTU;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;

    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    var rating = 0;
    db.updateGioiThieuContent(entity_code, updated_by, updated_date, "", html_vi, status, image, id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }

        db.updateMucDuLieuGioiThieuMeta(id, html_vi, updated_by, updated_date, entity_code, 'vi', meta_keyword_vi,meta_description_vi,meta_title_vi, function (e1, ressult) {
            if (e1) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhậ vào mục dữ liệu tiếng việt"));
                return;
            }
            db.updateMucDuLieuGioiThieuMeta(id, html_en, updated_by, updated_date, entity_code, 'en', meta_keyword_en,meta_description_en,meta_title_en, function (e3, ressult) {
                if (e3) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật vào mục dữ liệu tiếng anh"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));

            });

        });



    })


});

router.post('/upload', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});
module.exports = router;
