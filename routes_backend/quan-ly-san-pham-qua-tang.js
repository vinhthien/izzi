var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {

    db.getSanPhamNotInGiftCategory(function (error, sanpham) {
        if(error) return;
        db.getSanPhamInGiftCategory(function (error1, bestSell) {
            if(error1) return;
            res.render('admin/quan-ly-san-pham-qua-tang', {
                sanpham: sanpham,
                bestSell: bestSell
            });

        })

    })

});


router.post('/them', function (req, res, next) {
    var obj = req.body;
    var product_id = obj.product_id;
    var order = obj.order;
    order = Number(order);
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    db.updateToProductGift(product_id,updated_date, updated_by,function (error, countResult) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, "insert", ""));
    })
});

router.post('/count', function (req, res, next) {
    var obj = req.body;
    var product_id = obj.product_id;
    db.countProductGiftExisting(product_id,function (error, countResult) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, countResult[0].count, "Lỗi mạng"));

    })
});

router.post('/xoa', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    db.removeSanPham(id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

module.exports = router;
