var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {

    res.render('admin/them-thuong-hieu');
});


router.post('/them', function (req, res, next) {

    var obj = req.body;
    var title = obj.name;
    var id = uuidv1();
    var entity_code = api.ENUM_ENTITY_CODE.BND;
    var created_date = api.getNow();
    var created_by = req.session.username;
    var status = 1;
    db.countBrandByName(title,entity_code,function (er, codeRs) {
        if(er){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(codeRs[0].count > 0){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Thương hiệu này đã được sử dụng"));
            return;
        }else {
            db.insertLookupData(id,title,null,null,created_date,created_by,entity_code,status,null,function (error,rs) {
                if (error) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, id, ""));
            })
        }
    })

});


module.exports = router;
