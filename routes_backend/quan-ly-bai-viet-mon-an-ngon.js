var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {

    var id = req.query.id;
    db.getMonNgonByType(null,function (error, normalMonNgon) {
        if (error) return;

        db.getMonNgonByType(2,function (error1, topMonNgon) {
            if(error1) return;
            res.render('admin/quan-ly-bai-viet-mon-an-ngon', {
                normalMonNgon: normalMonNgon,
                topMonNgon: topMonNgon
            });
        })
    })



});




router.post('/them', function (req, res, next) {

    var obj = req.body;
    var content_id = obj.content_id;

    db.updateMonNgonType(2,content_id,function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    })

});


router.post('/xoa', function (req, res, next) {

    var obj = req.body;
    var content_id = obj.content_id;

    db.updateMonNgonType(null,content_id,function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    })

});

router.post('/sua', function (req, res, next) {
    console.log("IN");
    var obj = req.body;
    var type = obj.type;
    var code = obj.code;
    var name = obj.name;
    var beginDate = obj.beginDate;
    var endDate = obj.endDate;
    var giaThap = obj.giaThap;
    var giaCao = obj.giaCao;
    var order = obj.order;
    var entity_code = obj.entity_code;
    var promotion_id = obj.promotion_id;
    var image = obj.image;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    beginDate = api.convertDate(beginDate);
    endDate = api.convertDate(endDate);
    db.updatePromotion(code, name, beginDate, endDate, giaThap, giaCao, image, updated_by, updated_date, order, promotion_id, "", function (e, rs) {
        if (e) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }

        if (type == 3) {
            var gift = obj.gift;
            db.updateMucDuLieu(promotion_id, null, gift, null, null, updated_by, updated_date, entity_code, "vi", function (e1, rs1) {
                if (e1) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu tiếng việt"));
                    return;
                }
                db.updateMucDuLieu(promotion_id, null, gift, null, null, updated_by, updated_date, entity_code, "en", function (e2, rs2) {
                    if (e2) {
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu anh"));
                        return;
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));

                })
            })
        } else if (type == 4) {
            var coupon = obj.coupon;
            db.updatePromotion(code, name, beginDate, endDate, giaThap, giaCao, image, updated_by, updated_date, order, promotion_id, coupon, function (e4, rs4) {
                if (e4) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            });

        } else if (type == 1 || type == 2) {

            res.json(api.getResponse(api.SUCC_EXEC, null, ""));

        }


    })





});


module.exports = router;
