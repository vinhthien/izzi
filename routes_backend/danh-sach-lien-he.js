var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

// GET home page
router.get('/', function (req, res, next) {

    db.getDanhSachLienHe(function (error, data) {
        if(error){
            return;
        }
        console.log(data);
        for(var index=0;index<data.length;index++){
            data[index].created_date = api.revertDatePromotionFrontEnd(data[index].created_date);
        }
        res.render('admin/danh-sach-lien-he', {data:data});
    })



});

router.post('/da-lien-he', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;

    db.updateLienHe(2,id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

module.exports = router;
