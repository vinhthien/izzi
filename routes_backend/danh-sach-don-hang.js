var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var nodemailer = require('nodemailer');
const uuidv1 = require('uuid/v1');
var pug = require('pug');
var moment = require('moment');


var order_process = require('../businesses/order_process');
var email_process = require('../businesses/email_process');
var warehouse_process = require('../businesses/warehouse_process');
var promotion_process = require('../businesses/promotion_process');


// GET home page
router.get('/', function (req, res, next) {

    db.getPurchaseOrder(function (error, data) {
        if (error) {
            return;
        }
        var promotion_type = [];
        for(var index=0;index<data.length;index++){
            var content = data[index];
            var type = null;
            if (content.percent_promotion == null && content.price_promotion == null) {
                type = 3;
            } else if (content.percent_promotion == null && content.price_promotion != null) {
                type = 1;
            } else if (content.percent_promotion != null) {
                type = 2;
            }
            promotion_type.push({type: type});
            data[index].created_date = moment(data[index].created_date,"YYYY-MM-DDTHH:mm:ss.SSSZ").format("DD-MM-YYYY HH:mm:ss");
        }
        var statuses = [
            {value: 1, name: "Đơn hàng mới", relate: [2, 3, 4, 7]},
            {value: 2, name: "Nhân viên đã gọi xác nhận", relate: [3, 4, 7]},
            {value: 3, name: "Đã xuất hàng", relate: [4, 7]},
            {value: 4, name: "Đã giao và thanh toán", relate: []},
            {value: 5, name: "Đang tạo đơn hàng", relate: [7]},
            {value: 6, name: "Đang thanh toán", relate: [7]},
            {value: 7, name: "Đã huỷ đơn hàng", relate: []}
        ];
        res.render('admin/danh-sach-don-hang', {
            data: data,
            data_text: JSON.stringify(data),
            statuses: statuses,
            payment_methods: [
                {value: 1, name: "COD"},
                {value: 2, name: "Thẻ ATM"},
                {value: 3, name: "Thẻ quốc tế"}
            ]
        });
    });
});




router.post('/getPurchaseDetails', function (req, res, next) {
    var obj = req.body;
    var order_id = obj.order_id;
    console.log("order_id "+ order_id);
    order_process.getOrderInformation(order_id, req.session.locale, function (total_amount_after_product_promotion, ship_fee,

                                                                                    couponApplied, coupon_discount, orderDetails, orderInfo) {
        if (!orderDetails) {
            orderDetails = [];
        }




        var order_detail_ids = [];
        orderDetails.forEach(function (detail) {
            if (detail.order_detail_id) {
                order_detail_ids.push(detail.order_detail_id);
            }
        });
        res.json(api.getResponse(api.SUCC_EXEC, {
            products: orderDetails,
            orderInfo: orderInfo,
            total_amount_after_product_promotion: total_amount_after_product_promotion,
            phi_van_chuyen: ship_fee,
            couponApplied: couponApplied,
            coupon_discount: coupon_discount,
            final_total_amount: total_amount_after_product_promotion + ship_fee - coupon_discount
        }, ""));
    });
});

router.post('/getPurchaseDetailsFromArray', function (req, res, next) {

    console.log("000");
    var obj = req.body;
    var order_id_array = obj.order_id_array;
    console.log("222");
    order_process.getOrderArrayInformation(order_id_array,req.session.locale,function (array) {
        res.json(api.getResponse(api.SUCC_EXEC, array, ""));
    })

    return;
    order_process.getOrderInformation(order_id, req.session.locale, function (total_amount_after_product_promotion, ship_fee,
                                                                              couponApplied, coupon_discount, orderDetails, orderInfo) {
        console.log("333");
        if (!orderDetails) {
            orderDetails = [];
        }

        var order_detail_ids = [];
        orderDetails.forEach(function (detail) {
            if (detail.order_detail_id) {
                order_detail_ids.push(detail.order_detail_id);
            }
        });
        if(index==(order_id_array.length-1)){
            res.json(api.getResponse(api.SUCC_EXEC, temp, ""));
            return;
        }else {
            temp.push({
                products: orderDetails,
                orderInfo: orderInfo,
                total_amount_after_product_promotion: total_amount_after_product_promotion,
                phi_van_chuyen: ship_fee,
                couponApplied: couponApplied,
                coupon_discount: coupon_discount,
                final_total_amount: total_amount_after_product_promotion + ship_fee - coupon_discount
            });
        }

    });
});

router.post('/getTableExcel', function (req, res, next) {

    var obj = req.body;
    var data = obj.data;
    console.log(data);
});
router.post('/getGiftProduct', function (req, res, next) {
    var obj = req.body;
    var purchase_order_detail_id = obj.purchase_order_detail_id;
    order_process.getProductGiftByOrderDetailIds([purchase_order_detail_id], function(orderDetailGifts) {
        if (orderDetailGifts) {
            res.json(api.getResponse(api.SUCC_EXEC, orderDetailGifts, ""));
        }
    }, function () {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
    });
});

router.post('/update', function (req, res, next) {
    var obj = req.body;
    var order_id = obj.order_id;
    var old_status = obj.old_status;
    var new_status = obj.new_status;
    var name = obj.name;
    var email = obj.email;

    function send_mail(res, req) {


        order_process.getOrderInformation(order_id, "vi", function(total_amount_after_product_promotion, ship_fee,
                                                             couponApplied, coupon_discount, orderDetails, orderInfo) {


            var client_name = orderInfo.order_fullname;
            var client_email = orderInfo.order_email;
            var address = orderInfo.order_address_delivery;
            var phone = orderInfo.order_phone;
            var code = orderInfo.order_code;
            var now = api.getNowEmail();
            var baseUrl = api.getServerAddressUpload(req);
            var oriUrl = api.getServerAddressBase(req);
            // var baseUrl = "https://f7eda720.ngrok.io/upload/";
            // var oriUrl = "https://f7eda720.ngrok.io/";
            var url = api.getServerAddressBase(req);

            var payment_method = order_process.getPaymentMethodString(orderInfo.payment_method);

            res.render('admin/email-successfull-delevery', {
                client_name: client_name,
                payment_method: payment_method,
                code: code,
                now: now,
                address: address,
                phone: phone,
                products: orderDetails,
                total_amount_after_product_promotion: total_amount_after_product_promotion,
                ship_fee: ship_fee,
                base_url: baseUrl,
                ori_url: oriUrl,
                url: url,
                discount_voucher: coupon_discount,
                final_total_amount: total_amount_after_product_promotion - ship_fee - coupon_discount
            }, function (err, html) {
                email_process.sendEmail(client_email, "Thái Long giao hàng thành công", html);
            });



        });





    }


    db.updatePurchaseOrderStatus(new_status, order_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }

        if (result) {

            if (new_status == 4) { // Đã giao và thanh toán
                if (old_status != 3) { // Chưa xuất hàng
                    warehouse_process.exportWarehouse(order_id, req.session.username);
                }

                send_mail(res, req);

            } else if (new_status == 3) { // Đã xuất hàng
                warehouse_process.exportWarehouse(order_id, req.session.username);

            } else if (new_status == 7) { // Hủy đơn hàng
                if (old_status == 3 || old_status == 4) { // Chưa xuất hàng
                    warehouse_process.returnWarehouse(order_id, req.session.username);
                }

            }
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }
    });

});

module.exports = router;
