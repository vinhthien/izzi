var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    res.render('admin/them-tin-tuc-su-kien', {});
});

module.exports = router;
