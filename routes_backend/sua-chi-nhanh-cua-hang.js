var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {

    var id = req.query.id;
    db.getChiNhanhCuaHangById(id, function (error, chinhanh) {
        if (error) {
            return;
        }
        console.log(chinhanh);
        res.render('admin/sua-chi-nhanh-cua-hang', {
            data: chinhanh[0]
        });

    });

});


router.post('/sua', function (req, res, next) {
    var obj = req.body;

    var entity_code = api.ENUM_ENTITY_CODE.BRA;
    var updated_date = api.getNow();
    var updated_by = req.session.username;

    db.updateChiNhanhCuaHang(obj.address,obj.toado,updated_by,updated_date,entity_code, obj.id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })


});


module.exports = router;
