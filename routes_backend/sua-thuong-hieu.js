var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {

    var id = req.query.id;
    db.getLookupDataById(id,function (error, data) {
        if(error) return;
        res.render('admin/sua-thuong-hieu', {
            data: data[0]
        });
    })




});







router.post('/sua', function (req, res, next) {
    var obj = req.body;
    var title = obj.title;
    var brand_id = obj.brand_id;
    var entity_code = api.ENUM_ENTITY_CODE.BND;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    var rating = 0;

    db.updateLoopkupData(title,null,null,updated_by,updated_date,null,entity_code,brand_id, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        })
});


module.exports = router;
