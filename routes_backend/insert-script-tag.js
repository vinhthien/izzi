var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

router.get('/', function (req, res, next) {
    var entity_code = api.ENUM_ENTITY_CODE.SHT;
    db.getScript(entity_code,function (error, headscript) {
        if (error) {

            return;
        }

        console.log(8378783);
        console.log(headscript[0]);
        res.render('admin/insert-script-tag', {headscript: headscript[0]});
    });
});


router.post('/sua', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Hết phiên làm việc."));
        return;
    }

    var obj = req.body;
    var headScript = obj.headScript;
    // var id = uuidv1();
    var created_date = api.getNow();
    var created_by = req.session.username;
    var entity_code = api.ENUM_ENTITY_CODE.SHT;
    // var status=1;

    db.updateScript(headScript, entity_code, created_date, created_by, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));
    });
});

module.exports = router;
