var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {
    var call_name = "front_end_sms_number_footer";
    db.getConfig(call_name,function (error, call_number) {
        if(error) return;
        res.render('admin/quan-ly-tin-nhan-tong-dai', {
            call_number: call_number[0]
        });
    })
});


router.post('/update', function (req, res, next) {

    var obj = req.body;
    var number = obj.number;
    var name = "front_end_sms_number_footer";
    db.updateConfig(number,name,function (error2, rs2) {
        if(error2) return;
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })



});




module.exports = router;
