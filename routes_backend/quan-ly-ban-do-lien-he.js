var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {
    var lat_name = "front_end_map_latitude_footer";
    var long_name = "front_end_map_longitude_footer";
    db.getConfig(lat_name,function (error, lat) {
        if(error) return;
        db.getConfig(long_name,function (error1, long) {
            if (error1) return;
            res.render('admin/quan-ly-ban-do-lien-he', {
                lat: lat[0],
                long: long[0]
            });
        })

    })
});


router.post('/update', function (req, res, next) {

    var obj = req.body;
    var long = obj.long;
    var lat = obj.lat;
    var lat_name = "front_end_map_latitude_footer";
    var long_name = "front_end_map_longitude_footer";
    db.updateConfig(lat,lat_name,function (error2, rs2) {
        if(error2) return;
        db.updateConfig(long,long_name,function (error3, rs3) {
            if (error3) return;
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));

        })

    })



});




module.exports = router;
