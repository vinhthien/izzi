var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

// GET home page
router.get('/', function (req, res, next) {

    db.getAllChiNhanhCuaHang(api.ENUM_ENTITY_CODE.BRA, function (error, data) {
        if(error){
            return;
        }
        res.render('admin/danh-sach-chi-nhanh-cua-hang', {data:data});
    })


});

router.post('/xoa', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    db.removeChiNhanhCuaHang(id,function (error,result) {
       if(error){
           return;
       }
       res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    });


});

module.exports = router;
