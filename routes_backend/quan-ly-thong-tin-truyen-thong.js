var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {
    var facebook_name = "contact-facebook";
    var youtube_name = "contact-youtube";
    var zalo_name = "contact-zalo";

    db.getConfig(facebook_name,function (error, facebook) {
        if(error) return;
        db.getConfig(youtube_name,function (error1, youtube) {
            if(error1) return;
            db.getConfig(zalo_name,function (error2, zalo) {
                if(error2) return;

                res.render('admin/quan-ly-thong-tin-truyen-thong', {
                    facebook: facebook[0],
                    youtube: youtube[0],
                    zalo: zalo[0]
                });
            })
        })

    })
});


router.get('/get', function (req, res, next) {
    console.log("IN 1")
    var facebook_name = "contact-facebook";
    var youtube_name = "contact-youtube";
    var zalo_name = "contact-zalo";
    db.getConfig(facebook_name,function (error, facebook) {
        if(error) return;
        db.getConfig(youtube_name,function (error1, youtube) {
            if(error1) return;
            db.getConfig(zalo_name,function (error2, zalo) {
                if(error2) return;
                res.json({
                    facebook: facebook[0],
                    youtube: youtube[0],
                    zalo: zalo[0]
                });
            })
        })

    })
})

router.post('/update', function (req, res, next) {

    var obj = req.body;
    var facebook_value = obj.facebook;
    var youtube_value = obj.youtube;
    var zalo_value = obj.zalo;
    var facebook_name = "contact-facebook";
    var youtube_name = "contact-youtube";
    var zalo_name = "contact-zalo";
    db.updateConfig(facebook_value,facebook_name,function (error, rs) {
        if(error) return;
        db.updateConfig(youtube_value,youtube_name,function (error1, rs1) {
            if(error1) return;
            db.updateConfig(zalo_value,zalo_name,function (error2, rs2) {
                if(error2) return;
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));

            })
        })
    })



});




module.exports = router;
