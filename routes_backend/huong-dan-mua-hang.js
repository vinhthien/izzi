var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');

// GET home page
router.get('/', function (req, res, next) {
    if(req.session.token && req.session.email) {
        var email = req.session.email;
        var token = req.session.token;

        res.render('huong-dan-mua-hang', {
            email: email,
            token: token
        });

    }else {
        utility.renderLogin(res);
    }
});

module.exports = router;
