var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var nodemailer = require('nodemailer');

// GET home page
router.get('/', function (req, res, next) {
    var subject_name = "email_subject_editor";
    var content_name = "content_subject_editor";
    var email_name = "email_for_host";
    var password_name = "password_for_host";
    var mail_host_url = "mail_host_url";
    var promotion_for_user_signup_first_time = "promotion_for_user_signup_first_time";




    db.getPromotionCanUseOneTime(function (error7, promotion) {
        if (error7) return;
        db.getConfig(subject_name, function (error, subjectObj) {
            if (error) return;
            db.getConfig(content_name, function (error1, contentObj) {
                if (error1) return;
                            db.getConfig(promotion_for_user_signup_first_time, function (error4, promotion_hold) {
                                if (error4) return;

                                // var html = contentObj[0].value;
                                // console.log("html "+ html);
                                // var resa = html.replace("/upload/", url);
                                // console.log("resa "+ resa);
                                // api.sendEmail(nodemailer,"siamopizza.com","25",false,"chatserver@hablax.com","hablaxChat!","dnduc456@gmail.com","Test new email",resa);

                                res.render('admin/soan-thao-email',
                                    {
                                        subject: subjectObj[0].value,
                                        content: contentObj[0].value,
                                        promotion: promotion,
                                        promotion_id: promotion_hold[0].value
                                    });

                            })
            })
        })
    })


});

router.post('/sua', function (req, res, next) {

    var obj = req.body;
    var subject = obj.subject;
    var content = obj.content;
    var email = obj.email;
    var password = obj.password;
    var host = obj.host;
    var promotion_id = obj.promotion_id;
    var subject_name = "email_subject_editor";
    var content_name = "content_subject_editor";
    var email_name = "email_for_host";
    var password_name = "password_for_host";
    var mail_host_url = "mail_host_url";
    var promotion_for_user_signup_first_time = "promotion_for_user_signup_first_time";

    db.updateConfig(subject,subject_name,function (error, rs) {
        if(error) return;
        db.updateConfig(content,content_name,function (error1, rs1) {
            if (error1) return;
            db.updateConfig(promotion_id, promotion_for_user_signup_first_time, function (error3, rs1) {
                if (error3) return;
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            })
        })
    })



});

router.post('/upload', function (req, res, next) {
    console.log("IN");
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


module.exports = router;
