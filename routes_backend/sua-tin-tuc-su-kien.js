var express = require('express');
var router = express.Router();

router.get('/', function (req, res, next) {
    var id = req.query.id;
    res.render('admin/sua-tin-tuc-su-kien', {
        content_id: id
    });

});

module.exports = router;
