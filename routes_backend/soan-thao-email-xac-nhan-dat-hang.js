var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');

// GET home page
router.get('/', function (req, res, next) {
    var subject_name = "email_accept_subject_editor";
    var content_name = "content_accept_subject_editor";
    var email_name = "email_accept_for_host";
    var password_name = "password_accept_for_host";
    var mail_host_url = "mail_accept_host_url";
    db.getConfig(subject_name,function (error, subjectObj) {
        if(error) return;
        db.getConfig(content_name,function (error1, contentObj) {
            if (error1) return;
            res.render('admin/soan-thao-email-xac-nhan-dat-hang',
                {
                    subject: subjectObj[0].value,
                    content: contentObj[0].value
                });

        })
    })


});


router.post('/sua', function (req, res, next) {

    var obj = req.body;
    var subject = obj.subject;
    var content = obj.content;
    var subject_name = "email_accept_subject_editor";
    var content_name = "content_accept_subject_editor";
    db.updateConfig(subject,subject_name,function (error, rs) {
        if(error) return;
        db.updateConfig(content,content_name,function (error1, rs1) {
            if (error1) return;
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        })
    })



});

router.post('/upload', function (req, res, next) {
    console.log("IN");
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


module.exports = router;
