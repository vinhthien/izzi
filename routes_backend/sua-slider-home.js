var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug');

// GET home page
router.get('/', function (req, res, next) {
    var id = req.query.id;
    db.getSliderById(id, function (error, listContent) {
        if (error) {
            return;
        }
        db.getMucDuLieu(listContent[0].requiring_entity_code, "vi", id, function (error3, contentVI) {
            if (error3) {
                return;
            }
            db.getMucDuLieu(listContent[0].requiring_entity_code, "en", id, function (error4, contentEN) {
                if (error4) {
                    return;
                }
                console.log("contentVI "+  JSON.stringify(contentVI[0]));
                console.log("contentEN "+  JSON.stringify(contentEN[0]));
                res.render('admin/sua-slider-home', {content: listContent[0],
                    contentVI: contentVI[0],
                    contentEN: contentEN[0]});

            });
        });
    });


});
router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/sua', function (req, res, next) {
    if(!req.session.username){
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết phiên làm việc"));
        return;
    }
    var obj = req.body;
    var order = obj.order;
    var des_vi = "";
    var title_vi = "";
    var html_vi = "";
    var des_en = "";
    var title_en = "";
    var html_en = "";
    var image = obj.image;
    var id = obj.id;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;
    var entity_code = api.ENUM_ENTITY_CODE.HOM;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    var rating = 0;
    db.updateSlider(image,title_vi,html_vi,des_vi,updated_by,updated_date,"",entity_code,order,status,id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }

        db.updateMucDuLieu(id,"",title_vi,des_vi,html_vi,updated_by,updated_date,meta_keyword_vi,meta_description_vi,meta_title_vi,entity_code,"vi", function (e1, ressult) {
            if (e1) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật vào mục dữ liệu tiếng việt"));
                return;
            }
            db.updateMucDuLieu(id,"",title_en,des_en,html_en,updated_by,updated_date,meta_keyword_en,meta_description_en,meta_title_en,entity_code,"en", function (e3, ressult) {
                if (e3) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật vào mục dữ liệu tiếng anh"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            });
        });
    })


});


module.exports = router;
