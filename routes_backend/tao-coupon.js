var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    db.getUnselectedProductPromotionCouponWithOutPromotionId( function (e2, unSelectedPromotion) {
        if (e2) {
            return;
        }
        res.render('admin/tao-coupon', {
            new_coupon: api.randomString(10),
            unSelectedPromotion: unSelectedPromotion
        });
    });
});


router.post('/getUnselectedProductPromotionCoupon', function (req, res, next) {

    db.getUnselectedProductPromotionCoupon(null, function (e2, unSelectedPromotion) {
        if (e2) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, {
            unSelectedPromotion: unSelectedPromotion
        }, ""));
    });

});

router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/them-san-pham-ap-dung', function (req, res, next) {

    var obj = req.body;
    var product_id = obj.product_id;
    var created_at = api.getNow();
    var created_by = req.session.username;
    var promotion_id = obj.promotion_id;
    var id = uuidv1();
    var status = 1;
    var entity_code = api.ENUM_ENTITY_CODE.PCP;

    db.getSanPhamLienKet(product_id,function (error1, rs) {
        if (error1) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        if(rs.length > 0){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Sản phẩm này đã có khuyến mãi rồi"));
            return;
        }

        db.insertPromotionDetail(id, promotion_id, product_id, created_at, created_by, status, entity_code, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, id, ""));

        })
    })

});

router.post('/get_new_coupon', function (req, res, next) {
    var obj = req.body;
    var number = obj.number;
    res.json(api.getResponse(api.SUCC_EXEC, api.randomString(number), ""));

});

router.post('/get_new_multi_coupon', function (req, res, next) {
    var obj = req.body;
    var array = [];
    var number = obj.number;
    var number_text = obj.number_text;
    var pre = obj.pre;
    var last = obj.last;
    for(var index=0;index<number;index++){
        var ramdom = api.randomString(number_text);
        var text = pre+ramdom+last;
        array.push(text);

    }
    res.json(api.getResponse(api.SUCC_EXEC, array, ""));

});




router.post('/add-coupon', function (req, res, next) {
    var obj = req.body;
    var coupon = obj.coupon;
    var time_use = obj.time_use;
    var id = uuidv1();


    var created_date = api.getNow();
    var coupon_code = obj.coupon;
    var type = 1;
    var entity_code = "";
    var gift = "";
    if (obj.optionsRadios1 == true) { // Fixed
        type = 1;
    } else if (obj.optionsRadios2 == true) {
        type = 2;
    }


    entity_code = api.ENUM_ENTITY_CODE.COU;
    // obj.beginDate = api.convertDate(obj.beginDate);
    // obj.endDate = api.convertDate(obj.endDate, true);

    obj.giaThap = obj.giaThap == "" ? null : obj.giaThap;
    obj.giaCao = obj.giaCao == "" ? null : obj.giaCao;
    var time_use_coupon = obj.time_use;
    if(!time_use_coupon) time_use_coupon = 0;
    db.insertPromotion(id, obj.code, obj.name, obj.beginDate, obj.endDate, type,
        obj.image, obj.ipGiamGiaCoDinh, obj.listenSlider, obj.giaThap, obj.giaCao, created_date,
        req.session.username, 1, obj.order, entity_code, coupon_code,time_use_coupon, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }

            var couponType = null;
            if (obj.optionsRadiosApDung1 == true) { // Fixed
                couponType = 1;
            } else if (obj.optionsRadiosApDung2 == true) {
                couponType = 2;
            }
            db.updateCouponStatus(couponType,id, function (error, result) {
                if (error) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
                    return;
                }
                // nothing
            });

            res.json(api.getResponse(api.SUCC_EXEC, {
                promotion_id: id,
                entity_code: entity_code
            }, ""));

        })
});

router.post('/add-multi-coupon', function (req, res, next) {
    var obj = req.body;
    var array = obj.array;
    var promotion_id = obj.promotion_id;


function add_coupon(array,index,promotion_id) {
    var id = uuidv1();
    var coupon = array[index];
    db.insertCoupon(id,promotion_id,api.getNow(),req.session.username,1,api.ENUM_ENTITY_CODE.COU,coupon,function (error, rs) {
        if (index==(array.length-1)) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }else {
            index++;
            add_coupon(array,index,promotion_id);
        }
    })
}
add_coupon(array,0,promotion_id);

});




module.exports = router;
