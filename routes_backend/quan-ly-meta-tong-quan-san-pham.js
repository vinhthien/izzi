var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    var require_id = "4cc75d0a-4e83-11e8-9c2d-fa7ae01bbebc";
    db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,"vi",require_id,function (error,metaVI) {
        if(error) return;

        db.getMucDuLieu(api.ENUM_ENTITY_CODE.MTA,"en",require_id,function (error1, metaEN) {
            if(error1) return;

            res.render('admin/quan-ly-meta-tong-quan-san-pham', {
                metaVI: metaVI[0],
                metaEN: metaEN[0],
                require_id: require_id
            });


        })
    })
});





router.post('/capnhat', function (req, res, next) {

    var obj = req.body;
    var require_id = obj.require_id;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;

    var created_date = api.getNow();
    var created_by = req.session.username;
    db.updateMucDuLieu(require_id, "", "", "", "", created_by, created_date, meta_keyword_vi, meta_description_vi, meta_title_vi, api.ENUM_ENTITY_CODE.MTA, "vi", function (error, rs) {
        if (error) return;

        db.updateMucDuLieu(require_id, "", "", "", "", created_by, created_date, meta_keyword_en, meta_description_en, meta_title_en, api.ENUM_ENTITY_CODE.MTA, "en", function (error1, rs1) {
            if (error1) return;

            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        })
    })



    


});

module.exports = router;
