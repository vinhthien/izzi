var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

router.get('/', function (req, res, next) {
    var id = req.query.id;
    res.render('admin/sua-mon-an-ngon', {
        content_id: id
    });


});

router.get('/language', function (req, res, next) {
    var content_id = req.query.content_id;
    var language_code = req.query.language_code;
    db.getContentByLanguageCode(content_id, language_code, function (error, listContent) {
        if (error) {
            return;
        }

        if (listContent != null && listContent.length > 0) {
            res.json(api.getResponse(api.SUCC_EXEC, {
                content: listContent[0]
            }, ""));
        } else {
            res.json();
        }
    });

});
router.post('/uploader', multipartMiddleware, function(req, res) {

    fs.readFile(req.files.upload.path, function (err, data) {
        var form = new formidable.IncomingForm(),
            files = [],
            fields = [];

        form.uploadDir = "upload";
        var newPath = form.uploadDir + "/" + req.files.upload.name;
        fs.writeFile(newPath, data, function (err) {
            if (err) console.log({err: err});
            else {
                html = "";
                html += "<script type='text/javascript'>";
                html += "    var funcNum = " + req.query.CKEditorFuncNum + ";";
                html += "    var url     = \"/upload/" + req.files.upload.name + "\";";
                html += "    var message = \"Uploaded file successfully\";";
                html += "";
                html += "    window.parent.CKEDITOR.tools.callFunction(funcNum, url, message);";
                html += "</script>";
                res.send(html);
            }
        });
    });
});
router.post('/upload', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/sua', function (req, res, next) {
    if(!req.session.username){
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết phiên làm việc"));
        return;
    }
    var obj = req.body;
    var tag = obj.tag;
    var order = obj.order;
    var image = obj.image;
    var imageLarge = obj.image_large;
    var title_vi = obj.title_vi;
    var des_vi = obj.des_vi;
    var html_vi = obj.html_vi;
    var title_en = obj.title_en;
    var des_en = obj.des_en;
    var html_en = obj.html_en;
    var content_category_id = "9C50E7B6-A76B-4115-9EDA-F9E3604CB304";
    var id = obj.id;
    var entity_code = api.ENUM_ENTITY_CODE.MON;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;

    db.updateLargeImage(imageLarge, id);

    db.updateContent(entity_code,updated_by,updated_date,title_vi,tag,html_vi,status,order,image,id,function (error, result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }
        db.updateMucDuLieu(id,"",title_vi,des_vi,html_vi,updated_by,updated_date,meta_keyword_vi,meta_description_vi,meta_title_vi,entity_code,"vi",function (error1, a) {
            if(error1){
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu tiếng việt"));
                return;
            }
            db.updateMucDuLieu(id,"",title_en,des_en,html_en,updated_by,updated_date,meta_keyword_en,meta_description_en,meta_title_en,entity_code,"en",function (error1, a) {
                if(error1){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu tiếng anh"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));

            })
        })
    });
});


module.exports = router;
