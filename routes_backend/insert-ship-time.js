var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

router.get('/', function (req, res, next) {
    var name = "ship-time-text";
    db.getConfig(name,function (error, config) {
        if (error) {
            return;
        }
        res.render('admin/insert-ship-time',
            {config: config[0]});
    });
});


router.post('/sua', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Hết phiên làm việc."));
        return;
    }
    var name = "ship-time-text";

    var obj = req.body;
    var value = obj.value;
    db.updateConfig(value,name, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));
    });
});

module.exports = router;
