var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {

    var id = req.query.id;


    db.getUnselectedProductPromotionCoupon(id, function (e2, unSelectedPromotion) {
        if (e2) {
            return;
        }
        db.getSelectedProductionPromotionCoupon(id, function (e1, selectedPromotion) {
            if (e1) {
                return;
            }
            db.getPromotionById(id, function (e, data) {
                if (e) {
                    return;
                }
                data[0].start_date = api.revertDatePromotionFrontEnd(data[0].start_date);
                data[0].end_date = api.revertDatePromotionFrontEnd(data[0].end_date);

                if(data[0].coupon != null) {
                    res.render('admin/sua-chuong-trinh-coupon', {
                        is_list_coupon: false,
                        promotion: data[0],
                        unSelectedPromotion: unSelectedPromotion,
                        selectedPromotion: selectedPromotion
                    });
                }else {
                    db.getCouponList(data[0].id,function (error, couponList) {
                        if(error){
                            return;
                        }
                        res.render('admin/sua-chuong-trinh-coupon', {
                            promotion: data[0],
                            is_list_coupon: true,
                            couponList: couponList,
                            unSelectedPromotion: unSelectedPromotion,
                            selectedPromotion: selectedPromotion
                        });
                    })
                }
            })
        })
    })
});


router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/them-lien-ket', function (req, res, next) {

    var obj = req.body;
    var product_id = obj.product_id;
    var created_at = api.getNow();
    var created_by = req.session.username;
    var promotion_id = obj.promotion_id;
    var id = uuidv1();
    var status = 1;
    var entity_code = api.ENUM_ENTITY_CODE.PCP;
    db.insertPromotionDetail(id, promotion_id, product_id, created_at, created_by, status, entity_code, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, id, ""));

    })

});

router.post('/change-status-single-coupon', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;
    var status = obj.status;
    db.updatePromotionStatus(status, promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));

    })

});


router.post('/change-status-multi-coupon', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;
    var status = obj.status;
    db.updatePromotionDetailStatus(status, promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, "", ""));

    })

});


router.post('/update-option', function (req, res, next) {

    var obj = req.body;
    var option = obj.option;
    var promotion_id = obj.promotion_id;
    db.updateCouponStatus(option,promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, promotion_id, ""));

    })

});

router.post('/them-product-gift', function (req, res, next) {
    var obj = req.body;
    var product_id = obj.product_id;
    var created_at = api.getNow();
    var created_by = req.session.username;
    var promotion_id = obj.promotion_id;
    var id = uuidv1();
    var status = 1;
    var entity_code = api.ENUM_ENTITY_CODE.PKL;
    db.insertPromotionDetail(id, promotion_id, product_id, created_at, created_by, status, entity_code, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, id, ""));
    })
});


router.post('/xoa-lien-ket', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;

    db.xoaPromotionDetail(id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/xoa-ct', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;

    db.updatePromotionStatus(2,promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});


router.post('/deactive', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;

    db.updateCouponStatus(id,2, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/deactive-one', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;

    db.updatePromotionStatus(2,promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/active-one', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;

    db.updatePromotionStatus(1,promotion_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/active', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;

    db.updateCouponStatus(id,1, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/xoa-product-gift', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;
    var product_id = obj.product_id;

    db.xoaGiftProduct(promotion_id,product_id, function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));

    })

});

router.post('/sua', function (req, res, next) {
    var obj = req.body;
    var type = obj.type;
    var code = obj.code;
    var name = obj.name;
    var beginDate = obj.beginDate;
    var endDate = obj.endDate;
    var giaThap = obj.giaThap;
    var giaCao = obj.giaCao;
    var order = obj.order;
    var entity_code = obj.entity_code;
    var promotion_id = obj.promotion_id;
    var image = obj.image;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    var status = 1;
    //beginDate = api.convertDate(beginDate);
    //endDate = api.convertDate(endDate, true);

    giaThap = giaThap == "" ? null : giaThap;
    giaCao = giaCao == "" ? null : giaCao;

    db.updatePromotionCoupon(code, name, giaThap, giaCao, image, updated_by, updated_date, order, promotion_id, function (e, rs) {
        if (e) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }

        if (type == 3) {
            var gift = obj.gift;
            db.updateMucDuLieu(promotion_id, null, gift, null, null, updated_by, updated_date, entity_code, "vi", function (e1, rs1) {
                if (e1) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu tiếng việt"));
                    return;
                }
                db.updateMucDuLieu(promotion_id, null, gift, null, null, updated_by, updated_date, entity_code, "en", function (e2, rs2) {
                    if (e2) {
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật mục dữ liệu anh"));
                        return;
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));

                })
            })
        } else if (type == 4) {
            var coupon = obj.coupon;
            db.updatePromotionCoupon(code, name, giaThap, giaCao, image, updated_by, updated_date, order, promotion_id, function (e4, rs4) {
                if (e4) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            });

        } else if (type == 1 || type == 2) {

            res.json(api.getResponse(api.SUCC_EXEC, null, ""));

        }


    })





});


module.exports = router;
