var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');

// GET home page
router.get('/', function (req, res, next) {
    var id = "f0bde88c-444e-11e8-842f-0ed5f89f718b";
    db.getContentByID(id, function (error, content) {
        if (error) {
            return;
        }
        db.getMucDuLieu(content[0].entity_code, "vi", id, function (error3, contentVI) {
            if (error3) {
                return;
            }
            db.getMucDuLieu(content[0].entity_code, "en", id, function (error4, contentEN) {
                if (error4) {
                    return;
                }
                var data = {
                    content: content[0],
                    contentVI: contentVI[0],
                    contentEN: contentEN[0]
                };

                res.render('admin/quan-ly',
                    data);

            });
        });
    })

});

router.post('/sua', function (req, res, next) {

    var obj = req.body;
    var html_vi = obj.html_vi;
    var html_en = obj.html_en;
    var id = "f0bde88c-444e-11e8-842f-0ed5f89f718b";
    var entity_code = api.ENUM_ENTITY_CODE.AAA;
    var updated_date = api.getNow();
    var updated_by = req.session.username;

    db.updateQuanLy(updated_by, updated_date, html_vi, id, function (error, result) {
        if (error) {
            return;
        }

        db.updateMucDuLieuGioiThieu(id, "", "", html_vi, updated_by, updated_date, entity_code, "vi", function (e1, ressult) {
            if (e1) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhậ vào mục dữ liệu tiếng việt"));
                return;
            }
            db.updateMucDuLieuGioiThieu(id, "", "", html_en, updated_by, updated_date, entity_code, "en", function (e3, ressult) {
                if (e3) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật vào mục dữ liệu tiếng anh"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, null, ""));
            });
        });


    })


});


module.exports = router;
