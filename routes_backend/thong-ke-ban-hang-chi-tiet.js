var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');


function get_data_detail(start_date, end_date, callback) {
    db.getDataStatisticalDetail(start_date,end_date,function (error, data) {
        if (error) {
            return;
        }

        callback(data);

    })
}



// GET home page
router.get('/', function (req, res, next) {
    var obj = req.body;
    var previous_date = api.getPreviousWeekDayFromNow();
    var now = api.getNow();

    db.getDataStatisticalDetail(now,previous_date,function (error, data) {

        // for(var index=0;index<data.length;index++) {
        //     var obj = data[index];
        //     console.log(239489545);
        //     console.log(data[index]);
        //     data[index].first_store = parseInt(data[index].first_store / data[index].combo_quantity);
        //     data[index].end_store = parseInt(data[index].end_store / data[index].combo_quantity);
        //     var present_store = parseInt(data[index].store / data[index].combo_quantity);
        //
        //     data[index].end_store = present_store - data[index].end_store;
        //     data[index].first_store = data[index].end_store - data[index].first_store;
        //
        //     data[index].total_product_quantity = data[index].first_store - data[index].end_store;
        //     data[index].total = data[index].total_product_quantity * data[index].product_price;
        //     console.log(34554656456);
        //     console.log(data[index].first_store);
        //
        // }

        res.render('admin/thong-ke-ban-hang-chi-tiet', {
            data:data
        });
    });


});

router.post('/get-data-detail', function (req, res, next) {
    var obj = req.body;
    var option_sort = obj.option_sort;
    var start_date = obj.start_date;
    var end_date = obj.end_date;

    console.log(6677);
    console.log("start_date "+ start_date);
    console.log("end_date "+ end_date);
    console.log("option_sort "+ option_sort);
    get_data_detail(start_date,end_date, function (data) {
        console.log("data "+ data.length);
        console.log(9898989);
        res.json(api.getResponse(api.SUCC_EXEC, data, ""));
    })

});


module.exports = router;
