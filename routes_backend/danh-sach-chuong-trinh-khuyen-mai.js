var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

// GET home page
router.get('/', function (req, res, next) {
    // if(req.session.token && req.session.email) {
    //     var email = req.session.email;
    //     var token = req.session.token;

        // res.render('chinh-sach-ban-hang', {
        //     email: email,
        //     token: token
        // });


    db.getAllPromotionWithoutCoupon(function (error, data) {
        if(error){
            return;
        }

        res.render('admin/danh-sach-chuong-trinh-khuyen-mai', {data:data});
    })


    // }else {
    //     utility.renderLogin(res);
    // }
});

router.post('/xoa', function (req, res, next) {
    // if(req.session.token && req.session.email) {
    //     var email = req.session.email;
    //     var token = req.session.token;

    // res.render('chinh-sach-ban-hang', {
    //     email: email,
    //     token: token
    // });

    var obj = req.body;
    var id = obj.id;
    db.removePromotion(id,function (error,result) {
       if(error){
           res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
           return;
       }
       if(result){
           res.json(api.getResponse(api.SUCC_EXEC, null, ""));
       }

    });

    // }else {
    //     utility.renderLogin(res);
    // }
});

router.post('/update_order', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    var order = obj.order;

    db.updateOrderPromotion(order,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_status', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    var status = obj.status;
    db.updatePromotionStatus(status,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

module.exports = router;
