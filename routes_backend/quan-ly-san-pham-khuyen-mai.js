var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    db.getSanPhamKhuyenMai(function (error, sanpham) {
        if (error) return;
        res.render('admin/quan-ly-san-pham-khuyen-mai', {
            sanpham: sanpham
        });

    })

});





module.exports = router;
