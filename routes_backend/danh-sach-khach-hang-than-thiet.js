var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var nodemailer = require('nodemailer');
const uuidv1 = require('uuid/v1');
var pug = require('pug');

var customer_process = require('../businesses/customer_process');


// GET home page
router.get('/', function (req, res, next) {
    customer_process.getAllCustomers(function (customers) {
        res.render('admin/danh-sach-khach-hang-than-thiet', {
            customers: customers
        });
    });
});

router.post('/getPurchaseDetails', function (req, res, next) {

    var obj = req.body;
    var order_id = obj.order_id;

    order_process.getOrderInformation(order_id, req.session.locale, function (total_amount_after_product_promotion, ship_fee,
                                                                              couponApplied, coupon_discount, orderDetails, orderInfo) {
        if (!orderDetails) {
            orderDetails = [];
        }

        res.json(api.getResponse(api.SUCC_EXEC, {
            products: orderDetails,
            orderInfo: orderInfo,
            total_amount_after_product_promotion: total_amount_after_product_promotion,
            phi_van_chuyen: ship_fee,
            couponApplied: couponApplied,
            coupon_discount: coupon_discount,
            final_total_amount: total_amount_after_product_promotion + ship_fee - coupon_discount
        }, ""));

    });

});

module.exports = router;
