var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

// GET home page
router.get('/', function (req, res, next) {

    db.getDanhMucSanPham_New_Edit(function (error, data) {
        if(error){
            return;
        }
        res.render('admin/danh-sach-danh-muc-san-pham', {data:data});
    })

});
router.post('/changeProductStatus', function (req, res, next) {
    var obj = req.body;
    var id = obj.id;
    var status = obj.new_status;

    db.updateProductCategoryStatus(status,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});
router.post('/xoa', function (req, res, next) {
    // if(req.session.token && req.session.email) {
    //     var email = req.session.email;
    //     var token = req.session.token;

    // res.render('chinh-sach-ban-hang', {
    //     email: email,
    //     token: token
    // });
    if(!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết phiên làm việc"));
        return;
    }
    var obj = req.body;
    var id = obj.id;
    db.getProductCategoryById(id,function (e1, categoty_obj) {
        if(e1){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        console.log("categoty_obj "+ JSON.stringify(categoty_obj));
        if(categoty_obj[0].category_parent_id != null){
            console.log("IN 1");
            db.countExistProductUseCategotyId(id,function (er, number) {
                if(er){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                    return;
                }
                if(number[0].total > 0){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Còn "+number[0].total+" sản phẩm đang sử dụng danh mục này"));
                    return;
                }else {
                    db.removeDanhMucSanPham(id, function (error, result) {
                        if (error) {
                            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                            return;
                        }
                        if (result) {
                            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
                        }
                    });
                }


            })
        }else {
            console.log("IN 2");
            db.countChildProductCategory(id,function (e3, count) {
                if(e3){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                    return;
                }
                console.log("count "+ JSON.stringify(count));
                if(count[0].total > 0){
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Còn "+count[0].total+" danh mục con đang sử dụng danh mục này"));
                    return;
                }else {
                    db.removeDanhMucSanPham(id, function (error, result) {
                        if (error) {
                            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                            return;
                        }
                        if (result) {
                            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
                        }
                    });
                }
            })
        }
    })

});

router.post('/update_order', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    var order = obj.order;

    db.updateOrderProductCategory(order,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

module.exports = router;
