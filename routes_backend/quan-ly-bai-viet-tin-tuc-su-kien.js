var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {

    var id = req.query.id;
    db.getTinTucByType(null,function (error, normalMonNgon) {
        if (error) return;
        db.getTinTucByType(2,function (error1, topMonNgon) {
            if(error1) return;
            res.render('admin/quan-ly-bai-viet-tin-tuc-su-kien', {
                normalMonNgon: normalMonNgon,
                topMonNgon: topMonNgon
            });
        })
    })



});




router.post('/them', function (req, res, next) {

    var obj = req.body;
    var content_id = obj.content_id;

    db.updateMonNgonType(2,content_id,function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    })

});


router.post('/xoa', function (req, res, next) {

    var obj = req.body;
    var content_id = obj.content_id;

    db.updateMonNgonType(null,content_id,function (error, result) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    })

});


module.exports = router;
