var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {

    db.getDanhMucRootOnly("", function (error, danhmuc) {
        if (error) {
            return;
        }
        res.render('admin/them-danh-muc-san-pham', {danhmuc: danhmuc});
    });


});


router.post('/upload-hinh', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/them', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Hết phiên làm việc."));
        return;
    }
    var obj = req.body;
    var order = obj.order;
    var des_vi = obj.name_vi;
    var name_vi = obj.name_vi;
    var title_vi = obj.name_vi;
    var html_vi = obj.name_vi;
    var des_en = obj.name_en;
    var name_en = obj.name_en;
    var title_en = obj.name_en;
    var html_en = obj.name_en;
    var parent_id = obj.parent_id == "" ? null : obj.parent_id;
    var image = obj.image;
    var id = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var entity_code = api.ENUM_ENTITY_CODE.CAT;
    var created_date = api.getNow();
    var created_by = req.session.username;
    var status = 1;
    var rating = 0;
    var banner = obj.banner;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;
    var baner_link =obj.baner_link;
    db.validateProductCategoryExist(title_vi,function (e, rs) {
        if (e) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }

        if(rs[0].number_category_duplicate > 0){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Tên danh mục này đã được dùng rồi hãy chọn tên khác"));
            return;
        }

        db.insertDanhMucSanPham(id, name_vi, title_vi, des_vi, html_vi, created_date, created_by, entity_code, status, order, parent_id, image, banner, baner_link, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }

            var idVi = uuidv1();
            db.insertMucDuLieu(id, name_vi, title_vi, des_vi, html_vi, created_by, created_date, entity_code, status, "vi", meta_keyword_vi, meta_description_vi, meta_title_vi, idVi, function (e1, ressult) {
                if (e1) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể thêm vào mục dữ liệu tiếng việt"));
                    return;
                }
                var idEn = uuidv1();
                db.insertMucDuLieu(id, name_en, title_en, des_en, html_en, created_by, created_date, entity_code, status, "en", meta_keyword_en, meta_description_en, meta_title_en, idEn, function (e3, ressult) {
                    if (e3) {
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể thêm vào mục dữ liệu tiếng anh"));
                        return;
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        id: id
                    }, ""));

                });

            });

        })
    })

});


module.exports = router;
