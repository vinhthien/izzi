var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {
    if (req.session.username) {
        var id = req.query.id;

        db.getDanhMucSanPhamById(id, function (error, danhmuc) {
            if (error) {
                return;
            }
            db.getDanhMucRootOnly(id, function (error1, listDanhmuc) {
                if(error1){
                    return;
                }

                db.getMucDuLieu(danhmuc[0].entity_code, "vi", id, function (error3, danhmucVI) {
                    if (error3) {
                        return;
                    }
                    db.getMucDuLieu(danhmuc[0].entity_code, "en", id, function (error4, danhmucEN) {
                        if (error4) {
                            return;
                        }
                        res.render('admin/sua-danh-muc-san-pham', {
                            listDanhmuc: listDanhmuc,
                            danhmuc: danhmuc[0],
                            danhmucVI: danhmucVI[0],
                            danhmucEN: danhmucEN[0]
                        });
                    });
                });
            })

        });


    } else {
        res.render('admin/loginPage');
    }

});


router.post('/upload', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});

router.post('/sua', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết session làm việc"));
        return;
    }
    var obj = req.body;
    var order = obj.order;
    var des_vi = obj.des_vi;
    var name_vi = obj.name_vi;
    var title_vi = obj.name_vi;
    var html_vi = obj.html_vi;
    var des_en = obj.des_en;
    var name_en = obj.name_en;
    var title_en = obj.name_en;
    var html_en = obj.html_en;
    var parent_id = obj.parent_id == "" ? null : obj.parent_id;
    var image = obj.image;
    var category_id = obj.category_id;
    var entity_code = api.ENUM_ENTITY_CODE.CAT;
    var updated_date = new Date().toISOString().slice(0, 19).replace('T', ' ');
    var updated_by = req.session.username;
    var status = 1;
    var rating = 0;
    var banner = obj.banner;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;
    var link=obj.link;

    db.updateDanhMucSanPham(name_vi,title_vi,des_vi,html_vi,updated_by,updated_date,entity_code,status,order,parent_id,image,banner,category_id,link, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;

            }
            console.log(456456);

            db.updateMucDuLieu(category_id, name_vi, title_vi, des_vi, html_vi, updated_by, updated_date,meta_keyword_vi,meta_description_vi,meta_title_vi, entity_code, "vi", function (e1, ressult) {
                if (e1) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhậ vào mục dữ liệu tiếng việt"));
                    return;
                }
                db.updateMucDuLieu(category_id, name_en, title_en, des_en, html_en, updated_by, updated_date,meta_keyword_en,meta_description_en,meta_title_en, entity_code, "en", function (e3, ressult) {
                    if (e3) {
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể cập nhật vào mục dữ liệu tiếng anh"));
                        return;
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));

                });

            });

        })


});


module.exports = router;
