var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    var new_coupon = uuidv1();
    db.getAllGiftAndProduct(function (e9, un_gift_product) {
        if (e9) return;
        db.getUnselectedPromotion(null, function (e2, unSelectedPromotion) {
            if (e2) {
                return;
            }
            res.render('admin/them-chuong-trinh-khuyen-mai', {new_coupon: api.randomString(10),
                un_gift_product: un_gift_product,unSelectedPromotion: unSelectedPromotion});
        })

    })

});




router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


router.post('/get_new_coupon', function (req, res, next) {
    res.json(api.getResponse(api.SUCC_EXEC, api.randomString(10), "lỗi mạng"));

});

router.post('/get-unselected-promotion', function (req, res, next) {
    db.getUnselectedPromotion(null, function (e2, unSelectedPromotion) {
        if (e2) {
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, unSelectedPromotion, "lỗi mạng"));
    })
});

router.post('/them-lien-ket', function (req, res, next) {

    var obj = req.body;
    var promotion_id = obj.promotion_id;
    var entity_code = obj.entity_code;
    var link_product_array = obj.link_product_array;
    function add(index,link_product_array,entity_code,promotion_id,req) {
        if(index == link_product_array.length) {
            res.json(api.getResponse(api.SUCC_EXEC, id, ""));
            return;
        }
        var object = link_product_array[index];


        var status = 1;

        var created_at = api.getNow();
        var created_by = req.session.username;
        var id = uuidv1();
        var product_id = object.product_id;
        var product_code = object.product_code;
        var product_name = object.product_name;
        db.getSanPhamLienKet(product_id,function (error1, rs) {
            if (error1) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
                return;
            }
            if(rs.length > 0){
                add(index+1,link_product_array,entity_code,promotion_id,req);
                return;
            }
            db.insertPromotionDetail(id, promotion_id, product_id, created_at, created_by, status, entity_code, function (error, result) {
                if (error) {
                    return;
                }
                add(index+1,link_product_array,entity_code,promotion_id,req);

            })
        })
    }
    add(0,link_product_array,entity_code,promotion_id,req);


});


router.post('/them-product-gift', function (req, res, next) {
    var obj = req.body;
    var promotion_id = obj.promotion_id;
    var gift_product_array = obj.gift_product_array;

    function add(index,gift_product_array,promotion_id,req) {
        if(index==gift_product_array.length){
            res.json(api.getResponse(api.SUCC_EXEC, id, ""));
            return;
        }
        var product_id = gift_product_array[index].product_id;
        var created_at = api.getNow();
        var created_by = req.session.username;
        var id = uuidv1();
        var status = 1;
        var entity_code = api.ENUM_ENTITY_CODE.PKL;
        db.insertPromotionDetail(id, promotion_id, product_id, created_at, created_by, status, entity_code, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi server"));
                return;
            }
            add(index+1,gift_product_array,promotion_id,req)
        })
    }
    add(0,gift_product_array,promotion_id,req);



});

router.post('/them', function (req, res, next) {
    var obj = req.body;
    var id = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var created_date = api.getNow();
    var coupon_code = "";
    var type = 1;
    var entity_code = "";
    var gift = "";
    if (obj.optionsRadios1 == true) { // Fixed
        type = 1;
        entity_code = api.ENUM_ENTITY_CODE.PSO;
    } else if (obj.optionsRadios2 == true) {
        type = 2;
        entity_code = api.ENUM_ENTITY_CODE.PSO;
    }else if (obj.optionsRadios3 == true) {
        type = 3;
        entity_code = api.ENUM_ENTITY_CODE.PGT;
        gift = obj.ipGift;
    }

    // obj.beginDate = api.convertDate(obj.beginDate);
    // obj.endDate = api.convertDate(obj.endDate, true);

    obj.giaThap = obj.giaThap == "" ? null : obj.giaThap;
    obj.giaCao = obj.giaCao == "" ? null : obj.giaCao;
    var time_use_coupon = 0;


    db.insertPromotion(id, obj.code, obj.name, obj.beginDate, obj.endDate, type,
        obj.image, obj.ipGiamGiaCoDinh, obj.listenSlider, obj.giaThap, obj.giaCao, created_date,
        req.session.username, 1, obj.order, entity_code, coupon_code,time_use_coupon, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }
            if(type == 3){
                var idVI = uuidv1();
                db.insertMucDuLieu(id,"",gift,"","",req.session.username,created_date,entity_code,1,"vi","","","",idVI,function (error2, a) {
                    if(error2){
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                        return;
                    }
                    var idEN = uuidv1();
                    db.insertMucDuLieu(id,"",gift,"","",req.session.username,created_date,entity_code,1,"en","","","",idEN,function (error3, b) {
                        if(error3){
                            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                            return;
                        }
                        res.json(api.getResponse(api.SUCC_EXEC, {id:id,entity_code:entity_code,type:type}, ""));

                    })
                })
            }else {
                res.json(api.getResponse(api.SUCC_EXEC, {id:id,entity_code:entity_code,type:type}, ""));

            }

        })


});


module.exports = router;
