var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

router.get('/', function (req, res, next) {

    db.getAllMonAnNgonSliders(function (error, contentList) {
        if(error){
            return;
        }
        res.render('admin/danh-sach-slider-mon-an-ngon', {data: contentList});
    });

});

router.post('/xoa', function (req, res, next) {
    console.log(1)

    var obj = req.body;
    var id = obj.id;
    console.log(2)

    db.removeSlider(id, function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

module.exports = router;
