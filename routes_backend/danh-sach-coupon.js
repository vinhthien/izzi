var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var moment = require('moment');

// GET home page
router.get('/', function (req, res, next) {

    db.getAllCouponUseOneTime(function (error, couponUseOneTime) {
        if(error){
            return;
        }
        db.getAllCouponUseMultiTime(function (error1, couponUseMultiTime) {
            if (error1) {
                return;
            }
            var checkCouponList = [];
            for(var index = 0;index<couponUseOneTime.length;index++){
                var obj = couponUseOneTime[index];
                var coupon = obj.coupon;
                var id = obj.id;
                var status = obj.status;
                var promotion_status = obj.promotion_status;
                var use_time = 0;
                var end_date = obj.end_date;
                var not_used = false;
                var suspended = false;
                var used = false;
                var out_date = false;
                var promotion_name = obj.promotion_name;
                var start_date = obj.start_date;
                var promotion_suspended = false;
                if(status == 1){
                    use_time = 1;
                    not_used = true;
                }else if(status == 2){
                    suspended = true;
                }else if(status == 3){
                    used = true;
                }
                if(promotion_status == 2){
                    promotion_suspended = true;
                }
                var date = moment(end_date);
                var now = moment();

                if (now > date) {
                    out_date = true;
                    suspended = false;
                    not_used = false;
                    used = false;
                }
                couponUseOneTime[index].promotion_suspended = promotion_suspended;
                checkCouponList.push({
                    promotion_detail_id: id,
                    coupon : coupon,
                    status: status,
                    use_time: use_time,
                    not_used: not_used,
                    suspended: suspended,
                    promotion_suspended: promotion_suspended,
                    used: used,
                    is_single: false,
                    out_date: out_date,
                    promotion_name: promotion_name,
                    start_date: api.revertDatePromotionFrontEnd(start_date),
                    end_date: api.revertDatePromotionFrontEnd(end_date)
                })
                couponUseOneTime[index].start_date = api.revertDatePromotionFrontEnd(start_date);
                couponUseOneTime[index].end_date = api.revertDatePromotionFrontEnd(end_date);
            }

            for(var index = 0;index<couponUseMultiTime.length;index++){
                var obj = couponUseMultiTime[index];
                var coupon = obj.coupon;
                var id = obj.id;
                var status = obj.status;
                var promotion_status = obj.status;
                var promotion_suspended = false;
                var use_time = obj.number_of_uses;
                var end_date = obj.end_date;
                var promotion_name = obj.promotion_name;
                var start_date = obj.start_date;

                var not_used = false;
                var suspended = false;
                var used = false;
                var out_date = false;

                if(status == 1){
                    not_used = true;
                }else if(status == 2){
                    suspended = true;
                }else if(status == 3 ){
                    used = true;
                }
                if(use_time == 0){
                    not_used = false;
                    suspended = false;
                    used = true;
                }
                if(promotion_status == 2){
                    promotion_suspended = true;
                }

                var date = moment(end_date);
                var now = moment();

                if (now > date) {
                    out_date = true;
                    not_used = false;
                    suspended = false;
                    used = false;
                }
                couponUseMultiTime[index].promotion_suspended = promotion_suspended;
                checkCouponList.push({
                    promotion_id: id,
                    coupon : coupon,
                    status: status,
                    use_time: use_time,
                    not_used: not_used,
                    suspended: suspended,
                    promotion_suspended : promotion_suspended,
                    used: used,
                    promotion_name: promotion_name,
                    start_date: api.revertDatePromotionFrontEnd(start_date),
                    end_date: api.revertDatePromotionFrontEnd(end_date),
                    is_single: true,
                    out_date: out_date
                })
                couponUseMultiTime[index].start_date = api.revertDatePromotionFrontEnd(start_date);
                couponUseMultiTime[index].end_date = api.revertDatePromotionFrontEnd(end_date);
            }
            console.log("checkCouponList "+ JSON.stringify(checkCouponList));
            res.render('admin/danh-sach-coupon', {
                couponUseOneTime:couponUseOneTime,
                couponUseMultiTime:couponUseMultiTime,
                checkCouponList:checkCouponList
            });
        })
    })


    // }else {
    //     utility.renderLogin(res);
    // }
});


router.post('/update_order', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    var order = obj.order;

    db.updateOrderPromotion(order,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_status_multi', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    var status = obj.status;
    db.updatePromotionStatus(status,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_promotion_number', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    var number = obj.number;
    console.log("id "+ id);
    console.log("number "+ number);
    db.updatePromotionNumber(number,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_promotion_detail_number', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    var number = obj.number;
    db.updatePromotionDetailStatus(number,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_status_one', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    var status = obj.status;
    console.log("id "+ id);
    console.log("status "+ status);
    db.updatePromotionDetailStatus(status,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});


router.post('/update_coupon', function (req, res, next) {

    var obj = req.body;
    var array = obj.array;
    var status = obj.status;
    function has(object, key) {
        return object ? hasOwnProperty.call(object, key) : false;
    }
    function update(index, array, status) {
        if(index == array.length){
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));
                    return;
        }
        var obj = array[index];
        var promotion_id = obj.promotion_id;
        var promotion_detail_id = obj.promotion_detail_id;

        if(promotion_id && promotion_id.length > 0){
            db.updatePromotionStatus(status,promotion_id,function (error, rs) {
                if(error) return;
                update(index+1,array,status);
            })
        }else {
            db.updatePromotionDetailStatus(status,promotion_detail_id,function (error, rs) {
                if(error) return;
                update(index+1,array,status);
            })
        }
    }
    update(0,array,status);
});

module.exports = router;
