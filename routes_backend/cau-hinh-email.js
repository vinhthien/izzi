var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var nodemailer = require('nodemailer');

// GET home page
router.get('/', function (req, res, next) {
    var subject_name = "email_subject_editor";
    var content_name = "content_subject_editor";
    var email_name = "email_for_host";
    var password_name = "password_for_host";
    var mail_host_url = "mail_host_url";
    var promotion_for_user_signup_first_time = "promotion_for_user_signup_first_time";
    var mail_host_port = "mail_host_port";


    db.getConfig(email_name, function (error2, emailObj) {
        if (error2) return;
        db.getConfig(password_name, function (error3, passwordObj) {
            if (error3) return;
            db.getConfig(mail_host_url, function (error4, hostObj) {
                if (error4) return;
                db.getConfig(mail_host_port, function (error4, portObj) {
                    if (error4) return;
                    res.render('admin/cau-hinh-email',
                        {
                            email: emailObj[0].value,
                            password: passwordObj[0].value,
                            host: hostObj[0].value,
                            port: portObj[0].value
                        });
                })
            })
        })
    })


});

router.post('/sua', function (req, res, next) {

    var obj = req.body;
    var email = obj.email;
    var password = obj.password;
    var host = obj.host;
    var port = obj.port;
    var email_name = "email_for_host";
    var password_name = "password_for_host";
    var mail_host_url = "mail_host_url";
    var mail_host_port = "mail_host_port";

    db.updateConfig(email,email_name,function (error2, rs) {
        if (error2){ return;}
        db.updateConfig(password, password_name, function (error3, rs1) {
            if (error3) {return;}
            db.updateConfig(host, mail_host_url, function (error4, rs1) {
                if (error4) {return;}
                db.updateConfig(port, mail_host_port, function (error5, rs1) {
                    if (error5) {return;}
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));
                })
            })
        })
    })



});

router.post('/upload', function (req, res, next) {
    console.log("IN");
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


module.exports = router;
