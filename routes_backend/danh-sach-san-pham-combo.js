var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var nodemailer = require('nodemailer');

// GET home page
router.get('/', function (req, res, next) {


    db.getSanPhamCombo(function (error, data) {
        if(error){
            return;
        }
        res.render('admin/danh-sach-san-pham-combo', {data:data});
    })


});

router.post('/xoa', function (req, res, next) {

    // res.render('admin/email-comfirm-order', {
    // }, function (err, html) {
    //     api.sendMailCustom(nodemailer,"siamopizza.com","25",false,"chatserver@hablax.com","hablaxChat!","dnduc456@gmail.com","Test new email",html);
    // });
    // return;

    var obj = req.body;
    var id = obj.id;
    console.log(id);
    db.removeSanPham(id,function (error,result) {
       if(error){
           res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
           return;
       }
       if(result){
           res.json(api.getResponse(api.SUCC_EXEC, null, ""));
       }

    });

    // }else {
    //     utility.renderLogin(res);
    // }
});

router.post('/update_order', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    var order = obj.order;

    db.updateOrderProduct(order,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/changeProductStatus', function (req, res, next) {
    var obj = req.body;
    var id = obj.id;
    var status = obj.new_status;

    db.updateProductStatus(status,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});
module.exports = router;
