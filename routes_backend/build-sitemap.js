var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
var nodemailer = require('nodemailer');

var  SitemapGenerator = require('sitemap-generator');

// create generator
var generator = SitemapGenerator('https://192.168.1.110:1234', {
    maxDepth: 0,
    filepath: './sitemap.xml',
    maxEntriesPerFile: 50000,
    stripQuerystring: true
});

router.get('/', function (req, res, next) {

});

// start the crawler
generator.start();