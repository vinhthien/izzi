var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
const uuidv1 = require('uuid/v1');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');



// GET home page
router.get('/', function (req, res, next) {
    var id = "8D8FC659-006D-4BDF-A93F-895EFE3DFFE6";
    db.getContentByID(id, function (error, content) {
        if (error) {
            return;
        }
        db.getMucDuLieu(content[0].entity_code, "vi", id, function (error3, contentVI) {
            if (error3) {
                return;
            }
            db.getMucDuLieu(content[0].entity_code, "en", id, function (error4, contentEN) {
                if (error4) {
                    return;
                }
                if(!contentVI[0].meta_keywords) contentVI[0].meta_keywords = "";
                if(!contentVI[0].meta_description) contentVI[0].meta_description = "";
                if(!contentVI[0].meta_title) contentVI[0].meta_title = "";
                if(!contentEN[0].meta_keywords) contentEN[0].meta_keywords = "";
                if(!contentEN[0].meta_description) contentEN[0].meta_keywords = "";
                if(!contentEN[0].meta_keywords) contentEN[0].meta_keywords = "";
                var data = {
                    content: content[0],
                    contentVI: contentVI[0],
                    contentEN: contentEN[0]
                };
                if (contentEN.length == 0 && contentVI.length == 0) {
                    data.is_new = true;
                } else {
                    data.is_new = false;
                }
                res.render('admin/cong-ty-co-phan-opera',
                    data);

            });
        });
    })

});

router.post('/sua', function (req, res, next) {
    if (!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết session làm việc"));
        return;
    }



});


router.post('/upload', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


module.exports = router;
