var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')

// GET home page
router.get('/', function (req, res, next) {
    var call_name = "front_end_call_number_footer";
    var sms_name = "front_end_sms_number_footer";
    var lat_name = "front_end_map_latitude_footer";
    var long_name = "front_end_map_longitude_footer";
    db.getConfig(call_name,function (error, call_number) {
        if(error) return;
        db.getConfig(sms_name,function (error, sms_number) {
            if (error) return;
            db.getConfig(lat_name, function (error, lat_number) {
                if (error) return;
                db.getConfig(long_name, function (error, long_number) {
                    if (error) return;
                    res.render('admin/quan-ly-lien-he-footer', {
                        call_number: call_number[0],
                        sms_number: sms_number[0],
                        lat_number: lat_number[0],
                        long_number: long_number[0]
                    });
                })
            })
        })

    })
});


router.post('/update', function (req, res, next) {

    var obj = req.body;
    var call_number = obj.call;
    var sms_number = obj.sms;
    var lat_number = obj.lat;
    var long_number = obj.long;
    var call_name = "front_end_call_number_footer";
    var sms_name = "front_end_sms_number_footer";
    var lat_name = "front_end_map_latitude_footer";
    var long_name = "front_end_map_longitude_footer";

    db.updateConfig(call_number,call_name,function (error2, rs2) {
        if(error2) return;
        db.updateConfig(sms_number,sms_name,function (error2, rs2) {
            if (error2) return;
            db.updateConfig(lat_number, lat_name, function (error2, rs2) {
                if (error2) return;
                db.updateConfig(long_number, long_name, function (error2, rs2) {
                    if (error2) return;
                    res.json(api.getResponse(api.SUCC_EXEC, null, ""));
                })
            })
        })

    })



});




module.exports = router;
