var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {

    db.getDanhMucNotInFooter(function (error, sanpham) {
        if(error) return;
        db.getDanhMucInFooter(function (error1, bestSell) {
            if(error1) return;
            res.render('admin/quan-ly-danh-muc-cap-2-footer', {
                sanpham: sanpham,
                bestSell: bestSell
            });

        })

    })

});


router.post('/them', function (req, res, next) {
    var obj = req.body;
    var category_id = obj.category_id;
    var order = obj.order;
    order = Number(order);
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    db.updateDanhMucToShowInFooter(category_id,updated_date, updated_by,function (error, countResult) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, "insert", ""));
    })
});

router.post('/xoa', function (req, res, next) {
    var obj = req.body;
    var category_id = obj.category_id;
    var order = obj.order;
    var updated_date = api.getNow();
    var updated_by = req.session.username;
    db.removeDanhMucFromFooter(category_id,updated_date, updated_by,function (error, countResult) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }

        res.json(api.getResponse(api.SUCC_EXEC, "delete", ""));
    })
});

router.post('/count', function (req, res, next) {
    var obj = req.body;
    var product_id = obj.product_id;
    db.countProductGiftExisting(product_id,function (error, countResult) {
        if (error) {
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Lỗi mạng"));
            return;
        }
        res.json(api.getResponse(api.SUCC_EXEC, countResult[0].count, "Lỗi mạng"));

    })
});


module.exports = router;
