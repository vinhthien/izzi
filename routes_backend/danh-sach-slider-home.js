var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

// GET home page
router.get('/', function (req, res, next) {
    db.getAllHomeSliders(function (error, contentList) {
        if(error){
            return;
        }
        console.log(contentList);
        res.render('admin/danh-sach-slider-home', {data: contentList});
    });
});

router.post('/xoa', function (req, res, next) {

    var obj = req.body;
    var id = obj.id;
    db.removeSlider(id, function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

router.post('/update_order', function (req, res, next) {


    var obj = req.body;
    var id = obj.id;
    var order = obj.order;

    db.updateOrderSliderHome(order,id,function (error,result) {
        if(error){
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        if(result){
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        }

    });

});

module.exports = router;
