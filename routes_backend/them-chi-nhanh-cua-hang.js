var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {

    res.render('admin/them-chi-nhanh-cua-hang', {});

});

router.post('/them', function (req, res, next) {
    if(!req.session.username) {
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Hết phiên làm việc."));
        return;
    }
    var obj = req.body;

    var id = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var entity_code = api.ENUM_ENTITY_CODE.BRA;
    var created_date = api.getNow();
    var created_by = req.session.username;
    var status = 1;
    db.insertChiNhanhCuaHang(id,obj.address,obj.toado, null,
                            created_date,created_by,entity_code,status,null, function (error, result) {
            if (error) {
                return;
            }

            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    })


});


module.exports = router;
