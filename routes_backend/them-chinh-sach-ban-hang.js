var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    res.render('admin/them-chinh-sach-ban-hang', {});
});


router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


router.post('/them', function (req, res, next) {
    var obj = req.body;
    var tag = obj.tag;
    var order = obj.order;
    var title_vi = obj.title_vi;
    var html_vi = obj.html_vi;
    var title_en = obj.title_en;
    var html_en = obj.html_en;
    var image = obj.image;
    var id = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var entity_code = api.ENUM_ENTITY_CODE.MAN;
    var created_date = api.getNow();
    var created_by = req.session.username;
    var status = 1;
    var meta_title_vi = obj.meta_title_vi;
    var meta_description_vi = obj.meta_description_vi;
    var meta_keyword_vi = obj.meta_keyword_vi;
    var meta_description_en = obj.meta_description_en;
    var meta_keyword_en = obj.meta_keyword_en;
    var meta_title_en = obj.meta_title_en;
    var need_show = obj.need_show;
    if(need_show) status = 2;
    var content_link = obj.banner_link;
    var content_image = image;
    var content_status = obj.show_banner;
    var content_category_id = "6B89C7E1-B3EF-45AF-AA42-7D8053F8C10A";
    db.insertChinhSachBanHang(id,entity_code,created_date,created_by,title_vi,tag,content_category_id,html_vi,status,order,image, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }
        var idVi = uuidv1();
        db.insertMucDuLieu(id, "", title_vi, "", html_vi, created_by, created_date, entity_code, status, "vi",meta_keyword_vi,meta_description_vi,meta_title_vi, idVi, function (e1, ressult) {
            if (e1) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể thêm vào mục dữ liệu tiếng việt"));
                return;
            }
            var idEn = uuidv1();
            db.insertMucDuLieu(id, "", title_en, "", html_en, created_by, created_date, entity_code, status, "en",meta_keyword_en,meta_description_en,meta_title_en, idEn, function (e3, ressult) {
                if (e3) {
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "Không thể thêm vào mục dữ liệu tiếng anh"));
                    return;
                }
                db.updateBannerContent(id, content_link, content_image, content_status, function (error, result) {
                    if (error) {
                        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, ""));
                        return;
                    }
                    res.json(api.getResponse(api.SUCC_EXEC, {
                        id: id
                    }, ""));
                });


            });

        });

    });


});

module.exports = router;
