var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug');

// GET home page
router.get('/', function (req, res, next) {
    res.render('admin/chinh-sua-banner-khuyen-mai', {});
});

router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            // console.log(field, value);
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            // console.log(field, file);
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            // res.writeHead(200, {'content-type': 'text/plain'});
            // res.write('received fields:\n\n '+util.inspect(fields));
            // res.write('\n\n');
            // res.end('received files:\n\n '+util.inspect(files));
            api.sync_image_library(req,function (text) {
                db.updateImageLibJson(text,function (error23, rsos) {

                })
            });
            db.capnhatBannerSanPham(file_name, function (error, result) {
                if (error) {
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));
            });


        });
    form.parse(req);
});


module.exports = router;
