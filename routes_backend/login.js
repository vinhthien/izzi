var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var uuidV4 = require('uuid/v4');
var crypto = require('crypto');
var api = require('../api/response');
var config = require('../config.json');
var dbconfig = require('../'+config.db_config_name);

// GET home page
router.get('/', function(req, res, next) {
    req.session.username = null;
    req.session.user_id = null;
    res.render('admin/loginPage');

});


router.post('/validate', function (req, res) {
    if(req.session.token && req.session.email) {
        res.json(api.getResponse(api.TOKEN_EXIST, null, "Response from login"));
    }else {
        res.json(api.getResponse(api.ERRO_TOKEN_NOT_EXIST, null, "Response from login"));
    }
});

router.post('/', function (req, res) {


    var obj = req.body;
    var username = obj.username;
    var app_code_name = obj.app_code_name;
    var app_name = obj.app_name;
    var app_version = obj.app_version;
    var platform = obj.platform;
    var user_agent = obj.user_agent;
    var product = obj.product;
    var product_sub = obj.product_sub;
    var vendor = obj.vendor;

    function getClientIp(req) {
        return (req.headers["X-Forwarded-For"] ||
            req.headers["x-forwarded-for"] ||
            '').split(',')[0] ||
            req.client.remoteAddress;
    }
    var ip = getClientIp(req);
    var password = crypto.createHash('md5').update(obj.password).digest("hex");
    db.validateUser(username, function (err, results) {
        if (err) {
            return;
        }
        if (results.length > 0) {
            var socket = req.app.get('socket');
            if (username == results[0].user_name && password == results[0].password) {
                req.session.username = username;
                req.session.user_id = results[0].id;
                db.getRoleByUserId(results[0].id,function (error4, rsa) {
                    if(error4){
                        res.json(api.getResponse(api.ERRO_INVALID_AUTH, null, "Không thể lấy danh sách quyền"));
                        return;
                    }
                    var user_role = {
                        array: rsa
                    };

                    var direct_route = null;
                    db.getRoleCanSeeByUserId(results[0].id,function (error5, rhg) {
                        if (req.session.user_id == "719AD10A-04F9-4FD8-9351-1B2602277967") { // admin
                            direct_route = "danh-sach-san-pham-don-vi";
                        } else {
                            if (rhg.length > 0) {
                                direct_route = rhg[0].direct_route;
                            }
                        }
                        if (direct_route == null) {
                            req.session.username = null;
                            req.session.user_id = null;
                        }
                        console.log("user_role")
                        console.log(user_role)
                        // req.session.user_role = user_role;
                        res.json(api.getResponse(api.SUCC_LOGIN, {
                            direct_route : direct_route
                        }, "Response from login"));
                    })

                })

            } else {
                res.json(api.getResponse(api.ERRO_INVALID_AUTH, null, "Wrong username or password."));
            }

        } else {
            res.json(api.getResponse(api.ERRO_INVALID_AUTH, null, "Wrong username or password."));
        }
    });
});
module.exports = router;
