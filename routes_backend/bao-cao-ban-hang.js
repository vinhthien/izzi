var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');

function sync_product_data(data) {
    var array = [];
    var current_product_id = null;
    for(var index=0;index<data.length;index++){
        var obj = data[index];
        var product_id = obj.product_id;
        var quantity = Number(obj.product_quantity);
        var name = obj.product_name;
        var price = Number(obj.product_price);
        var total = quantity*price;
        if(index == 0){
            current_product_id = product_id;
            array.push({
                product_id: product_id,
                name: name,
                quantity: quantity,
                total : total
            })
        }else {
            if(product_id == current_product_id){
                array[array.length-1].total+=total;
                array[array.length-1].quantity+=quantity;
            }else {
                current_product_id = product_id;
                array.push({
                    product_id: product_id,
                    name: name,
                    quantity: quantity,
                    total : total
                })
            }
        }
    }
    return array;
}

function get_data(start_date, end_date, sort_type, order_status, callback) {
    if(sort_type == 1){ // sort theo san pham
        db.getStaticsSortProduct(order_status,start_date,end_date,function (error, data) {
            if (error) {
                return;
            }
            console.log(44242222);
            console.log(data);
            callback(data);

        })
    }else if(sort_type == 2){ // sort theo nguoi mua
        db.getStaticsSortPerson(order_status,start_date,end_date,function (error,data) {
            if (error) {
                return;
            }
            callback(data);
        })
    }
}

function get_data_nostatus(start_date, end_date, sort_type, callback) {
    if(sort_type == 1){ // sort theo san pham
        db.getStaticsSortProductNoStatus( start_date,end_date,function (error, data) {
            if (error) {
                return;
            }
            console.log(44242222);
            console.log(tong);
            var tong=0;
            for (i=0;i<data.length;i++){
                tong+=data[i].total_after_promotion;
            }

            callback(data, tong);

        })
    }else if(sort_type == 2){ // sort theo nguoi mua
        db.getStaticsSortPersonNoStatus( start_date,end_date,function (error,data) {
            if (error) {
                return;
            }
            var tong=0;
            for (i=0;i<data.length;i++){
                tong+=data[i].total_amount;
            }
            callback(data,tong);
        })
    }
}



// GET home page
router.get('/', function (req, res, next) {
    var obj = req.body;
    var previous_date = api.getTwoBackDayFromNow();
    var now = api.getNow();
    var sort_type = 1; // sort theo san pham
    var order_status = 0; // don hang da thanh toan
    console.log(order_status);
    console.log(6677);

    get_data_nostatus(previous_date,now,sort_type,function (data, tong) {
        res.render('admin/bao-cao-ban-hang', {
            data:data,
            tong: tong
        });
    })

});

router.post('/get-data', function (req, res, next) {
    var obj = req.body;
    var option_date = obj.option_date;
    var option_sort = obj.option_sort;
    var start_date = null;
    var end_date = null;
    var order_status = obj.order_status; // don hang da thanh toan
    console.log(order_status);
    console.log(6677);
    if(option_date == 1){
        start_date = api.getTwoBackDayFromNow();
        end_date = api.getNow();
    }else if(option_date == 2){
        start_date = api.getPreviousWeekDayFromNow();
        end_date = api.getNow();
    }else if(option_date == 3){
        //
    }
    console.log("start_date "+ start_date);
    console.log("end_date "+ end_date);
    console.log("option_sort "+ option_sort);
    get_data(start_date,end_date,option_sort,order_status,function (data) {
        res.json(api.getResponse(api.SUCC_EXEC, data, ""));
    })

});


router.post('/get-data-custom', function (req, res, next) {
    var obj = req.body;
    var option_sort = obj.option_sort;
    var start_date = obj.start_date;
    var end_date = obj.end_date;
    var order_status = obj.order_status; // don hang da thanh toan
    console.log(order_status);
    console.log(6677);
    console.log("start_date "+ start_date);
    console.log("end_date "+ end_date);
    console.log("option_sort "+ option_sort);
    get_data(start_date,end_date,option_sort,order_status,function (data) {
        console.log("data "+ data.length);
        res.json(api.getResponse(api.SUCC_EXEC, data, ""));
    })

});

router.post('/get-data-custom-nostatus', function (req, res, next) {
    var obj = req.body;
    var option_sort = obj.option_sort;
    var start_date = obj.start_date;
    var end_date = obj.end_date;

    console.log(6677);
    console.log("start_date "+ start_date);
    console.log("end_date "+ end_date);
    console.log("option_sort "+ option_sort);
    get_data_nostatus(start_date,end_date,option_sort, function (data) {
        console.log("data "+ data.length);
        console.log(9898989);
        res.json(api.getResponse(api.SUCC_EXEC, data, ""));
    })

});

router.post('/get-data-nostatus', function (req, res, next) {
    var obj = req.body;
    var option_sort = obj.option_sort;
    var option_date = obj.option_date;
    var start_date=null;
    var end_date=null;
    if(option_date == 1){
        start_date = api.getTwoBackDayFromNow();
        end_date = api.getNow();
    }else if(option_date == 2){
        start_date = api.getPreviousWeekDayFromNow();
        end_date = api.getNow();
    }else if(option_date == 3){
        //
    }

    get_data_nostatus(start_date,end_date,option_sort, function (data) {
        console.log("data "+ data.length);
        console.log(9898989);
        res.json(api.getResponse(api.SUCC_EXEC, data, ""));
    })

});

module.exports = router;
