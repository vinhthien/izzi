var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable');
var fs = require('fs');
const uuidv1 = require('uuid/v1');

// GET home page
router.get('/', function (req, res, next) {
    res.render('admin/them-slider-home', {});
});


router.post('/', function (req, res, next) {
    var form = new formidable.IncomingForm(),
        files = [],
        fields = [];

    form.uploadDir = "upload";
    var file_name = "";
    form
        .on('field', function (field, value) {
            fields.push([field, value]);
        })
        .on('file', function (field, file) {
            files.push([field, file]);
            fs.rename(file.path, form.uploadDir + "/" + file.name);
            file_name = file.name;
        })
        .on('end', function () {
            console.log('-> upload done');
            res.json(api.getResponse(api.SUCC_EXEC, file_name, ""));

        });
    form.parse(req);
});


router.post('/them', function (req, res, next) {

    if(!req.session.username){
        res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "hết phiên làm việc"));
        return;
    }
    var obj = req.body;
    var order = obj.order;
    var image = obj.image;
    var title_vi = "";
    var des_vi = "";
    var html_vi = "";
    var title_en = "";
    var des_en = "";
    var html_en = "";



    var id = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'
    var entity_code = api.ENUM_ENTITY_CODE.HOM;
    var created_date = api.getNow();
    var created_by = req.session.username;
    var status = 1;
    db.insertSlider(id,image,title_vi,html_vi,des_vi,created_by,created_date,"",entity_code,order,status,function (error, result) {
        if (error) {
            console.log(error);
            res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
            return;
        }
        var idVi = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'

        db.insertMucDuLieu(id,"",title_vi,des_vi,html_vi,created_by,created_date,entity_code,status,"vi","","","",idVi, function (e1, ressult) {
            if (e1) {
                console.log(e1);
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "không thể nhập dữ liệu tiếng việt"));
                return;
            }
            var idEn = uuidv1(); // ⇨ 'f64f2940-fae4-11e7-8c5f-ef356f279131'

            db.insertMucDuLieu(id,"",title_en,des_en,html_en,created_by,created_date,entity_code,status,"en","","","",idEn, function (e2, ressult) {
                if (e2) {
                    console.log(e2);
                    res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "không thể nhập dữ liệu tiếng anh"));
                    return;
                }
                res.json(api.getResponse(api.SUCC_EXEC, {
                    id: id
                }, ""));
            });
        });
    });

});

module.exports = router;
