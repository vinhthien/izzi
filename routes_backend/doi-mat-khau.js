var express = require('express');
var router = express.Router();
var db = require('../databases/database');
var utility = require('./utility');
var api = require('../api/response');
var formidable = require('formidable')
var util = require('util');
var fs = require('fs');
const uuidv1 = require('uuid/v1');
const html2pug = require('html2pug')
var crypto = require('crypto');

// GET home page
router.get('/', function (req, res, next) {

    var user_id = req.session.user_id;

    db.getUserById(user_id,function (error2, userObj) {
        if (error2){
            return;
        }
        res.render('admin/doi-mat-khau', {
            user_name:userObj[0].user_name,
            password:userObj[0].password,
            user_id:user_id
        });
    })

});


router.post('/sua', function (req, res, next) {
    var obj = req.body;
    var username = obj.username;
    var password = crypto.createHash('md5').update(obj.password).digest("hex");
    var unhash_password = obj.password;
    var user_id=obj.user_id;
    var ori_password=obj.ori_password;

    var created_date = api.getNow();
    var created_by = req.session.username;

    if(ori_password == unhash_password){
        res.json(api.getResponse(api.SUCC_EXEC, null, ""));
    }else {
        db.updateUserPassword(username,password, function (error, result) {
            if (error) {
                res.json(api.getResponse(api.ERRO_NOT_FOUND, null, "lỗi mạng"));
                return;
            }
            res.json(api.getResponse(api.SUCC_EXEC, null, ""));
        })
    }



});


module.exports = router;
