var moment = require('moment');
var db = require('../databases/database');
var api = require('../api/response');
const uuidv1 = require('uuid/v1');

var thiz = this;

exports.getAllCustomers = function (successCallback, errorCallback) {
    db.getAllCustomers(function(err6, customers) {
        if (err6) {
            if (errorCallback) {
                errorCallback();
            }
            return err6;
        }
        if (customers) {
            if (successCallback) {
                successCallback(customers);
            }
        }
    });
};
