/*
Name: 			UI Elements / Modals - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/

function check_all() {
    var checked = $("#check_all").prop('checked');

    $(".check-coupon").not(".hide").each(function() {
        if(checked){
            $(this).prop('checked', true);
        }else {
            $(this).prop('checked', false);
        }
    });


}
function switch_view() {
    var check_one = $( "#view-one" ).hasClass( "hide" );
    if(check_one){
        $( "#view-two" ).addClass("hide");
        $( "#view-one" ).removeClass("hide");
	}else {
        $( "#view-one" ).addClass("hide");
        $( "#view-two" ).removeClass("hide");
	}

}



function apply() {
	var array = [];
    $(".check-coupon:checked").each(function() {
    	var promotion_id = $(this).attr("promotion-id");
    	var promotion_detail_id = $(this).attr("promotion-detail-id");
    	array.push({
            promotion_id: promotion_id,
            promotion_detail_id: promotion_detail_id
		})
    });
    if(array.length>0){
        var e2 = document.getElementById("status-select");
        var status = e2.options[e2.selectedIndex].value;

        $.ajax({
            url: "danh-sach-coupon/update_coupon",
            type: "post", //send it through get method
            data: JSON.stringify({
                array: array,
                status: status
            }),    contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    $(".check-coupon:checked").each(function() {
                        $(this).closest('tr').find('.not-used').html("");
                        $(this).closest('tr').find('.suspended').html("");
                        if(status == 1){
                            $(this).closest('tr').find('.not-used').html("X");
                        }else if(status == 2) {
                            $(this).closest('tr').find('.suspended').html("X");
                        }
                    });
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa trạng thái coupon thất bại\n'+response.description,
                        type: 'error'
                    });
                }
            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa trạng thái coupon thất bại',
                    type: 'error'
                });
            }
        });
	}
}



(function( $ ) {

    'use strict';

    var datatableInit = function() {
        // var $table = $('#datatable-tabletools');
        //
        // $table.dataTable({
        //     sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
        //     oTableTools: {
        //         sSwfPath: $table.data('swf-path'),
        //         aButtons: []
        //     },
        //     ordering: true
        // }).order.listener($('#sort')[0], 1, function () {
        //     alert(1);
        // });

        // table.order.listener($('#code-col')[0], 1,function(){
        //     var sortedCol = $('#datatable-tabletools').dataTable().fnSettings().aaSorting;
        //     var a = sortedCol;
        //     var b = sortedCol;
        // });
        // table.order.listener($('#name-col')[0], 1,function(){
        //     var info = table.fnSettings().aaSorting;
        //     var idx = info[0][0];
        //     var a = idx;
        //     var b = idx;
        // });
        // table.order.listener($('#start-date-col')[0], 1,function(){
        //     alert(2);
        // });
        // table.order.listener($('#end-date-col')[0], 1,function(){
        //     alert(3);
        // });
        // table.order.listener($('#time-use-col')[0], 1,function(){
        //     alert(4);
        // });
        // table.order.listener($('#state-col')[0], 1,function(){
        //     var sortedCol = $('#datatable-tabletools').dataTable().fnSettings().aaSorting;
        //     var a = sortedCol;
        //     var b= a;
        // });
        // table.order.listener($('#action-col')[0], 1,function(){
        //     alert(6);
        // });
        var $table1 = $('#table-custom');

        $table1.dataTable({
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ]
        });
        var $table2 = $('#datatable-tabletools');

        $table2.dataTable({
            "columnDefs": [
                { "orderable": false, "targets": 0 }
            ]
        });

    };

    $(function() {
        datatableInit();
    });

}).apply( this, [ jQuery ]);

(function( $ ) {

	'use strict';

	/*
	Basic
	*/
	$('.modal-basic').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Sizes
	*/
	$('.modal-sizes').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Modal with CSS animation
	*/
	$('.modal-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		modal: true
	});

	$('.modal-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom',
		modal: true
	});

	/*
	Modal Dismiss
	*/
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	/*
	Modal Confirm
	*/


	/*
	Form
	*/
	$('.modal-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
		modal: true,

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

	/*
	Ajax
	*/
	$('.simple-ajax-modal').magnificPopup({
		type: 'ajax',
		modal: true
	});

}).apply( this, [ jQuery ]);