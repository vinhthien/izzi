
function update(id) {
    var address = $("#ip-address").val();
    var toado = $("#ip-toado").val();



    if (address.length == 0) {
        alert("Chưa thêm địa ");
        return;
    }

    if (toado.length == 0) {
        alert("Chưa thêm toạ độ bản đồ");
        return;
    }
    $.ajax({
        url: "sua-chi-nhanh-cua-hang/sua",
        type: "post", //send it through get method
        data: JSON.stringify({
            id: id,
            address: address,
            toado: toado
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa chi nhánh ['+ address +"] thành công",
                    type: 'success'
                });

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa chi nhánh ['+ address +"] thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Sửa chi nhánh ['+ address +"] thất bại",
                type: 'error'
            });
        }
    });
}
