
function add() {
    var address = $("#ip-address").val();
    var toado = $("#ip-toado").val();

    if (address.length == 0) {
        alert("Chưa thêm địa chỉ chi nhánh");
        return;
    }

    if (toado.length == 0) {
        alert("Chưa thêm toạ độ chi nhánh");
        return;
    }

    $.ajax({
        url: "them-chi-nhanh-cua-hang/them",
        type: "post", //send it through get method
        data: JSON.stringify({
            address: address,
            toado: toado

        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm chi nhánh ['+ address +"] thành công",
                    type: 'success'
                });
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm chi nhánh ['+ address +"] thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm chi nhánh ['+ address +"] thất bại",
                type: 'error'
            });
        }
    });
}
