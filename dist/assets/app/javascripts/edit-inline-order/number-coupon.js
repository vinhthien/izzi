/*global $, window*/
var current_id = null;
var promotion_id = null;
var promotion_detail_id = null;
var ori = null;

function update_order(number,callback) {
    if(promotion_id && promotion_id.length > 0){
        $.ajax({
            url: "danh-sach-coupon/update_promotion_number",
            type: "post", //send it through get method
            data: JSON.stringify({
                id: promotion_id,
                number: number
            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {

                    $('#table-custom > tbody  > tr').each(function() {
                        var row = $(this);
                        var promotion_id_temp = row.find(".canEdit").attr("promotion-id");
                        var promotion_detail_id_temp = row.find(".canEdit").attr("promotion-detail-id");
                        if(promotion_id_temp == promotion_id){
                            row.find(".not-used").html("");
                            row.find(".used").html("");
                            if(number > 0){
                                row.find(".not-used").html("X");
                                row.find(".check-coupon").removeClass("hide");
                            }else {
                                row.find(".used").html("X");
                                row.find(".check-coupon").addClass("hide");
                            }
                        }

                    });
                    callback();
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa thứ tự chương trình thất bại\n' + response.description,
                        type: 'error'
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thứ tự chương trình',
                    type: 'error'
                });
            }
        });
    }else if(promotion_detail_id && promotion_detail_id.length > 0){
        if(number == 0){
            number = 3;
        }
        $.ajax({
            url: "danh-sach-coupon/update_promotion_detail_number",
            type: "post", //send it through get method
            data: JSON.stringify({
                id: promotion_detail_id,
                number: number
            }), contentType: "application/json",
            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    $('#table-custom > tbody  > tr').each(function() {
                        var row = $(this);
                        var promotion_id = row.find(".canEdit").attr("promotion-id");
                        var promotion_detail_id_temp = row.find(".canEdit").attr("promotion-detail-id");
                        if(promotion_detail_id_temp == promotion_detail_id){
                            row.find(".not-used").html("");
                            row.find(".used").html("");
                            if(number == 1){
                                row.find(".not-used").html("X");
                                row.find(".check-coupon").removeClass("hide");
                            }else {
                                row.find(".used").html("X");
                                row.find(".check-coupon").addClass("hide");
                            }
                        }

                    });

                    callback();
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa thứ tự chương trình thất bại\n' + response.description,
                        type: 'error'
                    });
                }
            },
            error: function (xhr) {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thứ tự chương trình',
                    type: 'error'
                });
            }
        });
    }

}

$.fn.editableTableWidget = function (options) {
    'use strict';
    return $(this).each(function () {
        var buildDefaultOptions = function () {
                var opts = $.extend({}, $.fn.editableTableWidget.defaultOptions);
                opts.editor = opts.editor.clone();
                return opts;
            },
            activeOptions = $.extend(buildDefaultOptions(), options),
            ARROW_LEFT = 37, ARROW_UP = 38, ARROW_RIGHT = 39, ARROW_DOWN = 40, ENTER = 13, ESC = 27, TAB = 9,
            element = $(this),
            editor = activeOptions.editor.css('position', 'absolute').hide().appendTo(element.parent()),
            active,
            showEditor = function (select) {
                var can_edit = select.target.className.includes("canEdit");
                if(!can_edit) return;
                // var id = select.target.id.substr(2,select.target.id.length);
                ori = $(select.target).html();
                promotion_id = $(select.target).attr("promotion-id");
                promotion_detail_id = $(select.target).attr("promotion-detail-id");
                active = element.find('td:focus');
                if (active.length) {
                    editor.val(active.text())
                        .removeClass('error')
                        .show()
                        .offset(active.offset())
                        .css(active.css(activeOptions.cloneProperties))
                        .width(active.width())
                        .height(active.height())
                        .focus();
                    if (select) {
                        editor.select();
                    }
                }
            },
            setActiveText = function (need_save) {
                var text = editor.val(),
                    evt = $.Event('change'),
                    originalContent;
                // if (active.text() === text || editor.hasClass('error')) {
                //     return true;
                // }
                if(need_save) {
                        update_order(text, function () {
                            originalContent = active.html();
                            active.text(text).trigger(evt, text);
                            if (evt.result === false) {
                                active.html(originalContent);
                            }
                        })

                }else {
                    originalContent = active.html();
                    active.text(text).trigger(evt, text);
                    if (evt.result === false) {
                        active.html(originalContent);
                    }
                }

            },
            movement = function (element, keycode) {
                if (keycode === ARROW_RIGHT) {
                    return element.next('td');
                } else if (keycode === ARROW_LEFT) {
                    return element.prev('td');
                } else if (keycode === ARROW_UP) {
                    return element.parent().prev().children().eq(element.index());
                } else if (keycode === ARROW_DOWN) {
                    return element.parent().next().children().eq(element.index());
                }
                return [];
            };
        editor.blur(function () {
            var text = editor.val();
            if(text.length == 0){
                alert("Chưa thêm số lần sử dụng");
                return;
            }
            if(promotion_detail_id && promotion_detail_id.length>0){
                if(text != 0 && text != 1){
                    alert("Đây là coupon sử dụng 1 lần");
                    active.html(ori);
                    editor.hide();
                    return;
                }
            }

            setActiveText(true);
            editor.hide();
        }).keyup(function (e) {
            var key = window.event ? e.keyCode : e.which;

            if (e.keyCode === 8 || e.keyCode === 46) {
                setActiveText();
            } else if ( key < 48 || key > 57 ) {
                // return false;
                e.preventDefault();
                e.stopPropagation();;
                return;
            } else {
                setActiveText();
            }
            if (e.which === ENTER) {
                var text = editor.val();
                if(text.length == 0){
                    alert("Chưa thêm số lần sử dụng");
                    return;
                }
                setActiveText();
                editor.hide();
            } else if (e.which === ESC) {
                var text = editor.val();
                if(text.length == 0){
                    alert("Chưa thêm số lần sử dụng");
                    return;
                }
                editor.val(active.text());
                e.preventDefault();
                e.stopPropagation();
                editor.hide();
                active.focus();
            } else if (e.which === TAB) {
                var text = editor.val();
                if(text.length == 0){
                    alert("Chưa thêm số lần sử dụng");
                    return;
                }
                active.focus();
            } else if (this.selectionEnd - this.selectionStart === this.value.length) {
                var possibleMove = movement(active, e.which);
                if (possibleMove.length > 0) {
                    possibleMove.focus();
                    e.preventDefault();
                    e.stopPropagation();
                }
            }
        }).keydown(function (e) {
            var key = window.event ? e.keyCode : e.which;

            if ( (key < 48 || key > 57 ) && key != 8) {
                // return false;
                editor.val(active.text());
                e.preventDefault();
                e.stopPropagation();

            }
        })
            .on('input paste', function () {
                var evt = $.Event('validate');
                active.trigger(evt, editor.val());
                if (evt.result === false) {
                    editor.addClass('error');
                } else {
                    editor.removeClass('error');
                }
            });
        element.on('click keypress dblclick', showEditor)
            .css('cursor', 'pointer')
            .keydown(function (e) {
                var prevent = true,
                    possibleMove = movement($(e.target), e.which);
                if (possibleMove.length > 0) {
                    possibleMove.focus();
                } else if (e.which === ENTER) {
                    showEditor(false);
                } else if (e.which === 17 || e.which === 91 || e.which === 93) {
                    showEditor(true);
                    prevent = false;
                } else {
                    prevent = false;
                }
                if (prevent) {
                    e.stopPropagation();
                    e.preventDefault();
                }
            });

        element.find('td').prop('tabindex', 1);

        $(window).on('resize', function () {
            if (editor.is(':visible')) {
                editor.offset(active.offset())
                    .width(active.width())
                    .height(active.height());
            }
        });
    });

};
$.fn.editableTableWidget.defaultOptions = {
    cloneProperties: ['padding', 'padding-top', 'padding-bottom', 'padding-left', 'padding-right',
        'text-align', 'font', 'font-size', 'font-family', 'font-weight',
        'border', 'border-top', 'border-bottom', 'border-left', 'border-right'],
    editor: $('<input>')
};