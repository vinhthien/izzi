/*
Name: 			UI Elements / Modals - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/
start_date = null;
end_date = null;
$('#ip-begin-date').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
        format: 'DD/MM/YYYY hh:mm A'
    }
});
$('#ip-begin-date').val("");
$('#ip-begin-date').on('apply.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;
    custom_sort(start_date,end_date);
});
$('#ip-begin-date').on('hide.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;
});

function thongKeChiTiet() {
    custom_sort(start_date,end_date);
}

function custom_sort(start_date,end_date) {

    $.ajax({
        url: "thong-ke-ban-hang-chi-tiet/get-data-detail",
        type: "post", //send it through get method
        data: JSON.stringify({
            start_date: start_date,
            end_date: end_date,
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                handle_data(response);
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });
}

function export_excel() {

    var htmls = "";
    var uri = 'data:application/vnd.ms-excel;base64,';
    var template =
        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' +
        '<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">' +
        '</head>' +
        '<body>' +
        '<table>{table}</table>' +
        '</body>' +
        '</html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    htmls+=$("#table-product1").html();


    var formatTemplate = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };



    var ctx = {
        worksheet: 'Worksheet',
        table: "<table>" + htmls + "</table>"
    }

    var link = document.createElement("a");
    link.download = "bao-cao-ban-hang.xls";
    link.href = uri + base64(formatTemplate(template, ctx));
    link.click();
}

function change_sort() {
    var e = document.getElementById("select-date");
    var option_date = e.options[e.selectedIndex].value;
    $( "#btt-thongke" ).addClass( "hide" );
    if(option_date == 2||option_date == "2"){
        $( "#date" ).removeClass( "hide" );
        return;
    }else {
        $( "#date" ).addClass( "hide" );
        start_date=moment().subtract(7, 'days');
        end_date=moment();
    }
    $.ajax({
        url: "thong-ke-ban-hang-chi-tiet/get-data-detail",
        type: "post", //send it through get method
        data: JSON.stringify({
            start_date: start_date,
            end_date: end_date,
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                handle_data(response);
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });

}


function handle_data(response) {
    var table = $('#table-product').DataTable();
    var table1 = $('#table-product1').DataTable();
    table
        .clear()
        .draw();
    table1
        .clear()
        .draw();
    for(var index=0;index<response.data.length;index++){
        var obj = response.data[index];
        var product_id = obj.product_id;
        var product_code = obj.product_code;
        var product_name = obj.product_name;

        var first_store=parseInt(obj.first_store/obj.combo_quantity);
        var end_store=parseInt(obj.end_store/obj.combo_quantity);
        var present_store=parseInt(obj.store/obj.combo_quantity);

        end_store=present_store-end_store;
        first_store=end_store-first_store;

        var total_product_quantity = first_store-end_store;
        var total = total_product_quantity*obj.product_price;
        total = revert_money_to_normal(total+"");


        if(total == "") {
            return;
        }
        total = format_money(total);

        console.log(total);
        console.log(445);
        table.row.add( [
            product_code,
            product_name,
            first_store,
            end_store,
            total_product_quantity,
            total
        ] ).draw();

        table1.row.add( [
            product_code,
            product_name,
            first_store,
            end_store,
            total_product_quantity,
            total
        ] ).draw();
    }
}




// Dữ liệu table
(function( $ ) {

    'use strict';

    var datatableInit = function() {
        var $table = $('#table-product');
        var $table1 = $('#table-product1');

        $table.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table.data('swf-path'),
                aButtons: []
            }
        });

        $table1.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table1.data('swf-path'),
                aButtons: []
            },
            paging: false
        });

    };

    $(function() {
        datatableInit();
    });

}).apply( this, [ jQuery ]);

(function( $ ) {

	'use strict';

	/*
	Basic
	*/
	$('.modal-basic').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Sizes
	*/
	$('.modal-sizes').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Modal with CSS animation
	*/
	$('.modal-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		modal: true
	});

	$('.modal-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom',
		modal: true
	});

	/*
	Modal Dismiss
	*/
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	/*
	Modal Confirm
	*/


	/*
	Form
	*/
	$('.modal-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
		modal: true,

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

	/*
	Ajax
	*/
	$('.simple-ajax-modal').magnificPopup({
		type: 'ajax',
		modal: true
	});

}).apply( this, [ jQuery ]);