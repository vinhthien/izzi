/*
Name: 			UI Elements / Modals - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/

start_date = null;
end_date = null;
var diff;
var mixedChart;
$('#ip-begin-date').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
        format: 'DD/MM/YYYY hh:mm A'
    }
});
$('#ip-begin-date').val("");
$('#ip-begin-date').on('apply.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;
    custom_sort(start_date,end_date)

});



$('#ip-begin-date').on('hide.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;

});
var chart = {
    tong:[],
    date:[],
    amount:[],
    average:[],
    order_id:[]
};
var url=null;
function done(){
    url=mixedChart.toBase64Image();
    document.getElementById("url").src=url;
}

function export_excel() {
    var doc;
    // if(mixedChart.width > mixedChart.height){
    //     doc = new jsPDF('l', 'mm', [mixedChart.width, mixedChart.height]);
    // }
    // else{
    //     doc = new jsPDF('p', 'mm', [mixedChart.height, mixedChart.width]);
    // }
    if ($(document).width() > $(document).height()) {
        var doc = new jsPDF('l', 'pt', [$(document).width(), $(document).height()]); //
    }
    else {
        var doc = new jsPDF('p', 'pt', [$(document).height(), $(document).width()]);
    }
    doc.addImage(url, 'PNG', 10, 10);
    doc.save("bao-cao-doanh-so-theo-nganh-hang.pdf");
}

function custom_sort(start_date,end_date) {

    $.ajax({
        url: "thong-ke-ban-hang/get-data-custom-nostatus",
        type: "post", //send it through get method
        data: JSON.stringify({
            start_date: start_date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
            end_date: end_date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {

                chart.date=[];
                chart.tong=[];
                chart.amount=[];
                chart.average=[];
                chart.order_id=[];
                var c = end_date.startOf('day').diff(start_date.startOf('day'), 'days');
                chart.date[0]=start_date.format("dd/MM/YYYY");
                for (i=0;i<=c;i++){
                    chart.tong[i]=0;
                    if(i>0){
                        chart.date[i]=start_date.add(1, 'd').format("dd/MM/YYYY");
                    }
                }



                if(response.data.length>0){
                    for (i=0;i<response.data.length;i++){
                        response.data[i].product_date=moment(response.data[i].product_date).format("dd/MM/YYYY");
                    }

                    for (i=0;i<response.data.length;i++){
                        for (j = 0; j < chart.date.length; j++) {
                            if (chart.date[j] == response.data[i].product_date) {
                                chart.amount[j] = 1;
                                if(chart.order_id[j]!=""){
                                    chart.order_id[j] = response.data[i].order_id;
                                }
                            } else {
                                chart.amount[j] = 0;
                                chart.order_id[j] = "";
                            }
                            chart.average[j]=0;
                        }
                    }




                    for (i=0;i<response.data.length;i++){
                        {

                            for (j = 0; j < chart.date.length; j++) {
                                if (chart.date[j] == response.data[i].product_date) {
                                    chart.tong[j] += response.data[i].total_after_promotion;
                                    if(chart.order_id[j]!=response.data[i].order_id){
                                        chart.amount[j]++;
                                    }
                                }
                            }
                        }
                    }
                    for (i = 0; i < chart.date.length; i++){
                        if(chart.amount[i]>0){
                            chart.average[i]=(chart.tong[i]/chart.amount[i]).toFixed(0);
                        }
                    }

                }
                thongKeBanHangChart(false);
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });

}




function change_sort(start_date,end_date) {
    var e = document.getElementById("select-date");
    var option_date = e.options[e.selectedIndex].value;

    if(option_date == 2){
        $( "#date" ).removeClass( "hide" );
        return;
    }else {
        $( "#date" ).addClass( "hide" );
        start_date =  moment().subtract(7, 'days');
        end_date = moment();
    }


    $.ajax({
        url: "thong-ke-ban-hang/get-data-custom-nostatus",
        type: "post", //send it through get method
        data: JSON.stringify({
            start_date: start_date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
            end_date: end_date.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                chart.date=[];
                chart.tong=[];
                chart.amount=[];
                chart.average=[];
                chart.order_id=[];

                var c = end_date.startOf('day').diff(start_date.startOf('day'), 'days');
                chart.date[0]=start_date.add(1, 'd').format("dd/MM/YYYY");
                for (i=0;i<c;i++){
                    chart.tong[i]=0;
                    if(i>0){
                        chart.date[i]=start_date.add(1, 'd').format("dd/MM/YYYY");
                    }
                }

                if(response.data.length>0) {
                    for (i = 0; i < response.data.length; i++) {
                        response.data[i].product_date = moment(response.data[i].product_date).format("dd/MM/YYYY");
                    }

                    for (i=0;i<response.data.length;i++){
                        for (j = 0; j < chart.date.length; j++) {
                            if (chart.date[j] == response.data[i].product_date) {
                                chart.amount[j] = 1;
                                if(chart.order_id[j]!=""){
                                    chart.order_id[j] = response.data[i].order_id;
                                }
                            } else {
                                chart.amount[j] = 0;
                                chart.order_id[j] = "";
                            }
                            chart.average[j]=0;
                        }
                    }



                    for (i=0;i<response.data.length;i++){
                        {

                            for (j = 0; j < chart.date.length; j++) {
                                if (chart.date[j] == response.data[i].product_date) {
                                    chart.tong[j] += response.data[i].total_after_promotion;
                                    if(chart.order_id[j]!=response.data[i].order_id){
                                        chart.amount[j]++;
                                    }
                                }
                            }
                        }
                    }

                    for (i = 0; i < chart.date.length; i++){
                        if(chart.amount[i]>0){
                            chart.average[i]=(chart.tong[i]/chart.amount[i]).toFixed(0);
                        }
                    }


                }
                thongKeBanHangChart(false);
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });

}


function thongKeBanHangChart(isLoad) {

    if(isLoad==true){
        change_sort(moment().subtract(7, 'days').format("YYYY-MM-DDTHH:mm:ss.SSSZ"),moment().format("YYYY-MM-DDTHH:mm:ss.SSSZ"));
    }
    $( "#btt-loadChart" ).addClass( "hide" );


    if(mixedChart){
        mixedChart.destroy();
    }
    var ctx = document.getElementById('Chart-thongkebanhang').getContext('2d');
    mixedChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: chart.date,
            datasets: [{
                label: 'Doanh số',
                data: chart.tong,
                backgroundColor: 'rgba(0, 119, 204, 0.8)',
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
            }, {
                    label: 'Đơn hàng',
                    data: chart.amount,
                    fill: false,
                    backgroundColor:  'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    type: 'line'
                },{
                        label: 'Giá trị trung bình/Đơn hàng',
                        data: chart.average,
                        fill: false,
                        backgroundColor:  'rgba(255, 159, 64, 0.2)',
                        borderColor: 'rgba(255, 159, 64, 1)',
                        type: 'line'
            }]
        },
        options: {
            bezierCurve : false,
            animation: {
                onComplete: done
            },
            tooltips: {
                callbacks: {
                    label: function(t, d) {
                        var xLabel = d.datasets[t.datasetIndex].label;
                        var yLabel = t.yLabel >= 1000 ?  t.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") :  t.yLabel;
                        return xLabel + ': ' + yLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    ticks: {
                        callback: function(value, index, values) {
                            if (parseInt(value) >= 1000) {
                                return  value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                            } else {
                                return  value;
                            }
                        }
                    }
                }]
            }
        }
    });



}


