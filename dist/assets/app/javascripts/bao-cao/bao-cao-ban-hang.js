/*
Name: 			UI Elements / Modals - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/
start_date = null;
end_date = null;
$('#ip-begin-date').daterangepicker({
    timePicker: true,
    startDate: moment().startOf('hour'),
    endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
        format: 'DD/MM/YYYY hh:mm A'
    }
});
$('#ip-begin-date').val("");
$('#ip-begin-date').on('apply.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;
    var e2 = document.getElementById("select-status");
    var order_status = e2.options[e2.selectedIndex].value;
    custom_sort(start_date,end_date,order_status);
});
$('#ip-begin-date').on('hide.daterangepicker', function (ev, picker) {
    start_date = picker.startDate;
    end_date = picker.endDate;
});

function custom_sort(start_date,end_date,order_status) {

    var e1 = document.getElementById("select-sort");
    var option_sort = e1.options[e1.selectedIndex].value;
    if (order_status==0){
        $.ajax({
            url: "bao-cao-ban-hang/get-data-custom-nostatus",
            type: "post", //send it through get method
            data: JSON.stringify({
                start_date: start_date,
                end_date: end_date,
                option_sort: option_sort,
            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    handle_data(option_sort,response);
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Lọc thống kê thất bại',
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }
        });
    }
    else{
        $.ajax({
            url: "bao-cao-ban-hang/get-data-custom",
            type: "post", //send it through get method
            data: JSON.stringify({
                start_date: start_date,
                end_date: end_date,
                option_sort: option_sort,
                order_status: order_status,
            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    handle_data(option_sort, response);
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Lọc thống kê thất bại',
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }
        });
    }
}

function export_excel() {

    var htmls = "";
    var uri = 'data:application/vnd.ms-excel;base64,';
    var template =
        '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">' +
        '<head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' +
        '<meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8">' +
        '</head>' +
        '<body>' +
        '<table>{table}</table>' +
        '</body>' +
        '</html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    var sort_type = $('#select-sort').find(":selected").val();
    if(sort_type == 1){
        htmls+=$("#product-person1").html();
    }else if(sort_type == 2){
        htmls+=$("#table-person1").html();
    }

    var formatTemplate = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };



    var ctx = {
        worksheet: 'Worksheet',
        table: "<table>" + htmls + "</table>"
    }

    var link = document.createElement("a");
    link.download = "bao-cao-ban-hang.xls";
    link.href = uri + base64(formatTemplate(template, ctx));
    link.click();
}

function handle_data(option_sort,response) {
    var doanhThu = 0;
    if(option_sort == 1){
        $( "#product" ).removeClass( "hide" );
        $( "#person" ).addClass( "hide" );
        var table = $('#product-person').DataTable();
        var table1 = $('#product-person1').DataTable();
        doanhThu=0;
        table
            .clear()
            .draw();
        table1
            .clear()
            .draw();
        for(var index=0;index<response.data.length;index++){
            var obj = response.data[index];
            var product_id = obj.product_id;
            var product_code = obj.product_code;
            var product_name = obj.product_name;
            var total_product_quantity = obj.total_product_quantity;
            var total = obj.total_after_promotion;
            doanhThu+=parseInt(total);
            total = revert_money_to_normal(total+"");
            if(total == "") {
                return;
            }
            total = format_money(total);

            console.log(total);
            console.log(445);
            table.row.add( [
                product_code,
                product_name,
                total_product_quantity,
                total
            ] ).draw();


            table1.row.add( [
                product_code,
                product_name,
                total_product_quantity,
                total
            ] ).draw();

        }

    }else if(option_sort == 2){
        $( "#person" ).removeClass( "hide" );
        $( "#product" ).addClass( "hide" );
        var table = $('#table-person').DataTable();
        var table1 = $('#table-person1').DataTable();
        doanhThu=0;
        table
            .clear()
            .draw();
        table1
            .clear()
            .draw();
        for(var index=0;index<response.data.length;index++){
            var obj = response.data[index];
            var fullname = obj.fullname;
            var email = obj.email;
            var total_amount = obj.total_amount;
            doanhThu += parseInt(total_amount);
            total_amount = revert_money_to_normal(total_amount+"");
            if(total_amount == "") return;
            total_amount = format_money(total_amount);

            var total_quantity = obj.total_quantity;
            table.row.add( [
                fullname,
                email,
                total_quantity,
                total_amount
            ] ).draw();
            table1.row.add( [
                fullname,
                email,
                total_quantity,
                total_amount
            ] ).draw();
        }
    }
    doanhThu=format_money(doanhThu + "");
    $( "#lb-doanhthu" ).text(doanhThu);
}
function change_sort() {
    var e = document.getElementById("select-date");
    var option_date = e.options[e.selectedIndex].value;
    var e1 = document.getElementById("select-sort");
    var option_sort = e1.options[e1.selectedIndex].value;
    var e2 = document.getElementById("select-status");
    var order_status = e2.options[e2.selectedIndex].value;

    if(option_date == 3){
        $( "#date" ).removeClass( "hide" );
        return;
	}else {
        $( "#date" ).addClass( "hide" );
	}
    if(order_status=="0"){
        $.ajax({
            url: "bao-cao-ban-hang/get-data-nostatus",
            type: "post", //send it through get method
            data: JSON.stringify({
                option_date: option_date,
                option_sort: option_sort,

            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    handle_data(option_sort,response);

                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Lọc thống kê thất bại',
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }
        });
    }
    else
        $.ajax({
        url: "bao-cao-ban-hang/get-data",
        type: "post", //send it through get method
        data: JSON.stringify({
            option_date: option_date,
            option_sort: option_sort,
            order_status: order_status
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                handle_data(option_sort,response);

            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });
}

function change_sort_type() {
    var e = document.getElementById("select-date");
    var option_date = e.options[e.selectedIndex].value;
    var e1 = document.getElementById("select-sort");
    var option_sort = e1.options[e1.selectedIndex].value;
    var e2 = document.getElementById("select-status");
    var order_status = e2.options[e2.selectedIndex].value;
    if(option_date == 3){
        custom_sort(start_date,end_date,order_status);

        return;
    }else {
        $( "#date" ).addClass( "hide" );
    }
    if(order_status=="0"){
        $.ajax({
            url: "bao-cao-ban-hang/get-data-nostatus",
            type: "post", //send it through get method
            data: JSON.stringify({
                option_date: option_date,
                option_sort: option_sort,

            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    handle_data(option_sort,response);

                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Lọc thống kê thất bại',
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }
        });
    }
    else
        $.ajax({
        url: "bao-cao-ban-hang/get-data",
        type: "post", //send it through get method
        data: JSON.stringify({
            option_date: option_date,
            option_sort: option_sort,
            order_status: order_status,

        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                handle_data(option_sort,response);

            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });
}





function change_sort_status() {
    var e = document.getElementById("select-date");
    var option_date = e.options[e.selectedIndex].value;
    var e1 = document.getElementById("select-sort");
    var option_sort = e1.options[e1.selectedIndex].value;
    var e2 = document.getElementById("select-status");
    var order_status = e2.options[e2.selectedIndex].value;
    console.log(order_status);
    console.log(6657);
    if(option_date == 3){
        custom_sort(start_date,end_date,order_status);

        return;
    }else {
        $( "#date" ).addClass( "hide" );
    }
    if(order_status=="0"){
        $.ajax({
            url: "bao-cao-ban-hang/get-data-nostatus",
            type: "post", //send it through get method
            data: JSON.stringify({
                option_date: option_date,
                option_sort: option_sort,

            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    handle_data(option_sort,response);

                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Lọc thống kê thất bại',
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }
        });
    }
    else
        $.ajax({
        url: "bao-cao-ban-hang/get-data",
        type: "post", //send it through get method
        data: JSON.stringify({
            option_date: option_date,
            option_sort: option_sort,
            order_status: order_status,

        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                handle_data(option_sort,response);

            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lọc thống kê thất bại',
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lọc thống kê thất bại',
                type: 'error'
            });
        }
    });
}



(function( $ ) {

    'use strict';

    var datatableInit = function() {
        var $table = $('#table-person');

        $table.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table.data('swf-path'),
                aButtons: []
            }
        });

        var $table1 = $('#product-person');

        $table1.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table.data('swf-path'),
                aButtons: []
            }
        });

        var $table2 = $('#table-person1');


        $table2.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table.data('swf-path'),
                aButtons: [],
            },
            paging: false
        });

        var $table3 = $('#product-person1');

        $table3.dataTable({
            sDom: "<'text-right mb-md'T>" + $.fn.dataTable.defaults.sDom,
            oTableTools: {
                sSwfPath: $table.data('swf-path'),
                aButtons: []
            },
            paging: false
        });

    };

    $(function() {
        datatableInit();
    });

}).apply( this, [ jQuery ]);

(function( $ ) {

	'use strict';

	/*
	Basic
	*/
	$('.modal-basic').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Sizes
	*/
	$('.modal-sizes').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Modal with CSS animation
	*/
	$('.modal-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		modal: true
	});

	$('.modal-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom',
		modal: true
	});

	/*
	Modal Dismiss
	*/
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	/*
	Modal Confirm
	*/


	/*
	Form
	*/
	$('.modal-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
		modal: true,

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

	/*
	Ajax
	*/
	$('.simple-ajax-modal').magnificPopup({
		type: 'ajax',
		modal: true
	});

}).apply( this, [ jQuery ]);



