function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: '#', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link.length)
    {
        callback(image_link);
    }
}




function setDataCkeditor() {
    var vi = $( "#vi-label" ).attr( "data" );
    CKEDITOR.instances.editor1.setData(vi);
    var en = $( "#en-label" ).attr( "data" );
    CKEDITOR.instances.editor2.setData(en);
}

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function format_money() {
    var number = $("#ip-price").val();
    number = parse_number(number);
    $("#ip-price").val(number);
}
function update() {
    var order = $("#ip-order").val();

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp");
        return;
    }

    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();




    // if (meta_keyword_vi.length == 0) {
    //     alert("Chưa thêm meta key tiếng việt");
    //     return;
    // }
    // if (meta_description_vi.length == 0) {
    //     alert("Chưa thêm meta description tiếng việt");
    //     return;
    // }
    // if (meta_title_vi.length == 0) {
    //     alert("Chưa thêm meta title tiếng việt");
    //     return;
    // }

    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
    }
    uploadFile(function (url) {
        $.ajax({
            url: "sua-slider-home/sua",
            type: "post", //send it through get method
            data: JSON.stringify({
                meta_title_vi:meta_title_vi,
                meta_description_vi:meta_description_vi,
                meta_keyword_vi:meta_keyword_vi,
                meta_description_en:meta_description_en,
                meta_keyword_en:meta_keyword_en,
                meta_title_en:meta_title_en,
                order: order,
                image: url,
                id: global_id
            }),    contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa thành công',
                        type: 'success'
                    });

                    $("#ip-image-sample").attr("src", '../upload/'+url);


                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa  thất bại\n'+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thất bại',
                    type: 'error'
                });
            }
        });
    })
}