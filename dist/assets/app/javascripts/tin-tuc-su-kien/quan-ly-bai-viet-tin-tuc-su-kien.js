

function xoa_lien_ket(content_id,content_des) {
    $.ajax({
        url: "quan-ly-bai-viet-mon-an-ngon/xoa",
        type: "post", //send it through get method
        data: JSON.stringify({
            content_id: content_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa bài viết '+ content_id +" thành công",
                    type: 'success'
                });
                var temp = 'content'+content_id;
                var row = document.getElementById(temp);
                row.parentNode.removeChild(row);
                $("#bai_viet_select").append("<option value='"+content_id+"' selected>"+content_des+"</option>");
                $('#bai_viet_select').trigger('change');
                re_set_number_table()
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa bài viết '+ content_id +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Xóa bài viết ['+ content_id +"] thất bại",
                type: 'error'
            });
        }
    });
}

function add() {
    var content_des = $("#bai_viet_select option:selected").text();
    var content_id = $("#bai_viet_select option:selected").val();
    if(!content_id || content_id == -1 || content_des.length == 0){
        alert("chưa chọn bài viết")
        return;
    }

    var rowCount = $('#tb-mon-ngon tr').length;
    if (rowCount > 5) {
        alert("Danh sách không được quá 05 bài viết");
        return;
    }

    $.ajax({
        url: "quan-ly-bai-viet-mon-an-ngon/them",
        type: "post", //send it through get method
        data: JSON.stringify({
            content_id: content_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm bài viết thành công',
                    type: 'success'
                });

                var temp_id = "content"+content_id;
                $("#bai_viet_select option[value='" + content_id + "']").remove();
                $('#bai_viet_select').trigger('change');
                var text = 'onclick=\'xoa_lien_ket("'+content_id+'","'+content_des+'")\'';
                var rowCount = $('#tb-mon-ngon tr').length;
                $('#tb-mon-ngon').append('<tr id=\''+temp_id+'\'>' +
                    '<td>'+rowCount+'</td>' +
                    '<td>'+content_des+'</td>' +
                    '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');
                re_set_number_table()
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm bài viết thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });

}

function re_set_number_table() {
    var table = document.getElementById('tb-mon-ngon');

    var rowLength = table.rows.length;

    for(var i=1; i<rowLength; i+=1){
        var row = table.rows[i];

        //your code goes here, looping over every row.
        //cells are accessed as easy

        var cellLength = row.cells.length;
        var cell = row.cells[0];
        cell.textContent = i;
    }
}

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}


