
function setBadge(number) {
    if (number == 0) {
        $('#notification-span').hide();
        $('#notification-span1').hide();
        return;
    }
    $('#notification-span').show();
    $('#notification-span1').show();
    $('#notification-span').text(number);
    $('#notification-span1').text(number);
}

function applyUsernameFromSession() {
    $.ajax({
        url: "utility/getUsername",
        type: "post", //send it through get method
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                var username =  response.data.username;
                var user_id =  response.data.user_id;
                if (username == null) {
                    window.location.href =  "/admin/login";
                    return;
                }

                $("#logged_username").html(username);

                // var address = response.data.socket_url;
                // var details = {
                //     resource: response.data.socket_resource
                // };
                //
                // var client = io.connect(address, details);
                // client.on('connect', function () {
                //     client.emit("update_user_socket",user_id,client.id);
                //     });
                // client.on('on-notification', function (title,description,icon,link) {
                //         show_notification(title,description,icon,link);
                //     });
                // client.on('test', test);
                // function test() {
                //     console.log("Socket working");
                // }

            }

        },
        error: function (xhr) {
            // nothing
        }
    });
}

function getUnreadNotification() {
    $.ajax({
        url: "utility/getUnreadNotificationBySession",
        type: "post", //send it through get method
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                if(response.data && response.data.length > 0){
                    for(var index in response.data){
                        var obj = response.data[index];
                        var title = obj.notification_title;
                        var icon = obj.notification_icon;
                        var description = obj.notification_description;
                        var link = obj.notification_link;
                        show_notification(title,description,icon,link);
                    }
                }
            }
        },
        error: function (xhr) {
            // nothing
        }
    });
}

function start_fake_socket() {
    window.setInterval(function(){
        /// call your function here
        getUnreadNotification();
    }, 2000);
}
start_fake_socket();
function show_notification(title,description,icon,link) {
    new PNotify({
        title: title,
        text: description,
        type: 'custom',
        addclass: 'notification-primary'
    });

    // applyUserNotification();
    // applyUnreadNotification();

    var current_number = parseInt($('#notification-span').text());
    if(current_number < 5) {
        $("#ul-notification").prepend(
            '<li>' +
            '   <a class="clearfix" href=\'' + link + '\'>' +
            '       <div class="image">' +
            '           <i class=\'' + icon + ' bg-danger\'></i></div>' +
            '       <span class="title"><b>' + title + '</b></span>' +
            '       <span class="message">' + description + '</span></a></li>');
    }
    setBadge(current_number+1);
}


function applyUserNotification() {
    $.ajax({
        url: "utility/getUserNotification",
        type: "post", //send it through get method
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                var newsItem = $("#ul-notification");
                var htmlList = "";
                for(var index=0;index<response.data.length;index++){
                    var obj = response.data[index];
                    var description = moment(obj.created_date).fromNow();
                    var item = newsItem.clone();
                    item.html($("li:first-child", item));

                    $("a", item).attr("href", obj.link);
                    $(".image i", item).addClass(obj.icon);
                    $(".title", item).html(obj.title);
                    $(".message", item).html(description);

                    $(".title", item).css("font-weight", "initial");
                    $(".message", item).css("font-weight", "initial");
                    if (obj.unread == 1) {
                        $(".title", item).css("font-weight", "bold");
                        $(".message", item).css("font-weight", "bold");
                    }

                    htmlList += item.html();

                }
                newsItem.html(htmlList);
            }

        },
        error: function (xhr) {
            // nothing
        }
    });
}

function handle_master_group_menu(master_group, child_group, name) {
    var action_xem = "Xem";
    var action_them = "Thêm";
    switch(master_group) {
        case "Ship":
            if(child_group == "Script edit"){
                if(name == action_xem){
                    $("#insert-ship-time").remove();
                }
            }
            break;
        case "Script":
            if(child_group == "Script head tag"){
                if(name == action_xem){
                    $("#insert-script-tag").remove();
                }
            }
            break;
        case "Giới thiệu":
            if(child_group == "Giá trị cốt lõi của OPERA"){
                if(name == action_xem){
                    $("#cong_ty_co_phan_page").remove();
                }
            }else if(child_group == "Tầm nhìn - Sứ Mệnh - Chính sách chất lượng"){
                if(name == action_xem){
                    $("#chinh_sach_chat_luong_page").remove();
                }
            }
            break;
        case "Quản lý footer":
            if(child_group == "Thông tin liên lạc công ty"){
                if(name == action_xem){
                    $("#thong_tin_lien_lac_cong_ty").remove();
                }
            }else if(child_group == "Quản lý danh mục cấp 2 footer"){
                if(name == action_xem){
                    $("#quan-ly-danh-muc-cap-2-footer").remove();
                }
            }else if(child_group == "Chính sách bán hàng"){
                if(name == action_xem){
                    $("#danh-sach-chinh-sach-ban-hang").remove();
                    $("#danh-sach-chinh-sach-ban-hang-shorcut").remove();
                }
            }else if(child_group == "Thông tin truyền thông"){
                if(name == action_xem){
                    $("#thong-tin-truyen-thong").remove();
                    // $("#danh-sach-chinh-sach-ban-hang-shorcut").remove();
                }
            }
            break;
        case "Quản lý trang chủ":
            if(child_group == "Thêm slide hình"){
                if(name == action_them){
                    $("#them-slider-home").remove();
                }
            }else if(child_group == "Danh sách slide hình"){
                if(name == action_xem){
                    $("#danh-sach-slider-home").remove();
                }
            }else if(child_group == "Quản lý meta trang chủ"){
                if(name == action_xem){
                    $("#quan-ly-meta-trang-chu").remove();
                }
            }else if(child_group == "Quản lý tốc độ slider trang chủ"){
                if(name == action_xem){
                    $("#quan-ly-toc-do-slider-trang-chu").remove();
                }
            }
            break;
        case "Quản lý chính sách bán hàng":
            if(child_group == "Thêm bài viết"){
                if(name == action_them){
                    $("#them-chinh-sach-ban-hang").remove();
                }
            }
            // else if(child_group == "Danh sách bài viết"){
            //     if(name == action_xem){
            //         $("#danh-sach-chinh-sach-ban-hang").remove();
            //     }
            // }
            break;
        case "Quản lý sản phẩm":
            if(child_group == "Thêm danh mục sản phẩm"){
                if(name == action_them){
                    $("#them-danh-muc-san-pham").remove();
                }
            }else if(child_group == "Danh sách danh mục sản phẩm"){
                if(name == action_xem){
                    $("#danh-sach-danh-muc-san-pham").remove();
                }
            }else if(child_group == "Thêm sản phẩm đơn vị"){
                if(name == action_them){
                    $("#them-san-pham-don-vi").remove();
                }
            }else if(child_group == "Danh sách sản phẩm đơn vị"){
                if(name == action_xem){
                    $("#danh-sach-san-pham-don-vi").remove();
                }
            }else if(child_group == "Thêm sản phẩm combo"){
                if(name == action_them){
                    $("#themSanPhamCombo").remove();
                }
            }else if(child_group == "Danh sách sản phẩm combo"){
                if(name == action_xem){
                    $("#danh-sach-san-pham-combo").remove();
                }
            }else if(child_group == "Cập nhật banner tổng quan"){
                if(name == action_xem){
                    $("#chinh-sua-banner-san-pham").remove();
                }
            }else if(child_group == "Quản lý sản phẩm quà tặng"){
                if(name == action_xem){
                    $("#quan-ly-san-pham-qua-tang").remove();
                }
            }else if(child_group == "Quản lý sản phẩm bán chạy"){
                if(name == action_xem){
                    $("#quan-ly-san-pham-ban-chay").remove();
                }
            }else if(child_group == "Quản lý meta tổng quan sản phẩm"){
                if(name == action_xem){
                    $("#quan-ly-meta-tong-quan-san-pham").remove();
                }
            }else if(child_group == "Thêm thương hiệu"){
                if(name == action_them){
                    $("#them-thuong-hieu").remove();
                }
            }else if(child_group == "Quản lý thương hiệu"){
                if(name == action_xem){
                    $("#danh-sach-thuong-hieu").remove();
                }
            }
            break;
        case "Quản lý khuyến mãi":
            if(child_group == "Thêm CT khuyến mãi"){
                if(name == action_them){
                    $("#them-chuong-trinh-khuyen-mai").remove();
                }
            }else if(child_group == "Danh sách CT khuyến mãi"){
                if(name == action_xem){
                    $("#danh-sach-chuong-trinh-khuyen-mai").remove();
                }
            }else if(child_group == "Tạo coupon"){
                if(name == action_them){
                    $("#tao-coupon").remove();
                }
            }else if(child_group == "Danh sách CT Coupon"){
                if(name == action_xem){
                    $("#danh-sach-chuong-trinh-coupon").remove();
                }
            }else if(child_group == "Danh sách tất cả Coupon"){
                if(name == action_xem){
                    $("#danh-sach-coupon").remove();
                }
            }else if(child_group == "Quản lý meta tổng quan khuyến mãi"){
                if(name == action_xem){
                    $("#quan-ly-meta-tong-quan-khuyen-mai").remove();
                }
            }else if(child_group == "Cập nhật banner tổng quan"){
                if(name == action_xem){
                    $("#chinh-sua-banner-khuyen-mai").remove();
                }
            }else if(child_group == "Quản lý sản phẩm khuyến mãi"){
                if(name == action_xem){
                    $("#quan-ly-san-pham-khuyen-mai").remove();
                }
            }else if(child_group == "Quản lý Popup khuyến mãi"){
                if(name == action_xem){
                    $("#quan-ly-popup-khuyen-mai").remove();
                }
            }
            break;
        case "Quản lý tin tức sự kiện":
            if(child_group == "Thêm bài viết"){
                if(name == action_them){
                    $("#them-tin-tuc-su-kien").remove();
                }
            }else if(child_group == "Danh sách bài viết"){
                if(name == action_xem){
                    $("#danh-sach-tin-tuc-su-kien").remove();
                }
            }else if(child_group == "Cập nhật banner tổng quan"){
                if(name == action_xem){
                    $("#chinh-sua-banner-tin-tuc-su-kien").remove();
                }
            }else if(child_group == "Quản lý meta tổng quan tin tức"){
                if(name == action_xem){
                    $("#quan-ly-meta-tin-tuc-su-kien").remove();
                }
            }else if(child_group == "Quản lý bài viết ở top"){
                if(name == action_xem){
                    $("#quan-ly-bai-viet-tin-tuc-su-kien").remove();
                }
            }else if(child_group == "Quản lý tốc độ slider ở top"){
                if(name == action_xem){
                    $("#quan-ly-toc-do-slider-tin-tuc").remove();
                }
            }
            break;
        case "Quản lý món ăn ngon":
            if(child_group == "Thêm bài viết"){
                if(name == action_them){
                    $("#them-mon-an-ngon").remove();
                }
            }else if(child_group == "Danh sách bài viết"){
                if(name == action_xem){
                    $("#danh-sach-mon-an-ngon").remove();
                }
            }else if(child_group == "Cập nhật banner tổng quan"){
                if(name == action_xem){
                    $("#chinh-sua-banner-mon-an-ngon").remove();
                }
            }else if(child_group == "Quản lý meta tổng quan món ngon"){
                if(name == action_xem){
                    $("#quan-ly-meta-tong-quan-mon-ngon").remove();
                }
            }else if(child_group == "Quản lý bài viết ở top"){
                if(name == action_xem){
                    $("#quan-ly-bai-viet-mon-an-ngon").remove();
                }
            }else if(child_group == "Quản lý tốc độ slider ở top"){
                if(name == action_xem){
                    $("#quan-ly-toc-do-slider-mon-an-ngon").remove();
                }
            }
            break;
        case "Quản lý liên hệ":
            if(child_group == "Chỉnh sửa liên hệ"){
                if(name == action_xem){
                    $("#chinh-sua-lien-he").remove();
                }
            }else if(child_group == "Danh sách liên hệ"){
                if(name == action_xem){
                    $("#danh-sach-lien-he").remove();
                }
            }else if(child_group == "Quản lý liên hệ footer"){
                if(name == action_xem){
                    $("#quan-ly-lien-he-footer").remove();
                }
            }
            break;
        case "Soạn thảo Email":
            if(child_group == "Cấu hình email"){
                if(name == action_xem){
                    $("#cau-hinh-email").remove();
                }
            }else if(child_group == "Email xác nhận đã đăng ký"){
                if(name == action_xem){
                    $("#soan-thao-email").remove();
                }
            }
            break;
        case "Quản lý chi nhánh (để sau)":
            if(child_group == "Thêm chi nhánh"){
                if(name == action_them){
                    $("#them-chi-nhanh-cua-hang").remove();
                }
            }else if(child_group == "Danh sách chi nhánh"){
                if(name == action_xem){
                    $("#danh-sach-chi-nhanh-cua-hang").remove();
                }
            }else if(child_group == "Cập nhật banner chi nhánh"){
                if(name == action_xem){
                    $("#cap-nhat-banner-chi-nhanh").remove();
                }
            }
            break;
        case "Quản lý khách hàng thân thiết":
            if(child_group == "Danh sách khách hàng thân thiết"){
                if(name == action_xem){
                    $("#danh-sach-khach-hang-than-thiet").remove();
                }
            }
            break;
        case "Quản lý kho":
            if(child_group == "Kho hàng"){
                if(name == action_xem){
                    $("#danh-sach-nhap-kho").remove();
                }
            }
            break;
        case "Quản lý đơn hàng":
            if(child_group == "Danh sách đơn hàng"){
                if(name == action_xem){
                    $("#danh-sach-don-hang").remove();
                }
            }
            break;
        case "Báo cáo":
            if(child_group == "Báo cáo bán hàng"){
                if(name == action_xem){
                    $("#bao-cao-ban-hang").remove();
                }
            }else if(child_group == "Thống kê bán hàng tổng hợp"){
                if(name == action_xem){
                    $("#thong-ke-ban-hang").remove();
                }
            }else if(child_group == "Báo cáo doanh số theo ngành hàng"){
                if(name == action_xem){
                    $("#bao-cao-doanh-so-theo-nganh-hang").remove();
                }
            }else if(child_group == "Thống kê bán hàng chi tiết"){
                if(name == action_xem){
                    $("#thong-ke-ban-hang-chi-tiet").remove();
                }
            }else if(child_group == "Báo cáo bán hàng đầy đủ"){
                if(name == action_xem){
                    $("#bao-cao-ban-hang-day-du").remove();
                }
            }
            break;
        case "Quản lý user":
            if(child_group == "Thêm user"){
                if(name == action_them){
                    $("#them-user-admin").remove();
                }
            }else if(child_group == "Danh sách user"){
                if(name == action_xem){
                    $("#danh-sach-user-admin").remove();
                }
            }else if(child_group == "Thêm quyền"){
                if(name == action_them){
                    $("#them-quyen").remove();
                }
            }else if(child_group == "Danh sách quyền"){
                if(name == action_xem){
                    $("#danh-sach-quyen").remove();
                }
            }
            break;
    }
}




function applyUserRole() {
    $.ajax({
        url: "utility/getUserRole",
        type: "post", //send it through get method
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                // chính sách bán hàng
                var array = response.data.user_role;
                var user_id = response.data.user_id;

                // ẩn menu quan ly user neu 0 phai user admin
                if(user_id == "719AD10A-04F9-4FD8-9351-1B2602277967"){
                    $("#left-menu").css("display", "");
                    return;
                }else {
                    //$("#user-page").remove();
                }

                countQuanLyUser = 0;
                countQuanLyQuyen = 0;
                var length = array.length;
                for (var index=0;index<array.length;index++){
                    var obj = array[index];
                    var master_group = obj.master_group;
                    var child_group = obj.child_group;
                    var name = obj.name;
                    handle_master_group_menu(master_group,child_group,name);
                }
                finish_left_menu();
                $("#left-menu").css("display", "");
            }

        },
        error: function (xhr) {
            // nothing
        }
    });
}

function finish_left_menu() {
    $(".nav-children").each(function () {
        if($(this).children("li").length == 0){
            $(this).closest("li").remove();
        }
    })
}

function applyUnreadNotification() {
    $.ajax({
        url: "utility/getUnreadNotification",
        type: "post", //send it through get method
        data: JSON.stringify({}),
        contentType: "application/json",
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                setBadge(response.data);
            }

        },
        error: function (xhr) {
            // nothing
        }
    });
}

function seenNotifications() {
    setBadge(0);
    $.ajax({
        url: "utility/seenNotifications",
        type: "post", //send it through get method
        data: JSON.stringify({}),
        contentType: "application/json",
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                // nothing
            }

        },
        error: function (xhr) {
            // nothing
        }
    });
    return true;
};

function handle_ui_left_menu() {

    $("#"+page_name).addClass('nav-active');
    $("#"+page_name).addClass('nav-expanded');
    $("#"+page_sub_name).addClass('nav-active');

}

applyUsernameFromSession();
applyUserNotification();
// applyUnreadNotification();
handle_ui_left_menu();
applyUserRole();