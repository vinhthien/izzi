$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    var name = $(".image-lib.has-border").attr("image-name");
    var url = $(".image-lib.has-border").attr("image-url");
    if (name && name.length > 0 && url && url.length > 0) {
        image_link = name;
        $('#ip-image-sample').attr("src", url);
        $('.image-lib').removeClass("has-border");
    }
});

function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link.length)
    {
        callback(image_link);
    }else {
        alert("Chưa chọn hình ảnh");
    }
}

function setDataCkeditor() {
    if(is_new) return;
    var vi = $( "#vi-label" ).attr( "data" );
    CKEDITOR.instances.editor1.setData(vi);
    var en = $( "#en-label" ).attr( "data" );
    CKEDITOR.instances.editor2.setData(en);
}

function update() {
    var title_vi = $("#ip-title-vi").val();
    var html_vi = CKEDITOR.instances.editor1.getData();
    var title_en = $("#ip-title-en").val();
    var html_en = CKEDITOR.instances.editor2.getData();

    if (title_vi.length == 0) {
        alert("Chưa thêm tiêu đề tiếng việt");
        return;
    }
    if (html_vi.length == 0) {
        alert("Chưa thêm HTML tiếng việt");
        return;
    }

    if (title_en.length == 0) {
        title_en = title_vi;
    }
    if (html_en.length == 0) {
        html_en = html_vi;
    }
    uploadFile(function (url) {

        $.ajax({
            url: "chinh-sua-lien-he/sua",
            type: "post", //send it through get method
            data: JSON.stringify({
                title_vi: title_vi,
                html_vi: html_vi,
                title_en: title_en,
                html_en: html_en,
                image: url,
                id: global_id
            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Chỉnh sửa liên hệ thành công',
                        type: 'success'
                    });
                    $("#ip-image-sample").attr("src", '../upload/'+url);

                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Chỉnh sửa liên hệ thất bại\n' + response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Chỉnh sửa liên hệ thất bại',
                    type: 'error'
                });
            }
        });
    });
}

setDataCkeditor();

