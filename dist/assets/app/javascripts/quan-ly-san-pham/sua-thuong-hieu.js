










function update() {


    var name = $("#ip-name").val();
    if(name.length == 0){
        alert("Tên thương hiệu còn trống");
        return;
    }
    $.ajax({
        url: "/admin/sua-thuong-hieu/sua",
        type: "post", //send it through get method
        data: JSON.stringify({
            title: name,
            brand_id: brand_id
        }),    contentType: "application/json",
        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thương hiệu '+ name +" thành công",
                    type: 'success'
                });
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thương hiệu '+ name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }
        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Sửa thương hiệu '+ name +" thất bại",
                type: 'error'
            });
        }
    });

}