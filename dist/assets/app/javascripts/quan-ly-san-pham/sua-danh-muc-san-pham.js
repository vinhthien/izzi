function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh/upload', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link.length) {
        callback(image_link);
    } else {
        alert("hình ảnh còn trống");
        // callback(null);
    }
}

function uploadBanner(callback)
{
    if($("#file_to_upload1").val() != "")
    {
        var file_data = $('#file_to_upload1').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh/upload', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload1").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(banner_image.length) {
        callback(banner_image);
    } else {
        alert("banner còn trống");
        // callback(null);
    }
}




function setDataCkeditor() {
    var vi = $( "#vi-label" ).attr( "data" );
    CKEDITOR.instances.editor1.setData(vi);
    var en = $( "#en-label" ).attr( "data" );
    CKEDITOR.instances.editor2.setData(en);
}

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function format_money() {
    var number = $("#ip-price").val();
    number = parse_number(number);
    $("#ip-price").val(number);
}
function update() {
    var e1 = document.getElementById("parent_id");
    var parent_id = e1.options[e1.selectedIndex].value;
    var order = $("#ip-order").val();
    var des_vi = $("#ip-des-vi").val();
    var name_vi = $("#ip-name-vi").val();
    var title_vi = $("#ip-title-vi").val();
    var html_vi = CKEDITOR.instances.editor1.getData();
    var des_en = $("#ip-des-en").val();
    var name_en = $("#ip-name-en").val();
    var title_en = $("#ip-title-en").val();
    var link=$("#ip-link-banner").val();
    var html_en = CKEDITOR.instances.editor2.getData();


    // if(document.getElementById("file_to_upload").files.length == 0 ){
    //     alert("hình ảnh còn trống");
    //     return;
    // }

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp");
        return;
    }

    // if (des_vi.length == 0) {
    //     alert("Chưa thêm mô tả sản phẩm tiếng việt");
    //     return;
    // }
    if (name_vi.length == 0) {
        alert("Chưa thêm tên sản phẩm tiếng việt");
        return;
    }
    // if (title_vi.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng việt");
    //     return;
    // }
    // if (html_vi.length == 0) {
    //     alert("Chưa thêm HTML sản phẩm tiếng việt");
    //     return;
    // }
    // if (des_en.length == 0) {
    //     alert("Chưa thêm mô tả sản phẩm tiếng anh");
    //     return;
    // }
    if (name_en.length == 0) {
        name_en = name_vi;
    }
    // if (title_en.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng anh");
    //     return;
    // }
    // if (html_en.length == 0) {
    //     alert("Chưa thêm HTML sản phẩm tiếng anh");
    //     return;
    // }

    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();




    if (meta_keyword_vi.length == 0) {
        alert("Chưa thêm meta key tiếng việt");
        return;
    }
    if (meta_description_vi.length == 0) {
        alert("Chưa thêm meta description tiếng việt");
        return;
    }
    if (meta_title_vi.length == 0) {
        alert("Chưa thêm meta title tiếng việt");
        return;
    }

    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
    }


    uploadFile(function (url) {
        uploadBanner(function (banner) {
            $.ajax({
                url: "sua-danh-muc-san-pham/sua",
                type: "post", //send it through get method
                data: JSON.stringify({
                    meta_title_vi: meta_title_vi,
                    meta_description_vi: meta_description_vi,
                    meta_keyword_vi: meta_keyword_vi,
                    meta_description_en: meta_description_en,
                    meta_keyword_en: meta_keyword_en,
                    meta_title_en: meta_title_en,
                    order: order,
                    des_vi: des_vi,
                    name_vi: name_vi,
                    title_vi: title_vi,
                    html_vi: html_vi,
                    des_en: des_en,
                    name_en: name_en,
                    title_en: title_en,
                    html_en: html_en,
                    parent_id: parent_id,
                    banner: banner,
                    image: url,
                    link:link,
                    category_id: category_id
                }), contentType: "application/json",

                success: function (response) {
                    if (response.response_code == "SUCC_EXEC") {
                        new PNotify({
                            title: 'Thông báo',
                            text: 'Sửa danh mục [' + name_vi + "] thành công",
                            type: 'success'
                        });

                        $("#ip-image-sample").attr("src", '../upload/' + url);
                        $("#ip-image-sample1").attr("src", '../upload/' + banner);


                    } else {
                        new PNotify({
                            title: 'Thông báo',
                            text: 'Sửa danh mục [' + name_vi + "] thất bại\n" + response.description,
                            type: 'error'
                        });
                    }

                },
                error: function (xhr) {
                    //Do Something to handle error
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Sửa danh mục [' + name_vi + "] thất bại",
                        type: 'error'
                    });
                }
            });
        });
    })
}
setDataCkeditor();