
var image_link_array = [];
var image_link = null;
var sub_image_link = null;
var is_sub_image = false;

function show_image_lib(is_sub) {
    is_sub_image = is_sub;
    show_image_lib_ori();
}

$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    var name = $(".image-lib.has-border").attr("image-name");
    var url = $(".image-lib.has-border").attr("image-url");
    if(name && name.length > 0 && url && url.length > 0){
        if(!is_sub_image) {
            image_link = name;
            $('#preview-img').attr("src", url);
            $('#preview-img').css("max-width", "300px");
            $('#preview-img').css("max-height", "150px");
            $('#preview-img').removeClass("hide");
        }else {
            sub_image_link = name;
            them_hinh();
            sub_image_link = null;

        }
        $('.image-lib').removeClass("has-border");

    }
});
function them_hinh() {
    upload_lib_hinh(function (url) {
        image_link_array.push({url:url,index:image_link_array.length});
        new PNotify({
            title: 'Thông báo',
            text: 'Thêm hình ảnh sản phẩm thành công',
            type: 'success'
        });

        var temp_id = "hinhanh"+url+image_link_array.length;
        var pos = image_link_array.length;
        var text = 'onclick=\'xoa_hinh_anh("'+url+'","'+pos+'")\'';

        $('#tb-hinh-anh').append('<tr id=\''+temp_id+'\'>' +
            '<td><img class="image-sample" src="/upload/'+url+'"></td>' +
            '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');
    })
}

function sync_hinh_anh(product_id) {
    for(var index=0;index<image_link_array.length;index++){
        var obj = image_link_array[index];
        var url = obj.url;
        $.ajax({
            url: "sua-san-pham/upload-them-hinh",
            type: "post", //send it through get method
            data: JSON.stringify({
                url: url,
                product_id: product_id
            }),contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm hình ảnh sản phẩm thành công',
                        type: 'success'
                    });

                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm liên kết sản phẩm '+ url +" thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết sản phẩm ['+ url +"] thất bại",
                    type: 'error'
                });
            }
        });
    }
}

function upload_lib_hinh(callback) {
    if($("#file_to_upload3").val() != "")
    {
        var file_data = $('#file_to_upload3').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload3").val("");
                if(data.response_code == "SUCC_EXEC"){
                    var url = data.data;
                    callback(url);
                    return false;
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                    return false;
                }
            }
        });
    }
    else if(sub_image_link != null)
    {
        callback(sub_image_link);
        return false;
    }else {
        alert("Chưa chọn hình ảnh");
        return false;
    }
}

$('#parent_id').change(function(){
    var product_parent_id = $(this).val();
    if(product_parent_id == -1) return;
    $.ajax({
        url: "/admin/them-san-pham-combo/get_product_data",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_parent_id: product_parent_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                var sanphamObject = response.data.sanpham;
                var sanphamVIObject = response.data.sanphamVI;
                var sanphamENObject = response.data.sanphamEN;
                apply_data_UI(sanphamObject,sanphamVIObject,sanphamENObject);
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Lấy sản phẩm thất bại\n'+response.description,
                    type: 'error'
                });
            }
        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Lấy thông tin sản phẩm thất bại',
                type: 'error'
            });
        }
    });
});


$("#ip-sl").bind('change keyup', function () {
    var e1 = document.getElementById("parent_id");
    var parent_id = e1.options[e1.selectedIndex].value;
    var number = $(this).val();
    if(parent_id == -1 && number == "" || number == 0 || number < 0) return;
    $.ajax({
        url: "/admin/them-san-pham-combo/get_product_data",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_parent_id: parent_id
        }),    contentType: "application/json",
        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                var sanphamObject = response.data.sanpham;
                var quantity = sanphamObject.quantity;
                var price = sanphamObject.price;
                var cal = price * number;
                $('#ip-price').val(cal);
                format_money_and_edit();
            }
        },
        error: function (xhr) {
        }
    });
});



function apply_data_UI(sanphamObject,sanphamVIObject,sanphamENObject) {
    $("#product_category_id > option").each(function() {
        if(this.value == sanphamObject.product_category_id){
            $("#product_category_id").val(this.value).change();
        }
    });
    $('#ip-ma-sp').val(sanphamObject.code);
    $('#ip-price').val(sanphamObject.price);
    format_money_and_edit();
    $('#ip-order').val(sanphamObject.sort_order);
    $('#ip-weight').val(sanphamObject.weight);
    $('#ip-unit').val(sanphamObject.unit);
    $('#ip-sl').val(sanphamObject.quantity);
    removeTagInput()
    $('#ip-tag').val(sanphamObject.tags);
    addTagInput();

    var description_vi = sanphamVIObject.description;
    var name_vi = sanphamVIObject.name;
    var title_vi = sanphamVIObject.title;
    var contentHTML_vi = sanphamVIObject.contentHTML;
    var meta_description_vi = sanphamVIObject.meta_description;
    var meta_keywords_vi = sanphamVIObject.meta_keywords;
    var meta_title_vi = sanphamVIObject.meta_title;

    var description_en = sanphamENObject.description;
    var name_en = sanphamENObject.name;
    var title_en = sanphamENObject.title;
    var contentHTML_en = sanphamENObject.contentHTML;
    var meta_description_en = sanphamENObject.meta_description;
    var meta_keywords_en = sanphamENObject.meta_keywords;
    var meta_title_en = sanphamENObject.meta_title;

    $('#ip-name-vi').val(name_vi);
    $('#ip-name-en').val(name_en);
    $('#ip-des-vi').val(description_vi);
    $('#ip-des-en').val(description_en);
    $('#meta-keyword-vi').val(meta_keywords_vi);
    $('#meta-keyword-en').val(meta_keywords_en);
    $('#meta-description-vi').val(meta_description_vi);
    $('#meta-description-en').val(meta_description_en);
    $('#meta-title-vi').val(meta_title_vi);
    $('#meta-title-en').val(meta_title_en);
    CKEDITOR.instances.editor1.setData(contentHTML_vi);
    CKEDITOR.instances.editor2.setData(contentHTML_en);

}
function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link != null)
    {
        callback(image_link);
    }else {
        alert("Vui lòng chọn file");
    }
}


function addTagInput() {
    tagsInput(document.querySelector('#ip-tag'));
    $(".tags-input").css("display","inherit");
    $(".tags-input").css("width","auto");
    var array = document.getElementsByClassName("tags-input");
    if(array) {
        for (var index = 0; index < array.length; index++) {
            if(index>0){
                var element = array[index];
                element.parentNode.removeChild(element);
            }
        }
    }
}

function removeTagInput() {
    $("#ip-tag").attr("type","text");
}

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function format_money_and_edit() {
    var number = $("#ip-price").val();
    number = revert_money_to_normal(number);
    if(number == "") return;
    number = format_money(number);
    $("#ip-price").val(number);
}
function add() {
    var important = document.getElementById("ip-important").checked;
    var e = document.getElementById("product_category_id");
    var product_category_id = e.options[e.selectedIndex].value;
    var e1 = document.getElementById("parent_id");
    var parent_id = e1.options[e1.selectedIndex].value;

    var e2 = document.getElementById("ip-type");
    var type = e2.options[e2.selectedIndex].value;


    var code = $("#ip-ma-sp").val();
    var quantity = $("#ip-sl").val();
    var price = $("#ip-price").val();

    var weight = $("#ip-weight").val();
    var unit = $("#ip-unit").val();
    var tag = $("#ip-tag").val();
    var order = $("#ip-order").val();
    var des_vi = $("#ip-des-vi").val();
    var name_vi = $("#ip-name-vi").val();
    var title_vi = $("#ip-title-vi").val();
    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var html_vi = CKEDITOR.instances.editor1.getData();


    var des_en = $("#ip-des-en").val();
    var name_en = $("#ip-name-en").val();
    var title_en = $("#ip-title-en").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();
    var html_en = CKEDITOR.instances.editor2.getData();
    if( document.getElementById("file_to_upload").files.length == 0 && image_link == null){
        alert("hình ảnh còn trống");
        return;
    }
    if(code.length == 0){
        alert("Chưa thêm mã sản phẩm");
        return;
    }
    if (quantity.length == 0) {
        alert("Chưa thêm số lượng sản phẩm");
        return;
    }
    if (price.length == 0) {
        alert("Chưa thêm giá sản phẩm");
        return;
    }
    if (weight.length == 0) {
        alert("Chưa thêm trọng lượng sản phẩm");
        return;
    }
    if (unit.length == 0) {
        alert("Chưa thêm đơn vị tính của sản phẩm");
        return;
    }
    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return;
    }
    if (tag.length == 0) {
        alert("Chưa thêm nhãn sản phẩm");
        return;
    }
    if (des_vi.length == 0) {
        alert("Chưa thêm mô tả sản phẩm tiếng việt");
        return;
    }
    if (name_vi.length == 0) {
        alert("Chưa thêm tên sản phẩm tiếng việt");
        return;
    }
    if (meta_keyword_vi.length == 0) {
        alert("Chưa thêm meta key tiếng việt");
        return;
    }
    if (meta_description_vi.length == 0) {
        alert("Chưa thêm meta description tiếng việt");
        return;
    }
    if (meta_title_vi.length == 0) {
        alert("Chưa thêm meta title tiếng việt");
        return;
    }
    // if (title_vi.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng việt");
    //     return;
    // }
    if (html_vi.length == 0) {
        alert("Chưa thêm HTML sản phẩm tiếng việt");
        return;
    }
    if (des_en.length == 0) {
        des_en = des_vi;
    }
    if (name_en.length == 0) {
        name_en = name_vi;
    }
    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
    }
    // if (title_en.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng anh");
    //     return;
    // }
    if (html_en.length == 0) {
        html_en = html_vi;
    }
    if (parent_id == -1){
        alert("Chưa chọn sản phẩm đơn vị");
        return;
    }
    price = revert_money_to_normal(price);

    uploadFile(function (url) {
        $.ajax({
            url: "/admin/them-san-pham-combo/them",
            type: "post", //send it through get method
            data: JSON.stringify({
                product_category_id: product_category_id,
                code: code,
                quantity: quantity,
                price: price,
                weight: weight,
                unit: unit,
                tag: tag,
                order: order,
                des_vi: des_vi,
                name_vi: name_vi,
                // title_vi: title_vi,
                html_vi: html_vi,
                parent_id: parent_id,
                des_en: des_en,
                name_en: name_en,
                // title_en: title_en,
                html_en: html_en,
                image: url,
                important: important,
                type: type,
                meta_title_vi:meta_title_vi,
                meta_description_vi:meta_description_vi,
                meta_keyword_vi:meta_keyword_vi,
                meta_description_en:meta_description_en,
                meta_keyword_en:meta_keyword_en,
                meta_title_en:meta_title_en
            }),    contentType: "application/json",

            success: function (response) {

                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm sản phẩm ['+ name_vi +"] thành công",
                        type: 'success'
                    });
                    var product_id = response.data.id;
                    sync_hinh_anh(product_id);
                    image_link = null;
                    window.location.href = "sua-san-pham-combo?id="+response.data.id;
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm sản phẩm ['+ name_vi +"] thất bại\n"+response.description,
                        type: 'error'
                    });
                }
            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm sản phẩm ['+ name_vi +"] thất bại",
                    type: 'error'
                });
            }
        });
    })


}
function disable_scroll() {
    var input = document.getElementById("ip-sl");
    input.addEventListener("mousewheel", function(event){ this.blur() });
}
addTagInput();
disable_scroll();