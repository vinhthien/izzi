var product_id_global = null;
var link_array = [];
var not_sync = false;
var image_link_array = [];
var image_link = null;
var sub_image_link = null;
var is_sub_image = false;


/*
Modal Confirm
*/
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    var name = $(".image-lib.has-border").attr("image-name");
    var url = $(".image-lib.has-border").attr("image-url");
    if(name && name.length > 0 && url && url.length > 0){
        if(!is_sub_image) {
            $("#file_to_upload").val("");
            image_link = name;
            $('#preview-img').attr("src", url);
            $('#preview-img').removeClass("hide");
        }else {
            $("#file_to_upload3").val("");
            sub_image_link = name;
            them_hinh();
            sub_image_link = null;
        }
        $('.image-lib').removeClass("has-border");
    }
});
function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    image_link = data.data;
                    callback(image_link);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link != null)
    {
        callback(image_link);

    }else {
        alert("Vui lòng chọn file");
    }
}

function show_image_lib(is_sub) {
    is_sub_image = is_sub;
    show_image_lib_ori();
}



function addTagInput() {
    tagsInput(document.querySelector('input[type="tags"]'));
    $(".tags-input").css("display","inherit");
    $(".tags-input").css("width","auto");

}

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function format_money_and_edit() {
    var number = $("#ip-price").val();
    number = revert_money_to_normal(number);
    if(number == "") return;
    number = format_money(number);
    $("#ip-price").val(number);
}
function add() {
    var important = document.getElementById("ip-important").checked;
    var e = document.getElementById("product_category_id");
    var product_category_id = e.options[e.selectedIndex].value;


    var e2 = document.getElementById("ip-type");
    var type = e2.options[e2.selectedIndex].value;

    var e11 = document.getElementById("brand-select");
    var brand = e11.options[e11.selectedIndex].value;

    var code = $("#ip-ma-sp").val();
    var price = $("#ip-price").val();

    var weight = $("#ip-weight").val();
    var unit = $("#ip-unit").val();
    var tag = $("#ip-tag").val();
    var order = $("#ip-order").val();
    var des_vi = $("#ip-des-vi").val();
    var name_vi = $("#ip-name-vi").val();
    var title_vi = $("#ip-title-vi").val();
    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var html_vi = CKEDITOR.instances.editor1.getData();


    var des_en = $("#ip-des-en").val();
    var name_en = $("#ip-name-en").val();
    var title_en = $("#ip-title-en").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();
    var html_en = CKEDITOR.instances.editor2.getData();
    if( document.getElementById("file_to_upload").files.length == 0 && image_link == null ){
        alert("hình ảnh còn trống");
        return;
    }
    if(code.length == 0){
        alert("Chưa thêm mã sản phẩm");
        return;
    }
    if (price.length == 0) {
        alert("Chưa thêm giá sản phẩm");
        return;
    }
    if (weight.length == 0) {
        alert("Chưa thêm trọng lượng sản phẩm");
        return;
    }
    if (unit.length == 0) {
        alert("Chưa thêm đơn vị tính của sản phẩm");
        return;
    }
    if (brand == -1) {
        alert("Chưa chọn thương hiệu của sản phẩm");
        return;
    }
    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return;
    }
    if (tag.length == 0) {
        alert("Chưa thêm nhãn sản phẩm");
        return;
    }
    if (des_vi.length == 0) {
        alert("Chưa thêm mô tả sản phẩm tiếng việt");
        return;
    }
    if (name_vi.length == 0) {
        alert("Chưa thêm tên sản phẩm tiếng việt");
        return;
    }
    if (meta_keyword_vi.length == 0) {
        alert("Chưa thêm meta key tiếng việt");
        return;
    }
    if (meta_description_vi.length == 0) {
        alert("Chưa thêm meta description tiếng việt");
        return;
    }
    if (meta_title_vi.length == 0) {
        alert("Chưa thêm meta title tiếng việt");
        return;
    }
    // if (title_vi.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng việt");
    //     return;
    // }
    if (html_vi.length == 0) {
        alert("Chưa thêm HTML sản phẩm tiếng việt");
        return;
    }
    if (des_en.length == 0) {
        des_en = des_vi;
    }
    if (name_en.length == 0) {
        name_en = name_vi;
    }
    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
    }
    // if (title_en.length == 0) {
    //     alert("Chưa thêm tiêu đề sản phẩm tiếng anh");
    //     return;
    // }
    if (html_en.length == 0) {
        html_en = html_vi;
    }
    price = revert_money_to_normal(price);

    uploadFile(function (url) {
        $.ajax({
            url: "/admin/them-san-pham-don-vi/them",
            type: "post", //send it through get method
            data: JSON.stringify({
                product_category_id: product_category_id,
                code: code,
                price: price,
                weight: weight,
                unit: unit,
                tag: tag,
                order: order,
                des_vi: des_vi,
                name_vi: name_vi,
                // title_vi: title_vi,
                html_vi: html_vi,
                des_en: des_en,
                brand: brand,
                name_en: name_en,
                // title_en: title_en,
                html_en: html_en,
                image: url,
                important: important,
                type: type,
                meta_title_vi:meta_title_vi,
                meta_description_vi:meta_description_vi,
                meta_keyword_vi:meta_keyword_vi,
                meta_description_en:meta_description_en,
                meta_keyword_en:meta_keyword_en,
                meta_title_en:meta_title_en
            }),    contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm sản phẩm ['+ name_vi +"] thành công",
                        type: 'success'
                    });
                    product_id_global = response.data.id;
                    sync_mon_ngon();
                    sync_hinh_anh(product_id_global);
                    image_link = null;
                    window.location.href = "sua-san-pham-don-vi?id="+response.data.id;
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm sản phẩm ['+ name_vi +"] thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm sản phẩm ['+ name_vi +"] thất bại",
                    type: 'error'
                });
            }
        });
    })


}

function xoa_hinh_anh(url,pos) {

    new PNotify({
        title: 'Thông báo',
        text: 'Xóa hình ảnh sản phẩm thành công',
        type: 'success'
    });
    image_link_array.splice(pos-1,1);
    var temp = 'hinhanh'+url+pos;
    var row = document.getElementById(temp);
    row.parentNode.removeChild(row);
}

function sync_hinh_anh(product_id) {
    for(var index=0;index<image_link_array.length;index++){
        var obj = image_link_array[index];
        var url = obj.url;
        $.ajax({
            url: "sua-san-pham/upload-them-hinh",
            type: "post", //send it through get method
            data: JSON.stringify({
                url: url,
                product_id: product_id
            }),contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm hình ảnh sản phẩm thành công',
                        type: 'success'
                    });

                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm liên kết sản phẩm '+ url +" thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết sản phẩm ['+ url +"] thất bại",
                    type: 'error'
                });
            }
        });
    }
}

function them_hinh() {
    upload_lib_hinh(function (url) {
        sub_image_link = null;
        image_link_array.push({url:url,index:image_link_array.length});
        new PNotify({
            title: 'Thông báo',
            text: 'Thêm hình ảnh sản phẩm thành công',
            type: 'success'
        });

        var temp_id = "hinhanh"+url+image_link_array.length;
        var pos = image_link_array.length;
        var text = 'onclick=\'xoa_hinh_anh("'+url+'","'+pos+'")\'';

        $('#tb-hinh-anh').append('<tr id=\''+temp_id+'\'>' +
            '<td><img class="image-sample" src="'+address+"/upload/"+url+'"></td>' +
            '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');
        return;


        $.ajax({
            url: "sua-san-pham/upload-them-hinh",
            type: "post", //send it through get method
            data: JSON.stringify({
                url: url,
                product_id: product_id
            }),contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm hình ảnh sản phẩm thành công',
                        type: 'success'
                    });
                    var link_id = response.data.id;
                    var address = response.data.address;
                    var temp_id = "hinhanh"+link_id;
                    var text = 'onclick=\'xoa_hinh_anh("'+link_id+'")\'';

                    $('#tb-hinh-anh').append('<tr id=\''+temp_id+'\'>' +
                        '<td><img class="image-sample" src="'+address+url+'"></td>' +
                        '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');

                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm liên kết sản phẩm '+ content_name +" thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết sản phẩm ['+ content_name +"] thất bại",
                    type: 'error'
                });
            }
        });
    })


}

function upload_lib_hinh(callback) {
    if($("#file_to_upload3").val() != "")
    {
        var file_data = $('#file_to_upload3').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload3").val("");
                if(data.response_code == "SUCC_EXEC"){
                    sub_image_link = data.data;
                    callback(sub_image_link);
                    return false;
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                    return false;
                }
            }
        });
    }
    else if(sub_image_link != null)
    {
        callback(sub_image_link);
        return false;
    }else {
        alert("Chưa chọn hình ảnh");
        return false;
    }
}

function xoa_lien_ket(name,content_id) {
    if(link_array.length > 0) {
        var pos = null;
        for(var alpha=0;alpha<link_array.length;alpha++){
            if(link_array[alpha].content_id == content_id){
                pos = alpha;
                break;
            }
        }
        link_array.splice(pos,1);
        new PNotify({
            title: 'Thông báo',
            text: 'Xóa liên kết sản phẩm ' + name + " thành công",
            type: 'success'
        });
        var temp = 'monngon' + content_id;
        var row = document.getElementById(temp);
        row.parentNode.removeChild(row);
        $("#mon_ngon_select").append("<option value='" + content_id + "' selected>" + name + "</option>");
        $('#mon_ngon_select').trigger('change');
    }
}

function sync_mon_ngon() {
    for(var index=0;index<link_array.length;index++){
        var content_name = link_array[index].content_name;
        var content_id = link_array[index].content_id;
        $.ajax({
            url: "them-san-pham-don-vi/them-lien-ket",
            type: "post", //send it through get method
            data: JSON.stringify({
                content_id: content_id,
                product_id: product_id_global
            }),    contentType: "application/json",

            success: function (response) {
                // nothing

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết sản phẩm ['+ content_name +"] thất bại",
                    type: 'error'
                });
            }
        });
    }

}
function them_lien_ket() {
    var content_name = $("#mon_ngon_select option:selected").text();
    var content_id = $("#mon_ngon_select option:selected").val();
    if(!content_id || content_id.length == 0 || content_name.length == 0){
        alert("chưa chọn món ngon")
        return;
    }
    link_array.push({content_id: content_id,content_name: content_name});
    new PNotify({
        title: 'Thông báo',
        text: 'Thêm liên kết sản phẩm '+ content_name +" thành công",
        type: 'success'
    });
    var temp_id = "monngon"+content_id;
    $("#mon_ngon_select option:selected").empty();
    $('#mon_ngon_select').select2('val','');
    $("#mon_ngon_select option[value='" + content_id + "']").remove();
    $('#mon_ngon_select').trigger('change');
    var text = 'onclick=\'xoa_lien_ket("'+content_name+'","'+content_id+'")\'';
    $('#tb-mon-ngon').append('<tr id=\''+temp_id+'\'>' +
        '<td>'+content_name+'</td>' +
        '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');


}
addTagInput();