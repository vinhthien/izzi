

function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function format_money() {
    var number = $("#ip-price").val();
    number = parse_number(number);
    $("#ip-price").val(number);
}
function update() {

    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();




    if (meta_keyword_vi.length == 0) {
        alert("Chưa thêm meta key tiếng việt");
        return;
    }
    if (meta_description_vi.length == 0) {
        alert("Chưa thêm meta description tiếng việt");
        return;
    }
    if (meta_title_vi.length == 0) {
        alert("Chưa thêm meta title tiếng việt");
        return;
    }

    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
        $("#meta-keyword-en").val(meta_keyword_vi);
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
        $("#meta-description-en").val(meta_description_vi);
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
        $("#meta-title-en").val(meta_title_vi);
    }

    $.ajax({
        url: "quan-ly-meta-trang-chu/capnhat",
        type: "post", //send it through get method
        data: JSON.stringify({
            meta_title_vi:meta_title_vi,
            meta_description_vi:meta_description_vi,
            meta_keyword_vi:meta_keyword_vi,
            meta_description_en:meta_description_en,
            meta_keyword_en:meta_keyword_en,
            meta_title_en:meta_title_en,
            require_id: require_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Cập nhật meta thành công',
                    type: 'success'
                });
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Cập nhật meta thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Cập nhật meta thất bại',
                type: 'error'
            });
        }
    });


}