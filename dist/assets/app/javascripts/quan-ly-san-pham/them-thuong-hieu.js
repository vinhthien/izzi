function add() {
    var name = $("#ip-name").val();
    if(name.length == 0){
        alert("Tên thương hiệu còn trống");
        return;
    }
    $.ajax({
        url: "/admin/them-thuong-hieu/them",
        type: "post", //send it through get method
        data: JSON.stringify({
            name: name
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm thương hiệu ['+ name +"] thành công",
                    type: 'success'
                });
                window.location = "sua-thuong-hieu?id="+response.data;

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm thương hiệu ['+ name +"] thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm thương hiệu ['+ name +"] thất bại",
                type: 'error'
            });
        }
    });


}