/*
Name: 			Tables / Editable - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/
var current_product_id = null;
var current_product_code = null;
var current_product_name = null;
var is_add_action = false;
var can_add_new = true;
function them_san_pham() {

    var e = document.getElementById("product_select");
    current_product_id = e.options[e.selectedIndex].value;
    if (current_product_id == -1) {
        alert("Chưa chọn sản phẩm để thêm");
        return;
    }
    current_product_parent_id = e.options[e.selectedIndex].getAttribute("data-parent-id");
    current_product_code = e.options[e.selectedIndex].getAttribute("data-code");
    current_product_name = e.options[e.selectedIndex].text;
    check_exist(current_product_id,function () {
        add_database(current_product_id,current_product_name, function (success, action) {
            var goSanPham = "";
            if (current_product_parent_id == null) {
                goSanPham = "goSanPhamDonVi";
            } else {
                goSanPham = "goSanPhamCombo";
            }
            add_row(goSanPham,current_product_id,current_product_code,current_product_name);
        });
    })

}

function add_row(goSanPham,current_product_id,current_product_code,current_product_name) {
    var text =
        '<button class="btn btn-default"  onclick="'+goSanPham+'(\'' + current_product_id + '\')">'+
        '<i class="fa fa-pencil"></i>' +
        '</button>' +
        '<button class="btn btn-default" id="row' + current_product_id + '" product-code="'+current_product_code+'" product-id="'+current_product_id+'" product-name="'+current_product_name+'" onclick="deleted(this)">'+
        '<i class="fa fa-trash-o"></i>' +
        '</button>';

    var table = $('#datatable-tabletools').DataTable();

    var rowNode = table
        .row.add( [ current_product_code, current_product_name, 'Edinburgh' ] )
        .draw()
        .node();
    $(rowNode).find("td:last-child").html(text);
    $(rowNode).attr("id","product-"+current_product_id);
}

function check_exist(product_id,callback) {
    $.ajax({
        url: "/admin/quan-ly-san-pham-qua-tang/count",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id
        }), contentType: "application/json",

        success: function (response) {

            if (response.response_code == "SUCC_EXEC") {
                var number = response.data;
                if(number > 0){
                    new PNotify({
                        title: 'Thông báo',
                        text: ' sản phẩm này đã tồn tại trong danh sách' ,
                        type: 'error'
                    });
                }else {
                    callback();
                }
                return true;
            }

        },
        error: function (xhr) {

        }
    });
}

function add_database(product_id,product_name, callback) {
    $.ajax({
        url: "/admin/quan-ly-san-pham-qua-tang/them",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id
        }), contentType: "application/json",

        success: function (response) {
            var action = "";
            if(response.data == "insert"){
                action = "Thêm";
            }else {
                action = "Sửa";
            }
            if (response.response_code == "SUCC_EXEC") {
                new PNotify({
                    title: 'Thông báo',
                    text: action+' sản phẩm [' + product_name + "] thành công",
                    type: 'success'
                });
                callback(true, response.data);
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: action+' sản phẩm [' + product_name + "] thất bại\n" + response.description,
                    type: 'error'
                });
                callback(false);
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm(Sửa) sản phẩm [' + product_name + "] thất bại",
                type: 'error'
            });
        }
    });
}

function remove_product(product_id, callback) {
    $.ajax({
        url: "/admin/quan-ly-san-pham-qua-tang/xoa",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id
        }), contentType: "application/json",

        success: function (response) {

            if (response.response_code == "SUCC_EXEC") {
                new PNotify({
                    title: 'Thông báo',
                    text: "Xóa sản phẩm thành công",
                    type: 'success'
                });
                callback();
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: "Xóa sản phẩm thất bại\n" + response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Xóa sản phẩm thất bại',
                type: 'error'
            });
        }
    });
}


