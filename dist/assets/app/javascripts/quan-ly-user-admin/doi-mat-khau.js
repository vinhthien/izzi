










function update() {

    var username = $("#ip-username").val();
    var user_id=$("#ip-username").attr('name');
    if(username.length == 0){
        alert("Tên người dùng còn trống");
        return;
    }
    var password = $("#ip-password").val();
    if(password.length == 0){
        alert("Mật khẩu còn trống");
        return;
    }

    var retype = $("#ip-retype").val();
    if(retype.length == 0){
        alert("Chưa nhập lại mật khẩu");
        return;
    }


    if(retype != password){
        alert("Mật khẩu nhập lại chưa trùng khớp");
        return;
    }


    $.ajax({
        url: "/admin/doi-mat-khau/sua",
        type: "post", //send it through get method
        data: JSON.stringify({
            username: username,
            password: password,
            user_id:user_id,
            ori_password: ori_password
        }),
        contentType: "application/json",
        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thông tin người dùng '+ username +" thành công",
                    type: 'success'
                });
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa thông tin người dùng '+ username +" thất bại\n"+response.description,
                    type: 'error'
                });
            }
        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Sửa thông tin người dùng '+ username +" thất bại",
                type: 'error'
            });
        }
    });

}