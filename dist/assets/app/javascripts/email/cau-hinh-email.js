function add() {

    var email = $("#ip-email").val();

    if (email.length == 0) {
        alert("Chưa thêm email");
        return;
    }

    var pw = $("#ip-pw").val();

    var host = $("#ip-host").val();
    var port = $("#ip-port").val();

    if (host.length == 0) {
        alert("Chưa thêm host");
        return;
    }

    if (port.length == 0) {
        alert("Chưa thêm port");
        return;
    }

    $.ajax({
        url: short_url + "/sua",
        type: "post", //send it through get method
        data: JSON.stringify({
            email: email,
            password: pw,
            host: host,
            port: port
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa nội dung thành công',
                    type: 'success'
                });

            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Sửa nội dung  thất bại\n' + response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Sửa nội dung [#{content.title}] thất bại',
                type: 'error'
            });
        }
    });
}
