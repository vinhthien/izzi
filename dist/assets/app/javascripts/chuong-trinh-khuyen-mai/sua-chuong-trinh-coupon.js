function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: '/admin/sua-san-pham', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                $("#file_image").attr("src","../upload/"+data.data);
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link.length > 0)
    {
        callback(image_link);
    }
}

$('#optionsRadios2').change(function(){
    if(!$( "#gttn" ).hasClass( "hide" )){
        $( "#gttn" ).addClass("hide");
    }
    if(!$( "#gtcn" ).hasClass( "hide" )){
        $( "#gtcn" ).addClass("hide");
    }
    $( "#ip-gia-thap" ).val(null);
    $( "#ip-gia-thap" ).val(null);

});
$('#optionsRadios1').change(function(){
    $( "#gttn" ).removeClass("hide");
    $( "#gtcn" ).removeClass("hide");
});
function deactive_coupon_single(e) {
    var status = 2;
    var promotion_id = $(e).attr("promotion");
    $.ajax({
        url: "sua-chuong-trinh-coupon/change-status-single-coupon",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id,
            status: status
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạm ngưng coupon thành công',
                    type: 'success'
                });
                $('#status-coupon'+promotion_id).html('Ngưng hoạt động');
                e.innerText = 'Kích hoạt';

                e.onclick =  function() {
                    active_coupon_single(this);
                };
                $(e).attr("promotion_id",promotion_id);
                e.className = "btn btn-default";

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạm ngưng coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: "Tạm ngưng coupon thất bại",
                type: 'error'
            });
        }
    });
}

function active_coupon_single(e) {
    var promotion_id = $(e).attr("promotion");

    var status = 1;
    $.ajax({
        url: "sua-chuong-trinh-coupon/change-status-single-coupon",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id,
            status: status
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thành công',
                    type: 'success'
                });
                $('#status-coupon'+promotion_id).html('Đang hoạt động');
                e.innerText = 'Tạm ngưng';
                e.onclick =  function() {
                    deactive_coupon_single(this);
                };
                $(e).attr("promotion_id",promotion_id);
                e.className = "btn btn-primary";

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: "Tạm ngưng coupon thất bại",
                type: 'error'
            });
        }
    });
}




function deactive_coupon_multi(e) {
    var status = 2;
    var promotion_id = $(e).attr("promotion");
    $.ajax({
        url: "sua-chuong-trinh-coupon/change-status-multi-coupon",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id,
            status: status
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạm ngưng coupon thành công',
                    type: 'success'
                });
                $('#status-coupon'+promotion_id).html('Ngưng hoạt động');
                e.innerText = 'Kích hoạt';

                e.onclick =  function() {
                    active_coupon_multi(this);
                };
                $(e).attr("promotion_id",promotion_id);
                e.className = "btn btn-default";

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạm ngưng coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: "Tạm ngưng coupon thất bại",
                type: 'error'
            });
        }
    });
}

function active_coupon_multi(e) {
    var promotion_id = $(e).attr("promotion");

    var status = 1;
    $.ajax({
        url: "sua-chuong-trinh-coupon/change-status-multi-coupon",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id,
            status: status
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thành công',
                    type: 'success'
                });
                $('#status-coupon'+promotion_id).html('Đang hoạt động');
                e.innerText = 'Tạm ngưng';
                e.onclick =  function() {
                    deactive_coupon_multi(this);
                };
                $(e).attr("promotion_id",promotion_id);
                e.className = "btn btn-primary";

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: "Tạm ngưng coupon thất bại",
                type: 'error'
            });
        }
    });
}








function xoa_lien_ket(id,name,product_id) {
    $.ajax({
        url: "sua-chuong-trinh-coupon/xoa-lien-ket",
        type: "post", //send it through get method
        data: JSON.stringify({
            id: product_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa liên kết sản phẩm '+ name +" thành công",
                    type: 'success'
                });
                var temp = 'detail'+id;
                var row = document.getElementById(temp);
                row.parentNode.removeChild(row);
                $("#san_pham_select").append("<option value='"+product_id+"' selected>"+name+"</option>");
                $('#san_pham_select').trigger('change');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa liên kết sản phẩm '+ name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Xóa liên kết sản phẩm ['+ name +"] thất bại",
                type: 'error'
            });
        }
    });
}

function update_choice(option) {
    $.ajax({
        url: "sua-chuong-trinh-coupon/update-option",
        type: "post", //send it through get method
        data: JSON.stringify({
            option: option,
            promotion_id: promotion_id
        }),    contentType: "application/json",

        success: function (response) {
        },
        error: function (xhr) {

        }
    });
}
function add() {
    var product_name = $("#san_pham_select option:selected").text();
    var product_id = $("#san_pham_select option:selected").val();
    var a = $("#san_pham_select option:selected");
    var product_code = a[0].dataset.code;
    if(!product_id || product_id.length == 0 || product_name.length == 0){
        alert("chưa chọn sản phẩm")
        return;
    }
    $.ajax({
        url: "sua-chuong-trinh-coupon/them-lien-ket",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id,
            promotion_id: promotion_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm giá '+ product_name +" thành công",
                    type: 'success'
                });
                var detail_id = response.data;
                var temp_id = "detail"+detail_id;
                $("#san_pham_select option[value='" + product_id + "']").remove();
                var text = 'onclick=\'xoa_lien_ket("'+detail_id+'","'+product_name+'","'+product_id+'")\'';

                $('#tb-mon-ngon').append('<tr id=\''+temp_id+'\'>' +
                    '<td>'+product_code+'</td>' +
                    '<td>'+product_name+'</td>' +
                    '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');
                $('#san_pham_select').trigger('change');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm gía '+ product_name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });

}



function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function active_one(e,promotion_id) {
    $.ajax({
        url: "sua-chuong-trinh-coupon/active-one",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){

                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thành công',
                    type: 'success'
                });
                e.textContent = "Vô hiệu";
                e.parentNode.parentNode.cells[1].innerText = "Đang hoạt động";
                e.className = "btn btn-primary";
                e.onclick = function() {
                    deactive_one(e,promotion_id);
                };
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Kích hoạt coupon thất bại',
                type: 'error'
            });
        }
    });
}


function deactive_one(e,promotion_id) {
    $.ajax({
        url: "sua-chuong-trinh-coupon/deactive-one",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){

                new PNotify({
                    title: 'Thông báo',
                    text: 'Vô hiệu coupon thành công',
                    type: 'success'
                });
                e.parentNode.parentNode.cells[1].innerText = "Đã vô hiệu";
                e.textContent = "Kích hoạt";
                e.className = "btn btn-success";
                e.onclick = function() {
                    active_one(e,promotion_id);
                };
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Kích hoạt coupon thất bại',
                type: 'error'
            });
        }
    });
}
function deactive(e,id,coupon) {

    $.ajax({
        url: "sua-chuong-trinh-coupon/deactive",
        type: "post", //send it through get method
        data: JSON.stringify({
            id: id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Vô hiệu coupon thành công',
                    type: 'success'
                });
                e.parentNode.parentNode.cells[1].innerText = "Đã vô hiệu";
                e.textContent = "Kích hoạt";
                e.className = "btn btn-success";
                e.onclick = function() {
                    active(e,id,coupon);
                };
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Vô hiệu coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Vô hiệu coupon thất bại',
                type: 'error'
            });
        }
    });
}


function active(e,id,coupon) {

    $.ajax({
        url: "sua-chuong-trinh-coupon/active",
        type: "post", //send it through get method
        data: JSON.stringify({
            id: id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thành công',
                    type: 'success'
                });
                e.textContent = "Vô hiệu";
                e.parentNode.parentNode.cells[1].innerText = "Đang hoạt động";
                e.className = "btn btn-primary";
                e.onclick = function() {
                    deactive(e,id,coupon);
                };
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Kích hoạt coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Kích hoạt coupon thất bại',
                type: 'error'
            });
        }
    });

}

function update() {

    var code = $("#ip-ma-ctkm").val();
    var name = $("#ip-ten-ctkm").val();
    var beginDate = $("#ip-begin-date").val();
    var endDate = $("#ip-end-date").val();
    var listenSlider = $("#listenSlider").val();
    var giaThap = $("#ip-gia-thap").val();
    var giaCao = $("#ip-gia-cao").val();
    var order = $("#ip-order").val();

    if(code.length == 0){
        alert("Chưa thêm mã chương trình khuyến mãi");
        return;
    }
    if(name.length == 0){
        alert("Chưa thêm tên chương trình khuyến mãi");
        return;
    }
    if (beginDate.length == 0) {
        alert("Chưa thêm ngày bắt đầu chương trình khuyến mãi");
        return;
    }
    if (endDate.length == 0) {
        alert("Chưa thêm ngày kết thúc chương trình khuyến mãi");
        return;
    }

    // if (giaThap.length == 0) {
    //     alert("Chưa thêm giá khuyến mãi thấp nhất");
    //     return;
    // }
    //
    // if (giaCao.length == 0) {
    //     alert("Chưa thêm giá khuyến mãi cao nhất");
    //     return;
    // }

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return;
    }
    giaThap = revert_money_to_normal(giaThap);
    giaCao = revert_money_to_normal(giaCao);
    var data = {
        code: code,
        name: name,
        //beginDate: beginDate,
        //endDate: endDate,
        giaThap: giaThap,
        giaCao: giaCao,
        order: order,
        entity_code: entity_code,
        promotion_id: promotion_id,
        type: previous_type
    };

     if(previous_type == 3){
         var gift = $("#ip-gift").val();
         if(gift.length == 0){
             alert("chưa nhập mã gift");
             return;
         }
         data.gift=gift;

     }

    // uploadFile(function (url) {
    //     data.image = url;
    //     image_link= url;
        $.ajax({
            url: "sua-chuong-trinh-coupon/sua",
            type: "post", //send it through get method
            data: JSON.stringify(data),
            contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Cập nhật CT khuyến mãi ['+ name +"] thành công",
                        type: 'success'
                    });
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Cập nhật CT khuyến mãi ['+ name +"] thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Cập nhật CT khuyến mãi ['+ name +"] thất bại",
                    type: 'error'
                });
            }
        });
    // })

}
format_money_and_edit(document.getElementById("ip-gia-thap"));
format_money_and_edit(document.getElementById("ip-gia-cao"));