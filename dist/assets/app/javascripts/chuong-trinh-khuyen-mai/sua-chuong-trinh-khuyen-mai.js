$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    var name = $(".image-lib.has-border").attr("image-name");
    var url = $(".image-lib.has-border").attr("image-url");
    if (name && name.length > 0 && url && url.length > 0) {
        image_link = name;
        $('#file_image').attr("src", url);
        $('#file_image').css("max-width", "300px");
        $('#file_image').css("max-height", "150px");
        $('.image-lib').removeClass("has-border");
    }
});
function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                $("#file_image").attr("src","../upload/"+data.data);
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link.length > 0 || previous_type == 1)
    {
        callback(image_link);
    }
}


function xoa_lien_ket(product_code,product_name,product_id) {
    $.ajax({
        url: "sua-chuong-trinh-khuyen-mai/xoa-lien-ket",
        type: "post", //send it through get method
        data: JSON.stringify({
            id: product_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa liên kết sản phẩm '+ product_name +" thành công",
                    type: 'success'
                });
                var temp = 'detail'+product_id;
                var row = document.getElementById(temp);
                row.parentNode.removeChild(row);
                $("#san_pham_select").append("<option data-code='"+product_code+"' data-name='"+product_name+"'  value='"+product_id+"' selected> "+product_code+" | "+product_name+"</option>");
                $('#san_pham_select').trigger('change');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa liên kết sản phẩm '+ product_name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Xóa liên kết sản phẩm ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });
}

function xoa_gift_product(promotion_id,product_name,product_id) {
    $.ajax({
        url: "sua-chuong-trinh-khuyen-mai/xoa-product-gift",
        type: "post", //send it through get method
        data: JSON.stringify({
            promotion_id: promotion_id,
            product_id: product_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa sản phẩm ['+ name +"] thành công",
                    type: 'success'
                });
                var temp = 'product_gift'+product_id;
                var row = document.getElementById(temp);
                row.parentNode.removeChild(row);
                $("#san_pham_gift_select").append("<option value='"+product_id+"' selected>"+product_name+"</option>");
                $('#san_pham_gift_select').trigger('change');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Xóa sản phẩm ['+ product_name +"] thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Xóa sản phẩm ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });
}



function add() {
    var product_code = $("#san_pham_select option:selected").attr("data-code");
    var product_name = $("#san_pham_select option:selected").attr("data-name");
    var product_id = $("#san_pham_select option:selected").val();
    if(!product_id || product_id.length == 0 || product_name.length == 0){
        alert("chưa chọn sản phẩm")
        return;
    }
    $.ajax({
        url: "sua-chuong-trinh-khuyen-mai/them-lien-ket",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id,
            promotion_id: promotion_id,
            entity_code: entity_code
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm giá '+ product_name +" thành công",
                    type: 'success'
                });
                var detail_id = response.data;
                var temp_id = "detail"+detail_id;
                // $("#san_pham_select option:selected").empty();
                // $('#san_pham_select').select2('val','');
                $("#san_pham_select option[value='" + product_id + "']").remove();
                $('#san_pham_select').trigger('change');
                var text = 'onclick=\'xoa_lien_ket("'+detail_id+'","'+product_name+'","'+product_id+'")\'';
                $('#tb-mon-ngon').append('<tr id=\''+temp_id+'\'>' +
                    '<td>'+product_code+'</td>' +
                    '<td>'+product_name+'</td>' +
                    '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm gía '+ product_name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });

}

function add_product_gift() {
    var product_code = $("#san_pham_gift_select option:selected").attr("data-code");
    var product_name = $("#san_pham_gift_select option:selected").attr("data-name");
    var product_id = $("#san_pham_gift_select option:selected").val();
    if(!product_id || product_id.length == 0 || product_name.length == 0){
        alert("chưa chọn sản phẩm")
        return;
    }
    $.ajax({
        url: "sua-chuong-trinh-khuyen-mai/them-product-gift",
        type: "post", //send it through get method
        data: JSON.stringify({
            product_id: product_id,
            promotion_id: promotion_id
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm sản phẩm quà tặng ['+ product_name +"] thành công",
                    type: 'success'
                });
                var detail_id = response.data;
                var temp_id = "product_gift"+product_id;
                // $("#san_pham_select option:selected").empty();
                // $('#san_pham_select').select2('val','');
                $("#san_pham_gift_select option[value='" + product_id + "']").remove();
                $('#san_pham_gift_select').trigger('change');
                var text = 'onclick=\'xoa_gift_product("'+promotion_id+'","'+product_name+'","'+product_id+'")\'';
                $('#tb-product-gift').append('<tr id=\''+temp_id+'\'>' +
                    '<td>'+product_code+'</td>' +
                    '<td>'+product_name+'</td>' +
                    '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');

            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm gía '+ product_name +" thất bại\n"+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá ['+ product_name +"] thất bại",
                type: 'error'
            });
        }
    });

}


function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}



function update() {

    var code = $("#ip-ma-ctkm").val();
    var name = $("#ip-ten-ctkm").val();
    var beginDate = $("#ip-begin-date").val();
    var endDate = $("#ip-end-date").val();
    var listenSlider = $("#listenSlider").val();
    var giaThap = $("#ip-gia-thap").val();
    var giaCao = $("#ip-gia-cao").val();
    var order = $("#ip-order").val();
    var ipGift = $("#ip-gift").val();

    if(code.length == 0){
        alert("Chưa thêm mã chương trình khuyến mãi");
        return;
    }
    if(name.length == 0){
        alert("Chưa thêm tên chương trình khuyến mãi");
        return;
    }
    if (beginDate.length == 0) {
        alert("Chưa thêm ngày bắt đầu chương trình khuyến mãi");
        return;
    }
    if (endDate.length == 0) {
        alert("Chưa thêm ngày kết thúc chương trình khuyến mãi");
        return;
    }

    // if (giaThap.length == 0) {
    //     alert("Chưa thêm giá khuyến mãi thấp nhất");
    //     return;
    // }
    //
    // if (giaCao.length == 0) {
    //     alert("Chưa thêm giá khuyến mãi cao nhất");
    //     return;
    // }

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return;
    }
    giaThap = revert_money_to_normal(giaThap);
    giaCao = revert_money_to_normal(giaCao);
    var data = {
        code: code,
        name: name,
        beginDate: beginDate,
        endDate: endDate,
        giaThap: giaThap,
        giaCao: giaCao,
        order: order,
        entity_code: entity_code,
        promotion_id: promotion_id,
        type: previous_type
    };

     if(previous_type == 3){
         var gift = $("#ip-gift").val();
         if(gift.length == 0){
             alert("chưa nhập mã gift");
             return;
         }
         data.gift=gift;

     }

    uploadFile(function (url) {
        data.image = url;
        image_link= url;
        $.ajax({
            url: "sua-chuong-trinh-khuyen-mai/sua",
            type: "post", //send it through get method
            data: JSON.stringify(data),
            contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Cập nhật CT khuyến mãi ['+ name +"] thành công",
                        type: 'success'
                    });
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Cập nhật CT khuyến mãi ['+ name +"] thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Cập nhật CT khuyến mãi ['+ name +"] thất bại",
                    type: 'error'
                });
            }
        });
    })

}
format_money_and_edit(document.getElementById("ip-gia-thap"));
format_money_and_edit(document.getElementById("ip-gia-cao"));