function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'tao-coupon', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else
    {
        alert("Vui lòng chọn file");
    }
}

function create_one_coupon() {
    var number = $("#number_manual").val();
    if(number.length == 0){
        alert("Chưa nhập số kí tự");
        return;
    }
    if(number < 0){
        alert("Không nhận số âm");
        return;
    }

    $.ajax({
        url: "tao-coupon/get_new_coupon",
        type: "post", //send it through get method
        data: JSON.stringify({number: number}),
        contentType: "application/json",
        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                $("#coupon").val(response.data)
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạo coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Tạo coupon thất bại',
                type: 'error'
            });
        }
    });
}

function create_multi_coupon() {
    $("#tb-coupon").html("");
    var number_text = $("#number_auto").val();
    if(number_text.length == 0){
        alert("Chưa nhập số kí tự");
        return;
    }
    if(number_text < 0){
        alert("Không nhận số âm");
        return;
    }
    var text_one = $('#text1').val();
    var text_two = $('#text2').val();
    var number = $('#number').val();
    if(text_one.length==0){
        alert("Chưa thêm mở đầu của coupon");
        return;
    }

    if(number.length==0){
        alert("Chưa thêm số lượng coupon");
        return;
    }

    $.ajax({
        url: "tao-coupon/get_new_multi_coupon",
        type: "post", //send it through get method
        data: JSON.stringify({
            number: number,
            pre: text_one,
            last: text_two,
            number_text: number_text
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                for(var index=0;index<response.data.length;index++){
                    var text = response.data[index];
                    $('#tb-coupon').append('<tr>' +
                        '<td>'+text+'</td>' +
                        '</tr>');

                }
                $("#number-x").empty();
                for(var index=0;index<number_text;index++){
                    $("#number-x").append("X ");
                }
            }else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Tạo coupon thất bại\n'+response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Tạo coupon thất bại',
                type: 'error'
            });
        }
    });
}

function validate_second_form() {
    var check_one = $('#radio1').is(":checked");
    var check_two = $('#radio2').is(":checked");

    if(!check_one && !check_two){
        alert("Chưa tích chọn lựa chọn");
        return false;
    }

    if(check_one){
        var coupon = $('#coupon').val();
        var time_use = $("#number_use").val();

        if (coupon.length == 0){
            alert("Chưa thêm mã coupon");
            return false;

        }
        if(time_use.length == 0){
            alert("Chưa thêm số lần cho phép sử dụng của coupon");
            return false;
        }
        if(time_use < 0){
            alert("Số lần cho phép sử dụng của coupon không được âm");
            return false;
        }


    }else if(check_two){
        var rowCount = $('#tb-coupon tr').length;
        if (rowCount == 0){
            alert("Chưa tạo coupon");
            return false;
        }

    }
    return true;
}

function finish() {
    var check_one = $('#radio1').is(":checked");
    var check_two = $('#radio2').is(":checked");

    if(!check_one && !check_two){
        alert("Chưa tích chọn lựa chọn");
        return false;
    }


    function add_data(coupon,time_use,callback) {
            var url = "";
            var code = $("#ip-ma-ctkm").val();
            var name = $("#ip-ten-ctkm").val();
            var beginDate = start_date;
            var endDate = end_date;
            var optionsRadios = $('input[name=optionsRadios]:checked').val();
            var optionsRadios1 = $("#optionsRadios1").val();
            var optionsRadios2 = $("#optionsRadios2").val();
            var ipGiamGiaCoDinh = $("#ip-giam-gia-co-dinh").val();
            var listenSlider = $("#listenSlider").val();
            var giaThap = $("#ip-gia-thap").val();
            var giaCao = $("#ip-gia-cao").val();
            var order = $("#ip-order").val();

            var optionsRadiosApDung = $('input[name=optionsRadiosApDung]:checked').val();
            var optionsRadiosApDung1 = $("#optionsRadiosApDung1").val();
            var optionsRadiosApDung2 = $("#optionsRadiosApDung2").val();

            if(giaThap.length == 0 && giaCao.length == 0){
                giaThap = null;
                giaCao = null;
            }else {
                giaThap = revert_money_to_normal(giaThap);
                giaCao = revert_money_to_normal(giaCao);
            }

            $.ajax({
                url: "tao-coupon/add-coupon",
                type: "post", //send it through get method
                data: JSON.stringify({
                    code: code,
                    name: name,
                    beginDate: beginDate,
                    endDate: endDate,
                    optionsRadios1: optionsRadios == optionsRadios1,
                    optionsRadios2: optionsRadios == optionsRadios2,
                    ipGiamGiaCoDinh: ipGiamGiaCoDinh,
                    ipMaQuaTang: ipGiamGiaCoDinh,
                    listenSlider: listenSlider,
                    giaThap: giaThap,
                    giaCao: giaCao,
                    order: order,
                    image: url,
                    time_use:time_use,
                    coupon: coupon,
                    optionsRadiosApDung1: optionsRadiosApDung == optionsRadiosApDung1,
                    optionsRadiosApDung2: optionsRadiosApDung == optionsRadiosApDung2,
                }), contentType: "application/json",

                success: function (response) {
                    callback(response);
                    if (optionsRadiosApDung == optionsRadiosApDung2) { // apply for selected products
                        sync_san_pham_ap_dung(response.data.promotion_id);
                    } else {
                        refresh_coupon();
                        window.location.href = "sua-chuong-trinh-coupon?id="+response.data.promotion_id;
                    }




                },
                error: function (xhr) {
                    //Do Something to handle error
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm CT khuyến mãi [' + name + "] thất bại",
                        type: 'error'
                    });
                }
            });
    }




    if(check_one){
        var coupon = $('#coupon').val();
        var time_use = $("#number_use").val();

        if (coupon.length == 0){
            alert("Chưa thêm mã coupon");
            return;
        }
        if(time_use.length == 0){
            alert("Chưa thêm số lần cho phép sử dụng của coupon");
            return;
        }
        if(time_use < 0){
            alert("Số lần cho phép sử dụng của coupon không được âm");
            return;
        }
        add_data(coupon,time_use,function (response) {
            if (response.response_code == "SUCC_EXEC") {
                var name = $("#ip-ten-ctkm").val();

                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm CT khuyến mãi ['+ name +"] thành công",
                    type: 'success'
                });


            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm CT khuyến mãi [' + name + "] thất bại\n" + response.description,
                    type: 'error'
                });
            }
        })


    }else if(check_two){
        var rowCount = $('#tb-coupon tr').length;
        if (rowCount == 0){
            alert("Chưa tạo coupon");
            return;
        }
        add_data(null,null,function (response) {
            if (response.response_code == "SUCC_EXEC") {
                var name = $("#ip-ten-ctkm").val();
                var promotion_id = response.data.promotion_id;

                var array = get_data_table();
                $.ajax({
                    url: "tao-coupon/add-multi-coupon",
                    type: "post", //send it through get method
                    data: JSON.stringify({
                        array: array,
                        promotion_id: promotion_id
                    }),    contentType: "application/json",

                    success: function (response) {
                        if(response.response_code == "SUCC_EXEC"){
                            new PNotify({
                                title: 'Thông báo',
                                text: 'Thêm coupon thành công',
                                type: 'success'
                            });
                            refresh_coupon();
                        }else {
                            new PNotify({
                                title: 'Thông báo',
                                text: 'Thêm coupon thất bại\n'+response.description,
                                type: 'error'
                            });
                        }

                    },
                    error: function (xhr) {
                        //Do Something to handle error
                        new PNotify({
                            title: 'Thông báo',
                            text: 'Thêm coupon thất bại',
                            type: 'error'
                        });
                    }
                });

            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm CT khuyến mãi [' + name + "] thất bại\n" + response.description,
                    type: 'error'
                });
            }
        })





    }

}

function _getUnselectedProductPromotionCoupon() {
    $.ajax({
        url: "tao-coupon/getUnselectedProductPromotionCoupon",
        type: "post", //send it through get method
        data: JSON.stringify({
        }),    contentType: "application/json",

        success: function (response) {
            if(response.response_code == "SUCC_EXEC"){
                for (var i = 0; i < response.data.unSelectedPromotion.length; i++) {
                    var product = response.data.unSelectedPromotion[i];

                    $("#san_pham_select").append($("<option></option>")
                        .attr("value", product.product_id )
                        .attr("data-code", product.product_code )
                        .attr("data-name", product.product_name )
                        .text(product.product_code + " | " + product.product_name ));
                }

            }

        }
    });
}

function refresh_coupon() {
    $("#tb-coupon").html("");
    $("#number_manual").val("");
    $("#coupon").val("");
    $("#number_auto").val("");
    $("#text1").val("");
    $("#text2").val("");
    $("#number").val("");
    $("#tb-mon-ngon tbody").html("");
    $("#san_pham_select").html("");
    link_array = [];
    _getUnselectedProductPromotionCoupon();


    var back = document.getElementById('back');
    back.click();
}

function get_data_table() {
    var array = [];
    var table = document.getElementById('tb-coupon');

    var rowLength = table.rows.length;

    for(var i=0; i<rowLength; i+=1){
        var row = table.rows[i];

        //your code goes here, looping over every row.
        //cells are accessed as easy

        var cell = row.cells[0].innerText;
        array.push(cell);
    }
    return array;
}


function validate_form() {
    var code = $("#ip-ma-ctkm").val();
    var name = $("#ip-ten-ctkm").val();
    var beginDate = start_date;
    var endDate = end_date;
    var optionsRadios = $('input[name=optionsRadios]:checked').val();
    var optionsRadios1 = $("#optionsRadios1").val();
    var optionsRadios2 = $("#optionsRadios2").val();
    var ipGiamGiaCoDinh = $("#ip-giam-gia-co-dinh").val();
    var listenSlider = $("#listenSlider").val();
    var order = $("#ip-order").val();
    if(code.length == 0){
        alert("Chưa thêm mã chương trình khuyến mãi");
        return false;
    }
    if(name.length == 0){
        alert("Chưa thêm tên chương trình khuyến mãi");
        return false;
    }
    if(!beginDate){
        alert("Chưa chọn ngày bắt đầu chương trình khuyến mãi");
        return false;
    }
    if (beginDate.length == 0) {
        alert("Chưa thêm ngày bắt đầu chương trình khuyến mãi");
        return false;
    }
    if(!endDate){
        alert("Chưa chọn ngày kết thúc chương trình khuyến mãi");
        return false;
    }
    if (endDate.length == 0) {
        alert("Chưa thêm ngày kết thúc chương trình khuyến mãi");
        return false;
    }

    if (optionsRadios1 == optionsRadios) {
        if (ipGiamGiaCoDinh.length == 0) {
            alert("Chưa thêm giảm giá cố định");
            return false;
        }
    }
    if (optionsRadios2 == optionsRadios) {
        if (listenSlider.length == 0) {
            alert("Chưa chọn phần trăm giảm giá");
            return false;
        }
    }


    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return false;
    }
    return true;
}

(function( $ ) {

    'use strict';

    /*
    Wizard #1
    */
    var $w1finish = $('#w1').find('ul.pager li.finish'),
        $w1validator = $("#w1 form").validate({
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
                $(element).remove();
            },
            errorPlacement: function( error, element ) {
                element.parent().append( error );
            }
        });

    $w1finish.on('click', function( ev ) {
        ev.preventDefault();
        finish();
    });

    $('#w1').bootstrapWizard({
        tabClass: 'wizard-steps',
        nextSelector: 'ul.pager li.next',
        previousSelector: 'ul.pager li.previous',
        firstSelector: null,
        lastSelector: null,
        onNext: function( tab, navigation, index, newindex ) {
            if(index == 1){
                if (!validate_form()){
                    // $w1validator.focusInvalid();
                    return false;
                }
            } else if (index == 2) {
                if (!validate_second_form()) {
                    return false
                }

            }
            // var validated = $('#w1 form').valid();
            // if( !validated ) {
            //     $w1validator.focusInvalid();
            //     return false;
            // }
        },
        onTabClick: function( tab, navigation, index, newindex ) {


            if(newindex == 1){
                if(!validate_form()){
                    return false;
                }
            }else {
                if ( newindex == index + 1 ) {
                    return this.onNext( tab, navigation, index, newindex);
                } else if ( newindex > index + 1 ) {
                    return false;
                } else {
                    return true;
                }
            }
        },
        onTabChange: function( tab, navigation, index, newindex ) {
            var totalTabs = navigation.find('li').size() - 1;
            $w1finish[ newindex != totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );
            $('#w1').find(this.nextSelector)[ newindex == totalTabs ? 'addClass' : 'removeClass' ]( 'hidden' );
        }
    });



}).apply( this, [ jQuery ]);

var link_array = [];

function xoa_lien_ket(name,content_id) {
    if(link_array.length > 0) {
        var pos = null;
        for(var alpha=0;alpha<link_array.length;alpha++){
            if(link_array[alpha].product_id == content_id){
                pos = alpha;
                break;
            }
        }
        link_array.splice(pos,1);
        new PNotify({
            title: 'Thông báo',
            text: 'Xóa sản phẩm [' + name + "] thành công",
            type: 'success'
        });
        var temp = 'monngon' + content_id;
        var row = document.getElementById(temp);
        row.parentNode.removeChild(row);
        $("#san_pham_select").append("<option value='" + content_id + "' selected>" + name + "</option>");
        $('#san_pham_select').trigger('change');
    }
}



function sync_san_pham_ap_dung(promotion_id) {
    for(var index=0;index<link_array.length;index++){
        var content_name = link_array[index].product_name;
        var product_id = link_array[index].product_id;
        $.ajax({
            url: "tao-coupon/them-san-pham-ap-dung",
            type: "post", //send it through get method
            data: JSON.stringify({
                product_id: product_id,
                promotion_id: promotion_id
            }),    contentType: "application/json",

            success: function (response) {
                if(response.response_code == "SUCC_EXEC"){
                    refresh_coupon();
                    window.location.href = "sua-chuong-trinh-coupon?id="+promotion_id;
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm sản phẩm áp dụng coupon ['+ content_name +"] thất bại\n"+response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm sản phẩm áp dụng coupon ['+ content_name +"] thất bại",
                    type: 'error'
                });
            }
        });
    }

}
function addSanPhamApDung() {
    var content_id = $("#san_pham_select option:selected").val();
    var content_code = $("#san_pham_select option:selected").attr('data-code');
    var content_name = $("#san_pham_select option:selected").attr('data-name');
    if(!content_id || content_id.length == 0 || content_name.length == 0){
        alert("chưa chọn món ngon");
        return;
    }
    link_array.push({product_id: content_id,product_code: content_code,product_name: content_name});
    var temp_id = "monngon"+content_id;
    $("#san_pham_select option:selected").empty();
    $('#san_pham_select').select2('val','');
    $("#san_pham_select option[value='" + content_id + "']").remove();
    $('#san_pham_select').trigger('change');
    var text = 'onclick=\'xoa_lien_ket("'+content_name+'","'+content_id+'")\'';
    $('#tb-mon-ngon').append('<tr id=\''+temp_id+'\'>' +
        '<td>'+content_code+'</td>' +
        '<td>'+content_name+'</td>' +
        '<td><button class="btn btn-primary" '+ text +' >Xóa</button></td></tr>');


}