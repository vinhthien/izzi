var gift_product_array = [];
var link_product_array = [];
var global_promotion_id = null;
var image_link = null;
$(document).on('click', '.modal-confirm', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
    var name = $(".image-lib.has-border").attr("image-name");
    var url = $(".image-lib.has-border").attr("image-url");
    if (name && name.length > 0 && url && url.length > 0) {
        image_link = name;
        $('#image_res').attr("src", url);
        $('#image_res').css("max-width", "300px");
        $('#image_res').css("max-height", "150px");
        $('.image-lib').removeClass("has-border");
    }
});
$('#optionsRadios1').change(function () {
    if (!$("#panel-qua-tang").hasClass("hide")) {
        $("#panel-qua-tang").addClass("hide");
    }

});
$('#optionsRadios2').change(function () {
    if (!$("#panel-qua-tang").hasClass("hide")) {
        $("#panel-qua-tang").addClass("hide");
    }
});
$('#optionsRadios3').change(function () {
    $("#panel-qua-tang").removeClass("hide");
});


function xoa_lien_ket(name, product_id) {
    new PNotify({
        title: 'Thông báo',
        text: 'Xóa liên kết sản phẩm ' + name + " thành công",
        type: 'success'
    });
    var pos = null;
    for (var index = 0; index < link_product_array.length; index++) {
        var obj = link_product_array[index];
        if (obj.product_id == product_id) {
            pos = index;
            break;
        }
    }
    if (pos != null) {
        link_product_array.splice(pos, 1);
    }
    var temp = 'link' + product_id;
    var row = document.getElementById(temp);
    row.parentNode.removeChild(row);
    $("#san_pham_select").append("<option value='" + product_id + "' selected>" + name + "</option>");
    $('#san_pham_select').trigger('change');

}

function xoa_gift_product(product_name, product_id) {
    new PNotify({
        title: 'Thông báo',
        text: 'Xóa sản phẩm [' + product_name + "] thành công",
        type: 'success'
    });
    var pos = null;
    var product_code = null;
    for (var index = 0; index < gift_product_array.length; index++) {
        var obj = gift_product_array[index];
        if (obj.product_id == product_id) {
            pos = index;
            product_code = obj.product_code;
            break;
        }
    }
    if (pos != null) {
        gift_product_array.splice(pos, 1);
    }

    var temp = 'product_gift' + product_id;
    var row = document.getElementById(temp);
    row.parentNode.removeChild(row);
    $("#san_pham_gift_select").append("<option value='" + product_id + "' data-code='" + product_code + "' data-name='" + product_name + "' selected>" + product_name + "</option>");
    $('#san_pham_gift_select').trigger('change');

}


function them_lien_ket() {
    var product_code = $("#san_pham_select option:selected").attr("data-code");
    var product_name = $("#san_pham_select option:selected").attr("data-name");
    var product_id = $("#san_pham_select option:selected").val();
    if (!product_id || product_id.length == 0 || product_name.length == 0) {
        alert("chưa chọn sản phẩm")
        return;
    }

    link_product_array.push({product_id: product_id, product_code: product_code, product_name: product_name});
    new PNotify({
        title: 'Thông báo',
        text: 'Thêm liên kết giảm giá ' + product_name + " thành công",
        type: 'success'
    });
    var temp_id = "link" + product_id;
    $("#san_pham_select option[value='" + product_id + "']").remove();
    $('#san_pham_select').trigger('change');
    var text = 'onclick=\'xoa_lien_ket("' + product_name + '","' + product_id + '")\'';
    $('#tb-mon-ngon').append('<tr id=\'' + temp_id + '\'>' +
        '<td>' + product_code + '</td>' +
        '<td>' + product_name + '</td>' +
        '<td><button class="btn btn-primary" ' + text + ' >Xóa</button></td></tr>');


}

function sync_link_product(promotion_id_temp, entity_code, callback) {

    $.ajax({
        url: "them-chuong-trinh-khuyen-mai/them-lien-ket",
        type: "post", //send it through get method
        data: JSON.stringify({
            link_product_array: link_product_array,
            promotion_id: promotion_id_temp,
            entity_code: entity_code
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                callback();
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm gía ' + promotion_id_temp + " thất bại\n" + response.description,
                    type: 'error'
                });
            }
        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá [' + promotion_id_temp + "] thất bại",
                type: 'error'
            });
        }
    });

}

function sync_gift_product(promotion_id_temp, callback) {
    $.ajax({
        url: "them-chuong-trinh-khuyen-mai/them-product-gift",
        type: "post", //send it through get method
        data: JSON.stringify({
            gift_product_array: gift_product_array,
            promotion_id: promotion_id_temp
        }), contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {
                // new PNotify({
                //     title: 'Thông báo',
                //     text: 'Thêm sản phẩm quà tặng ['+ promotion_id_temp +"] thành công",
                //     type: 'success'
                // });
                callback();
            } else {
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm liên kết giảm gía ' + promotion_id_temp + " thất bại\n" + response.description,
                    type: 'error'
                });
            }

        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá [' + promotion_id_temp + "] thất bại",
                type: 'error'
            });
        }
    });


}

function add_product_gift() {
    var product_code = $("#san_pham_gift_select option:selected").attr("data-code");
    var product_name = $("#san_pham_gift_select option:selected").attr("data-name");
    var product_id = $("#san_pham_gift_select option:selected").val();
    if (!product_id || product_id.length == 0 || product_name.length == 0) {
        alert("chưa chọn sản phẩm");
        return;
    }
    new PNotify({
        title: 'Thông báo',
        text: 'Thêm sản phẩm quà tặng [' + product_name + "] thành công",
        type: 'success'
    });
    gift_product_array.push({product_id: product_id, product_code: product_code, product_name: product_name});
    var temp_id = "product_gift" + product_id;
    // $("#san_pham_select option:selected").empty();
    // $('#san_pham_select').select2('val','');
    $("#san_pham_gift_select option[value='" + product_id + "']").remove();
    $('#san_pham_gift_select').trigger('change');
    var text = 'onclick=\'xoa_gift_product("' + product_name + '","' + product_id + '")\'';
    $('#tb-product-gift').append('<tr id=\'' + temp_id + '\'>' +
        '<td>' + product_code + '</td>' +
        '<td>' + product_name + '</td>' +
        '<td><button class="btn btn-primary" ' + text + ' >Xóa</button></td></tr>');

}


function uploadFile(callback) {
    if ($("#file_to_upload").val() != "") {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (data) {
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if (data.response_code == "SUCC_EXEC") {
                    callback(data.data);
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if (image_link != null) {
        callback(image_link);

    } else {
        alert("Vui lòng chọn file");
    }
}

function format_money_and_edit(e) {
    var number = e.value;
    if (number == "") return;
    number = revert_money_to_normal(number);
    number = format_money(number);
    e.value = number;
}

function change(number) {
    if (number != 1) {
        $("#icon-frm").removeClass("hide");
    } else {
        $("#icon-frm").addClass("hide");
    }
}


function parse_number(number) {
    return parseFloat(Math.round(number * 100) / 100).toFixed(2);
}

function reset_link_product_table() {
    $("#tb-mon-ngon tr").remove();
}

function reset_unselected_product() {
    $.ajax({
        url: "them-chuong-trinh-khuyen-mai/get-unselected-promotion",
        type: "post", //send it through get method
        data: "", contentType: "application/json",

        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {

            } else {

            }
        },
        error: function (xhr) {
            //Do Something to handle error
            new PNotify({
                title: 'Thông báo',
                text: 'Thêm liên kết giảm giá [' + promotion_id_temp + "] thất bại",
                type: 'error'
            });
        }
    });
}


function add() {
    var code = $("#ip-ma-ctkm").val();
    var name = $("#ip-ten-ctkm").val();
    var beginDate = start_date;
    var endDate = end_date;
    var optionsRadios = $('input[name=optionsRadios]:checked').val();
    var optionsRadios1 = $("#optionsRadios1").val();
    var optionsRadios2 = $("#optionsRadios2").val();
    var optionsRadios3 = $("#optionsRadios3").val();
    var optionsRadios4 = $("#optionsRadios4").val();
    var ipGiamGiaCoDinh = $("#ip-giam-gia-co-dinh").val();
    var ipGift = $("#ip-gift").val();
    var listenSlider = $("#listenSlider").val();
    var giaThap = $("#ip-gia-thap").val();
    var giaCao = $("#ip-gia-cao").val();
    var order = $("#ip-order").val();


    if (code.length == 0) {
        alert("Chưa thêm mã chương trình khuyến mãi");
        return;
    }
    if (name.length == 0) {
        alert("Chưa thêm tên chương trình khuyến mãi");
        return;
    }
    if (beginDate == null) {
        alert("Chưa thêm ngày bắt đầu chương trình khuyến mãi");
        return;
    }
    if (beginDate.length == 0) {
        alert("Chưa thêm ngày bắt đầu chương trình khuyến mãi");
        return;
    }
    if (endDate.length == 0) {
        alert("Chưa thêm ngày kết thúc chương trình khuyến mãi");
        return;
    }

    if (optionsRadios1 == optionsRadios) {
        if (ipGiamGiaCoDinh.length == 0) {
            alert("Chưa thêm giảm giá cố định");
            return;
        }
        image_link = "";
    }
    if (optionsRadios2 == optionsRadios) {
        if (listenSlider.length == 0) {
            alert("Chưa chọn phần trăm giảm giá");
            return;
        }
    }

    if (optionsRadios3 == optionsRadios) {
        if (ipGift.length == 0) {
            alert("Chưa nhập mã quà tặng");
            return;
        }
    }

    if (document.getElementById("file_to_upload").files.length == 0 && image_link == null) {
        alert("hình ảnh còn trống");
        return;
    }

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp sản phẩm");
        return;
    }

    uploadFile(function (url) {
        var code = $("#ip-ma-ctkm").val();
        var name = $("#ip-ten-ctkm").val();
        var beginDate = start_date;
        var endDate = end_date;
        var optionsRadios = $('input[name=optionsRadios]:checked').val();
        var optionsRadios1 = $("#optionsRadios1").val();
        var optionsRadios2 = $("#optionsRadios2").val();
        var optionsRadios3 = $("#optionsRadios3").val();
        var ipGiamGiaCoDinh = $("#ip-giam-gia-co-dinh").val();
        var ipGift = $("#ip-gift").val();
        var ipIsCoupon = $("#ip-is-coupon").prop('checked');
        var ipCoupon = $("#ip-coupon").val();
        var listenSlider = $("#listenSlider").val();
        var giaThap = $("#ip-gia-thap").val();
        var giaCao = $("#ip-gia-cao").val();
        var order = $("#ip-order").val();
        giaThap = revert_money_to_normal(giaThap);
        giaCao = revert_money_to_normal(giaCao);
        $.ajax({
            url: "them-chuong-trinh-khuyen-mai/them",
            type: "post", //send it through get method
            data: JSON.stringify({
                code: code,
                name: name,
                beginDate: beginDate.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                endDate: endDate.format("YYYY-MM-DDTHH:mm:ss.SSSZ"),
                optionsRadios1: optionsRadios == optionsRadios1,
                optionsRadios2: optionsRadios == optionsRadios2,
                optionsRadios3: optionsRadios == optionsRadios3,
                ipGiamGiaCoDinh: ipGiamGiaCoDinh,
                ipIsCoupon: ipIsCoupon,
                ipCoupon: ipCoupon,
                ipGift: ipGift,
                ipMaQuaTang: ipGiamGiaCoDinh,
                listenSlider: listenSlider,
                giaThap: giaThap,
                giaCao: giaCao,
                order: order,
                image: url
            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm CT khuyến mãi [' + name + "] thành công",
                        type: 'success'
                    });
                    var id = response.data.id;
                    var entity_code = response.data.entity_code;
                    var type = response.data.type;
                    if (type == 3) {
                        sync_link_product(id, entity_code, function () {
                            sync_gift_product(id, function () {
                                window.location.href = "sua-chuong-trinh-khuyen-mai?id=" + id;
                            });
                        });

                    } else {
                        sync_link_product(id, entity_code, function () {
                            window.location.href = "sua-chuong-trinh-khuyen-mai?id=" + id;
                        });
                    }
                    reset_link_product_table();
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm CT khuyến mãi [' + name + "] thất bại\n" + response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm CT khuyến mãi [' + name + "] thất bại",
                    type: 'error'
                });
            }
        });
    })


}


