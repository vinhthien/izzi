/*
Name: 			UI Elements / Modals - Examples
Written by: 	Okler Themes - (http://www.okler.net)
Theme Version: 	1.4.0
*/


function showProductWithoutPromotion(html, product) {
    html.push(
        '<tr class="b-top-none">',
        '<td><label class="mb-none"><strong>Mã sản phẩm:</strong></label></td>',
        '<td>' + product.product_code + '</td>',
        '<td><label class="mb-none"><strong>Số lượng:</strong></label></td>',
        '<td>' + product.order_detail_quantity + '</td>',
        '<td><label class="mb-none"><strong>Thành tiền:</strong></label></td>',
        '<td colspan="2" align="right">' + format(product.order_detail_total_amount) + ' VNĐ</td>',
        '</tr>'
    );

    html.push(
        '<tr class="b-top-none">',
        '<td><label class="mb-none"><strong>Tên sản phẩm:</strong></label></td></td>',
        '<td colspan="5">' + product.product_title + '</td>',
        '</tr>'
    );

}

function showProductWithPromotion(html, product, index) {

    if (product.order_detail_price_promotion == null) {
        var purchase_order_detail_id = product.order_detail_id;

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"><strong>Mã sản phẩm:</strong></label></td>',
            '<td>' + product.product_code + '</td>',
            '<td><label class="mb-none"><strong>Số lượng:</strong></label></td>',
            '<td>' + product.order_detail_quantity + '</td>',
            '<td><label class="mb-none"><strong>Thành tiền:</strong></label></td>',
            '<td colspan="2" align="right">' + format(product.order_detail_total_amount) + ' VNĐ</td>',
            '</tr>'
        );

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"><strong>Tên sản phẩm:</strong></label></td></td>',
            '<td colspan="5">' + product.product_title + '</td>',
            '</tr>'
        );

        html.push(
            '<div id="quatang'+index+'"></div>'
        );
        get_gift_product(purchase_order_detail_id, function (data) {

            if (!data || data.length == 0) {
                return;
            }

            var html1 = '<tr class="b-top-none">'+
                '<td><label class="mb-none"></label></td>'+
                '<td colspan="2"><label class="mb-none"><strong>Danh sách sản phẩm quà tặng:</strong></label></td>'+
                '</tr>';
            for (var alpha = 0; alpha < data.length; alpha++) {
                html1 += '<tr class="b-top-none">' +
                    '<td><label class="mb-none"></label></td>' +
                    '<td><label class="mb-none"><strong>Mã sản phẩm:</strong></label></td>' +
                    '<td>' + data[alpha].product_code + '</td>' +
                    '</tr>';

                html1 += '<tr class="b-top-none">' +
                    '<td><label class="mb-none"></label></td>' +
                    '<td><label class="mb-none"><strong>Tên sản phẩm:</strong></label></td>' +
                    '<td colspan="5">' + data[alpha].product_name + '</td>' +
                    '</tr>';
            }
            //html = [html.join("").replace('quatang'+index, html1)];

            //$("#quatang"+index,tr).html(html1);
            $("#quatang"+index).html(html1);

        })
    } else {

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"><strong>Mã sản phẩm:</strong></label></td>',
            '<td>' + product.product_code + '</td>',
            '<td><label class="mb-none"><strong>Số lượng:</strong></label></td>',
            '<td>' + product.order_detail_quantity + '</td>',
            '<td><label class="mb-none"><strong>Thành tiền:</strong></label></td>',
            '<td colspan="2" align="right" id="price_product' + index + '" >' + format(product.order_detail_total_amount - product.order_detail_price_promotion) + ' VNĐ</td>',
            '</tr>'
        );

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"><strong>Tên sản phẩm:</strong></label></td></td>',
            '<td colspan="5">' + product.product_title + '</td>',
            '</tr>'
        );

    }

    html.push(
        '<tr class="b-top-none">',
        '<td><label class="mb-none"><strong>Chương trình khuyến mãi:</strong></label></td>',
        '<td>' + product.promotion_name + '</td>',
        '</tr>'
    );



}

function handle_data(index,temp,html,src,datatable,tr) {
    if (index > (temp.length-1)) {
        return;
    }

    if (temp != null) {
        temp.products.forEach(function (product, index) {
            if (product.order_detail_is_promotion) {
                showProductWithPromotion(html, product, index);
            } else {
                showProductWithoutPromotion(html, product);
            }

        });

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"></label></td>',
            '<td></td>',
            '<td><label class="mb-none"></label></td></td>',
            '<td></td>',
            '<td><label class="mb-none"></label></td>',
            '<td><label class="mb-none"><strong>Giá vận chuyển:</strong></label></td>',
            '<td colspan="2" align="right">' + format(temp.phi_van_chuyen) + ' VNĐ</td>',
            '</tr>'
        );

        if (temp.couponApplied) {
            html.push(
                '<tr class="b-top-none">',
                '<td><label class="mb-none"></label></td>',
                '<td></td>',
                '<td><label class="mb-none"></label></td></td>',
                '<td></td>',
                '<td><label class="mb-none"></label></td>',
                '<td><label class="mb-none"><strong>Khuyễn mãi coupon:</strong></label></td>',
                '<td colspan="2" align="right">' + format(temp.coupon_discount) + ' VNĐ</td>',
                '</tr>'
            );
        }

        html.push(
            '<tr class="b-top-none">',
            '<td><label class="mb-none"></label></td>',
            '<td></td>',
            '<td><label class="mb-none"></label></td></td>',
            '<td></td>',
            '<td><label class="mb-none"></label></td>',
            '<td><label class="mb-none"><strong>Tổng cộng:</strong></label></td>',
            '<td colspan="2" align="right">' + format(temp.final_total_amount) + ' VNĐ</td>',
            '</tr>'
        );
        html.push('</table>');
        datatable.fnOpen(tr, html.join(""), 'details');
    }
}

(function( $ ) {

    'use strict';

    var datatableInit = function() {
        var $table = $('#datatable-tabletools');

        // format function for row details
        var fnFormatDetails = function( datatable, tr ) {
            var data = datatable.fnGetData( tr );
			var src = JSON.parse(tr.attributes.object.value);
            return [
                '<table class="table mb-none">',
                '<tr class="b-top-none">',
                '<td><label class="mb-none"><strong>Mã sản phẩm:</strong></label></td>',
                '<td>' + src.code+ '</td>',
                '<td><label class="mb-none"><strong>Tên sản phẩm:</strong></label></td></td>',
                '<td>' + src.product_title+ '</td>',
                '<td><label class="mb-none"><strong>Giá:</strong></label></td>',
                '<td>' + format(src.price)+ '</td>',
                '<td><label class="mb-none"><strong>Số lượng:</strong></label></td>',
                '<td>' + src.quantity+ '</td>',
                '</tr>',
                '</div>'
            ].join('');


        };

        // insert the expand/collapse column
        var th = document.createElement( 'th' );
        var td = document.createElement( 'td' );
        td.innerHTML = '<i data-toggle class="fa fa-plus-square-o text-primary h5 m-none" style="cursor: pointer;"></i>';
        td.className = "text-center";

        $table
            .find( 'thead tr' ).each(function() {
            this.insertBefore( th, this.childNodes[0] );
        });

        $table
            .find( 'tbody tr' ).each(function() {
            this.insertBefore(  td.cloneNode( true ), this.childNodes[0] );
        });

        // initialize
        var datatable = $table.dataTable({
            aoColumnDefs: [{
                bSortable: false,
                aTargets: [ 0 ]
            }],
            aaSorting: [
                [2, 'desc']
            ]
        });

        // add a listener
        $table.on('click', 'i[data-toggle]', function() {
            var $this = $(this),
                tr = $(this).closest( 'tr' ).get(0);

            if ( datatable.fnIsOpen(tr) ) {
                $this.removeClass( 'fa-minus-square-o' ).addClass( 'fa-plus-square-o' );
                datatable.fnClose( tr );
            } else {
                $this.removeClass( 'fa-plus-square-o' ).addClass( 'fa-minus-square-o' );
                var src = JSON.parse(tr.attributes.object.value);
                $.ajax({
                    url: "danh-sach-don-hang/getPurchaseDetails",
                    type: "post", //send it through get method
                    data: JSON.stringify({
                        order_id: src.id
                    }), contentType: "application/json",
                    success: function (response) {
                        if (response.response_code == "SUCC_EXEC") {
                            var html = ['<table class="table mb-none">'];
                            handle_data(0,response.data,html,src,datatable,tr);


                        }
                    }
                });

            }
        });

    };

    $(function() {
        datatableInit();
    });

}).apply( this, [ jQuery ]);

(function( $ ) {

	'use strict';

	/*
	Basic
	*/
	$('.modal-basic').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Sizes
	*/
	$('.modal-sizes').magnificPopup({
		type: 'inline',
		preloader: false,
		modal: true
	});

	/*
	Modal with CSS animation
	*/
	$('.modal-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		modal: true
	});

	$('.modal-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom',
		modal: true
	});

	/*
	Modal Dismiss
	*/
	$(document).on('click', '.modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	/*
	Modal Confirm
	*/


	/*
	Form
	*/
	$('.modal-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',
		modal: true,

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

	/*
	Ajax
	*/
	$('.simple-ajax-modal').magnificPopup({
		type: 'ajax',
		modal: true
	});

}).apply( this, [ jQuery ]);