function apply_role() {
    $.ajax({
        url: "utility/getUserRole",
        type: "post", //send it through get method
        success: function (response) {
            if (response.response_code == "SUCC_EXEC") {


                var array = response.data.user_role;
                var user_id = response.data.user_id;
                var url = response.data.url;

                // ẩn menu quan ly user neu 0 phai user admin
                if(user_id == "719AD10A-04F9-4FD8-9351-1B2602277967"){
                    return;
                }else {
                    // $("#user-page").remove();
                }
                var full = page_master_group;
                var sub = page_child_group;
                console.log("array "+ JSON.stringify(array));
                for (var index=0;index<array.length;index++){
                    var obj = array[index];
                    var master_group = obj.master_group;
                    var child_group = obj.child_group;
                    var name = obj.name;
                    if(master_group == full && child_group == sub){
                        handle_child_group(name,url,sub);
                        break;
                    }

                }
            }

        },
        error: function (xhr) {
            // nothing
        }
    });
}

function handle_child_group(name,url,sub) {
    var action_them = "Thêm";
    var action_xoa = "Xóa";
    var action_sua = "Sửa";
    var action_xem = "Xem";
    if (name == action_them) {
        $("#btn-add").remove();
        $(".btn-copy").remove();
        if(sub == "Thêm danh mục sản phẩm" ||
            sub == "Quản lý sản phẩm quà tặng" ||
            sub == "Thêm sản phẩm combo" ||
            sub == "Thêm sản phẩm đơn vị" ||
            sub == "Quản lý sản phẩm quà tặng" ||
            sub == "Thêm thương hiệu" ||
            sub == "Thêm CT khuyến mãi" ||
            sub == "Tạo coupon" ||
            sub == "Thêm bài viết" ||
            sub == "Danh sách liên hệ" ||
            sub == "Thêm chi nhánh" ||
            sub == "Thêm user" ||
            sub == "Thêm quyền" ||
            sub == "Script tag"
        ){
            window.location.href = url+"error";
        }
    }else if(name == action_sua){
        $(".btn-edit").remove();
    }else if(name == action_xoa){
        $(".btn-del").remove();
    }else if(name == action_xem){
        window.location.href = url+"error";
    }

}




apply_role();