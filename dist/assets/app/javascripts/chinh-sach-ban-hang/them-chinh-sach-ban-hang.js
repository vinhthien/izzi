function uploadFile(callback)
{
    if($("#file_to_upload").val() != "")
    {
        var file_data = $('#file_to_upload').prop('files')[0];
        var form_data = new FormData();

        form_data.append('file', file_data);

        $.ajax({
            url: 'upload-hinh/', // point to server-side PHP script
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function(data){
                // get server responce here
                data = JSON.parse(data);
                // clear file field
                $("#file_to_upload").val("");
                if(data.response_code == "SUCC_EXEC"){
                    callback(data.data);
                }else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'upload hình ảnh thất bại',
                        type: 'error'
                    });
                }
            }
        });
    }
    else if(image_link!=null && image_link.length>0)
    {
        callback(image_link);
    }else {
        callback("");
    }
}



function add() {
    var tag = $("#ip-tag").val();
    var order = $("#ip-order").val();
    var title_vi = $("#ip-title-vi").val();
    var html_vi = CKEDITOR.instances.editor1.getData();
    var title_en = $("#ip-title-en").val();
    var html_en = CKEDITOR.instances.editor2.getData();

    if (order.length == 0) {
        alert("Chưa thêm thứ tự sắp xếp ");
        return;
    }
    if (tag.length == 0) {
        alert("Chưa thêm nhãn ");
        return;
    }

    if (title_vi.length == 0) {
        alert("Chưa thêm tiêu đề tiếng việt");
        return;
    }
    if (html_vi.length == 0) {
        alert("Chưa thêm HTML tiếng việt");
        return;
    }

    if (title_en.length == 0) {
        title_en = title_vi;
    }
    if (html_en.length == 0) {
        html_en = html_vi;
    }
    var meta_keyword_vi = $("#meta-keyword-vi").val();
    var meta_description_vi = $("#meta-description-vi").val();
    var meta_title_vi = $("#meta-title-vi").val();
    var meta_keyword_en = $("#meta-keyword-en").val();
    var meta_description_en = $("#meta-description-en").val();
    var meta_title_en = $("#meta-title-en").val();




    if (meta_keyword_vi.length == 0) {
        alert("Chưa thêm meta key tiếng việt");
        return;
    }
    if (meta_description_vi.length == 0) {
        alert("Chưa thêm meta description tiếng việt");
        return;
    }
    if (meta_title_vi.length == 0) {
        alert("Chưa thêm meta title tiếng việt");
        return;
    }

    if (meta_keyword_en.length == 0) {
        meta_keyword_en = meta_keyword_vi;
    }
    if (meta_description_en.length == 0) {
        meta_description_en = meta_description_vi;
    }
    if (meta_title_en.length == 0) {
        meta_title_en = meta_title_vi;
    }

    var need_show = $('#show_check').prop( "checked" );
    var banner_link = $("#ip-link-banner").val();
    uploadFile(function (url) {
        $.ajax({
            url: "them-chinh-sach-ban-hang/them",
            type: "post", //send it through get method
            data: JSON.stringify({
                meta_title_vi:meta_title_vi,
                meta_description_vi:meta_description_vi,
                meta_keyword_vi:meta_keyword_vi,
                meta_description_en:meta_description_en,
                meta_keyword_en:meta_keyword_en,
                meta_title_en:meta_title_en,
                tag: tag,
                order: order,
                title_vi: title_vi,
                html_vi: html_vi,
                title_en: title_en,
                html_en: html_en,
                image: url,
                need_show: need_show,
                banner_link: banner_link,
                show_banner: 1

            }), contentType: "application/json",

            success: function (response) {
                if (response.response_code == "SUCC_EXEC") {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm bài viết [' + title_vi + "] thành công",
                        type: 'success'
                    });
                    window.location.href = "sua-chinh-sach-ban-hang?id="+response.data.id;
                } else {
                    new PNotify({
                        title: 'Thông báo',
                        text: 'Thêm bài viết [' + title_vi + "] thất bại\n" + response.description,
                        type: 'error'
                    });
                }

            },
            error: function (xhr) {
                //Do Something to handle error
                new PNotify({
                    title: 'Thông báo',
                    text: 'Thêm bài viết [' + title_vi + "] thất bại",
                    type: 'error'
                });
            }
        });
    });

}
