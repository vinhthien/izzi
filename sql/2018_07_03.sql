INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('1155a020-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Giá trị cốt lõi của OPERA', 'Giới thiệu', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES
('6b68592c-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Giá trị cốt lõi của OPERA', 'Giới thiệu', NULL, NULL, NULL, NULL, NULL),

('8326ee70-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Tầm nhìn - Sứ Mệnh - Chính sách chất lượng', 'Giới thiệu', NULL, NULL, NULL, NULL, NULL),
('b66b44b6-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Tầm nhìn - Sứ Mệnh - Chính sách chất lượng', 'Giới thiệu', NULL, NULL, NULL, NULL, NULL),

('b66b4984-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Thông tin liên lạc công ty', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),
('b66b4ab0-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Thông tin liên lạc công ty', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),

('b66b4bd2-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý danh mục cấp 2 footer', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),
('b66b4fce-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý danh mục cấp 2 footer', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),
('b66b5104-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý danh mục cấp 2 footer', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),
('b66b521c-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Quản lý danh mục cấp 2 footer', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),

('b66b5334-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm slide hình', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),

('b66b544c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách slide hình', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),
('b66b5564-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách slide hình', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),

('b66b5852-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách slide hình', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),
('13f38a5c-7e81-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách slide hình', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),

('b66b597e-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý meta trang chủ', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),
('b66b5aa0-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý meta trang chủ', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),

('b66b5bae-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý tốc độ slider trang chủ', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),
('b66b5cc6-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý tốc độ slider trang chủ', 'Quản lý trang chủ', NULL, NULL, NULL, NULL, NULL),

('b66b5dd4-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm bài viết', 'Quản lý chính sách bán hàng', NULL, NULL, NULL, NULL, NULL),

('b66b613a-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách bài viết', 'Quản lý chính sách bán hàng', NULL, NULL, NULL, NULL, NULL),
('b66b6266-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách bài viết', 'Quản lý chính sách bán hàng', NULL, NULL, NULL, NULL, NULL),
('b66b6374-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách bài viết', 'Quản lý chính sách bán hàng', NULL, NULL, NULL, NULL, NULL),
('b66b648c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách bài viết', 'Quản lý chính sách bán hàng', NULL, NULL, NULL, NULL, NULL),

('b66b65a4-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm danh mục sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b66bc-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách danh mục sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b6a54-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách danh mục sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b6bb2-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách danh mục sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b6cca-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách danh mục sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b6de2-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm sản phẩm đơn vị', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b6efa-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách sản phẩm đơn vị', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b7012-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách sản phẩm đơn vị', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b712a-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách sản phẩm đơn vị', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b7e36-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách sản phẩm đơn vị', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b7fa8-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm sản phẩm combo', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b819c-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách sản phẩm combo', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b82d2-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách sản phẩm combo', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b83ea-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách sản phẩm combo', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b8502-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách sản phẩm combo', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b882c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý sản phẩm quà tặng', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b8980-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý sản phẩm quà tặng', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b8aa2-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý sản phẩm bán chạy', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b8bb0-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý sản phẩm bán chạy', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b8cc8-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý meta tổng quan sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b8de0-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý meta tổng quan sản phẩm', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b8eee-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm thương hiệu', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b92fe-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý thương hiệu', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b942a-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Quản lý thương hiệu', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b9542-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý thương hiệu', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),
('b66b965a-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý thương hiệu', 'Quản lý sản phẩm', NULL, NULL, NULL, NULL, NULL),

('b66b9998-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm CT khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66b9b28-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách CT khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66b9c72-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách CT khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66b9d8a-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách CT khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66b9eac-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách CT khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66b9fc4-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Tạo coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66ba38e-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách CT Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66ba4ce-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách CT Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66ba5fa-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách CT Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66ba71c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách CT Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66ba834-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách tất cả Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66ba942-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách tất cả Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66bac8a-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách tất cả Coupon', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66badca-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý meta tổng quan khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66baeec-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý meta tổng quan khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66bb004-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Cập nhật banner tổng quan', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66bb126-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Cập nhật banner tổng quan', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66bb23e-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý sản phẩm khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66bb54a-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý sản phẩm khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66bb68a-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý Popup khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),
('b66bb7a2-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý Popup khuyến mãi', 'Quản lý khuyến mãi', NULL, NULL, NULL, NULL, NULL),

('b66bb8ba-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm bài viết', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bb9d2-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách bài viết', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bbae0-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách bài viết', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bbbf8-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách bài viết', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bbf54-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách bài viết', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bc08a-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Cập nhật banner tổng quan', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bc1f2-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Cập nhật banner tổng quan', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bc33c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý meta tổng quan tin tức', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bc45e-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý meta tổng quan tin tức', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bc576-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý bài viết ở top', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bc7ba-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Quản lý bài viết ở top', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bc8dc-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý bài viết ở top', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bc9f4-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý tốc độ slider ở top', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),
('b66bcb0c-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý tốc độ slider ở top', 'Quản lý tin tức sự kiện', NULL, NULL, NULL, NULL, NULL),

('b66bcc1a-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm bài viết', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66bcd32-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách bài viết', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bce40-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách bài viết', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bd4a8-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách bài viết', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bd61a-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách bài viết', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66bd746-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Cập nhật banner tổng quan', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bd868-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Cập nhật banner tổng quan', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66bd98a-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý meta tổng quan món ngon', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bdaa2-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý meta tổng quan món ngon', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66bdbe2-7e70-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Quản lý bài viết ở top', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66bdee4-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý bài viết ở top', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66be0a6-7e70-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Quản lý bài viết ở top', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66be1e6-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý tốc độ slider ở top', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),
('b66be2fe-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý tốc độ slider ở top', 'Quản lý món ăn ngon', NULL, NULL, NULL, NULL, NULL),

('b66be416-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Chỉnh sửa liên hệ', 'Quản lý liên hệ', NULL, NULL, NULL, NULL, NULL),
('b66be52e-7e70-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Chỉnh sửa liên hệ', 'Quản lý liên hệ', NULL, NULL, NULL, NULL, NULL),

('b66be63c-7e70-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách liên hệ', 'Quản lý liên hệ', NULL, NULL, NULL, NULL, NULL),

('13f337e6-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Quản lý liên hệ footer', 'Quản lý liên hệ', NULL, NULL, NULL, NULL, NULL),
('13f33d0e-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Quản lý liên hệ footer', 'Quản lý liên hệ', NULL, NULL, NULL, NULL, NULL),

('13f33e76-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Cấu hình email', 'Soạn thảo Email', NULL, NULL, NULL, NULL, NULL),
('13f33fac-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Cấu hình email', 'Soạn thảo Email', NULL, NULL, NULL, NULL, NULL),

('13f340ce-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Email xác nhận đã đăng ký', 'Soạn thảo Email', NULL, NULL, NULL, NULL, NULL),
('13f341e6-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Email xác nhận đã đăng ký', 'Soạn thảo Email', NULL, NULL, NULL, NULL, NULL),

('13f34416-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Email xác nhận đã đăng ký', 'Soạn thảo Email', NULL, NULL, NULL, NULL, NULL),

('13f347d6-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),

('13f3490c-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),
('13f34a38-7e81-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),
('13f34b5a-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),
('13f34c72-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),

('13f34d8a-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách khách hàng thân thiết', 'Quản lý khách hàng thân thiết', NULL, NULL, NULL, NULL, NULL),

('13f35082-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Kho hàng', 'Quản lý kho', NULL, NULL, NULL, NULL, NULL),

('13f351b8-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách đơn hàng', 'Quản lý đơn hàng', NULL, NULL, NULL, NULL, NULL),

('13f352da-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Báo cáo bán hàng', 'Báo cáo', NULL, NULL, NULL, NULL, NULL),

('13f353fc-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Thống kê bán hàng tổng hợp', 'Báo cáo', NULL, NULL, NULL, NULL, NULL),

('13f35514-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Báo cáo doanh số theo ngành hàng', 'Báo cáo', NULL, NULL, NULL, NULL, NULL),

('13f35898-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Thống kê bán hàng chi tiết', 'Báo cáo', NULL, NULL, NULL, NULL, NULL),

('13f35a78-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Báo cáo bán hàng đầy đủ', 'Báo cáo', NULL, NULL, NULL, NULL, NULL),

('13f35c1c-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm user', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),

('13f35d48-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách user', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f35e6a-7e81-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách user', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f35fe6-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách user', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f363ba-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách user', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),

('13f36874-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Thêm quyền', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),

('13f369be-7e81-11e8-adc0-fa7ae01bbebc', 'Thêm', NULL, 'Danh sách quyền', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f36ae0-7e81-11e8-adc0-fa7ae01bbebc', 'Xóa', NULL, 'Danh sách quyền', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f36c02-7e81-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Danh sách quyền', 'Quản lý user', NULL, NULL, NULL, NULL, NULL),
('13f36d24-7e81-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Danh sách quyền', 'Quản lý user', NULL, NULL, NULL, NULL, NULL);

DELETE FROM `permission` WHERE `master_group` = 'Business' OR `master_group` = 'CMS';

UPDATE `permission` SET `sort_order` = 1 WHERE `master_group` = "Giới thiệu";
UPDATE `permission` SET `sort_order` = 2 WHERE `master_group` = "Quản lý footer";
UPDATE `permission` SET `sort_order` = 3 WHERE `master_group` = "Quản lý trang chủ";
UPDATE `permission` SET `sort_order` = 4 WHERE `master_group` = "Quản lý chính sách bán hàng";
UPDATE `permission` SET `sort_order` = 5 WHERE `master_group` = "Quản lý sản phẩm";
UPDATE `permission` SET `sort_order` = 6 WHERE `master_group` = "Quản lý khuyến mãi";
UPDATE `permission` SET `sort_order` = 7 WHERE `master_group` = "Quản lý tin tức sự kiện";
UPDATE `permission` SET `sort_order` = 8 WHERE `master_group` = "Quản lý món ăn ngon";
UPDATE `permission` SET `sort_order` = 9 WHERE `master_group` = "Quản lý liên hệ";
UPDATE `permission` SET `sort_order` = 10 WHERE `master_group` = "Soạn thảo Email";
UPDATE `permission` SET `sort_order` = 11 WHERE `master_group` = "Quản lý chi nhánh (để sau)";
UPDATE `permission` SET `sort_order` = 12 WHERE `master_group` = "Quản lý khách hàng thân thiết";
UPDATE `permission` SET `sort_order` = 13 WHERE `master_group` = "Quản lý kho";
UPDATE `permission` SET `sort_order` = 14 WHERE `master_group` = "Quản lý đơn hàng";
UPDATE `permission` SET `sort_order` = 15 WHERE `master_group` = "Báo cáo";
UPDATE `permission` SET `sort_order` = 16 WHERE `master_group` = "Quản lý user";

UPDATE `permission` SET `child_group` = 'Cập nhật banner tổng quan' WHERE `permission`.`id` = 'b66b5104-7e70-11e8-adc0-fa7ae01bbebc'; UPDATE `permission` SET `child_group` = 'Cập nhật banner tổng quan' WHERE `permission`.`id` = 'b66b521c-7e70-11e8-adc0-fa7ae01bbebc';

UPDATE `permission` SET `name` = 'Sửa' WHERE `permission`.`id` = 'b66b5104-7e70-11e8-adc0-fa7ae01bbebc';

UPDATE `permission` SET `name` = 'Xem' WHERE `permission`.`id` = 'b66b521c-7e70-11e8-adc0-fa7ae01bbebc';

UPDATE `permission` SET `child_group` = 'Chính sách bán hàng' WHERE `permission`.`id` = 'b66b5104-7e70-11e8-adc0-fa7ae01bbebc'; UPDATE `permission` SET `child_group` = 'Chính sách bán hàng' WHERE `permission`.`id` = 'b66b521c-7e70-11e8-adc0-fa7ae01bbebc';

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('f8bbb22c-7ea4-11e8-adc0-fa7ae01bbebc', 'Sửa', NULL, 'Cập nhật banner tổng quan', 'Quản lý sản phẩm', '5', NULL, NULL, NULL, NULL), ('f8bbb4b6-7ea4-11e8-adc0-fa7ae01bbebc', 'Xem', NULL, 'Cập nhật banner tổng quan', 'Quản lý sản phẩm', '5', NULL, NULL, NULL, NULL);
