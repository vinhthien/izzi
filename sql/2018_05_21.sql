ALTER TABLE `Config` CHANGE `value` `value` LONGTEXT NULL ;

INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'image-library-json', '[]');

INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'front_end_call_number_footer', NULL), (NULL, 'front_end_sms_number_footer', NULL);

INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'front_end_map_longitude_footer', NULL), (NULL, 'front_end_map_latitude_footer', NULL);


ALTER TABLE `ProductCategory` ADD `banner_image` TEXT NULL;

