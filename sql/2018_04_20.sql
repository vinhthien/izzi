

ALTER TABLE `PurchaseOrder`  ADD `fullname` VARCHAR(250) NULL  AFTER `code`,  ADD `email` VARCHAR(100) NULL  AFTER `fullname`,  ADD `phone` VARCHAR(50) NULL  AFTER `email`,  ADD `delivery_different` int NULL  AFTER `phone`;


INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `countview`) VALUES
('f0bde88c-444e-11e8-842f-0ed5f89f718b', 'AAA', NULL, NULL, 'admin', '2018-04-12 08:49:03', 'Quan ly', '', '20145F90-55BD-4475-9B7F-A915AC5C9A06', NULL, 'quan ly', NULL, 1, 3, NULL, 'category2.png', NULL);

INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('b316d1ba-45e8-11e8-842f-0ed5f89f718b', '', 'Liên lạc công ty', '', '<p>lien lac</p>', 'admin', 'admin', '2018-04-11 22:05:36', '2018-04-12 08:54:09', 'AAA', 'AAA', '1', 'vi', NULL, NULL, NULL, NULL, 'f0bde88c-444e-11e8-842f-0ed5f89f718b');
INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('b316db42-45e8-11e8-842f-0ed5f89f718b', '', 'Liên lạc công ty', '', '<p>lien lac</p>', 'admin', 'admin', '2018-04-11 22:05:36', '2018-04-12 08:54:09', 'AAA', 'AAA', '1', 'en', NULL, NULL, NULL, NULL, 'f0bde88c-444e-11e8-842f-0ed5f89f718b');

ALTER TABLE `PurchaseOrder` ADD `payment_method` int NULL COMMENT '1:COD,2:The ATM,3:Quoc te';

ALTER TABLE `PurchaseOrder` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '1: New; 2: Confirmed; 3: Delivered; 4: Done; 5: Not ready;6: Paying' ;


ALTER TABLE `PurchaseOrderDetail`
ADD PRIMARY KEY (`id`);