ALTER TABLE `purchaseorderdetail` CHANGE `promotion_id` `promotion_id` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `purchaseorder` CHANGE `coupon_promotion_name` `coupon_promotion_name` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `purchaseorder` CHANGE `coupon_promotion_id` `coupon_promotion_id` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;