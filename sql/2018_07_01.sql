INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('b31916e8-7ceb-11e8-adc0-fa7ae01bbebc', 'Thêm tài khoản', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('44653578-7cec-11e8-adc0-fa7ae01bbebc', 'Sửa tài khoản', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('4a2e3554-7cec-11e8-adc0-fa7ae01bbebc', 'Xoá tài khoản', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);


INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('4ee08626-7d8a-11e8-adc0-fa7ae01bbebc', 'Thêm quyền', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('57d958de-7d8a-11e8-adc0-fa7ae01bbebc', 'Sửa quyền', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('5fdab5f0-7d8a-11e8-adc0-fa7ae01bbebc', 'Xoá quyền', NULL, 'Quản lý tài khoản', 'CMS', '1', '2018-06-20 00:00:00', 'admin', NULL, NULL);

INSERT INTO `userrole` (`id`, `role_id`, `user_id`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('7134ac1a-7d90-11e8-adc0-fa7ae01bbebc', '78368c7c-739f-11e8-adc0-fa7ae01bbebc', '719AD10A-04F9-4FD8-9351-1B2602277967', '2018-06-29 08:09:29', 'admin', NULL, NULL);

UPDATE `userrole` SET `role_id` = '783688d0-739f-11e8-adc0-fa7ae01bbebc' WHERE `userrole`.`id` = '7134ac1a-7d90-11e8-adc0-fa7ae01bbebc';