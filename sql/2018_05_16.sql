ALTER TABLE `ProductCategory` ADD `footer` INT NOT NULL AFTER `image`;

ALTER TABLE `ProductCategory` CHANGE `footer` `footer` INT(11) NOT NULL COMMENT '0: not in footer; 1: in footer';