ALTER TABLE User
ADD COLUMN `socket_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE Notification
ADD COLUMN `unread` int(11) COLLATE utf8_unicode_ci NOT NULL;