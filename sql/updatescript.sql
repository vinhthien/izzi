INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `countview`) VALUES
('2CD0E485-122F-490C-866D-37BC3F4234A6', 'GTU', NULL, NULL, 'admin', '2018-04-12 08:49:03', 'Chính sách chất lượng Opera', '', '20145F90-55BD-4475-9B7F-A915AC5C9A06', NULL, '<p>1. Opera coi kh&aacute;ch h&agrave;ng l&agrave; trung t&acirc;m, phục vụ kh&aacute;ch h&agrave;ng l&agrave; sứ mệnh.</p>\n\n<p>2. Opera lu&ocirc;n thỏa m&atilde;n v&agrave; c&oacute; tr&aacute;ch nhiệm với kh&aacute;ch h&agrave;ng.</p>\n\n<p>3. Opera kinh doanh theo văn h&oacute;a B&aacute;t Trọng v&agrave; tu&acirc;n theo Luật định.</p>\n', NULL, 1, 3, NULL, 'category2.png', NULL),
('6659A28A-7A30-4CC6-95D6-7ADC415AD5B7', 'GTU', NULL, NULL, 'admin', '2018-04-12 08:51:39', 'Sứ mệnh Opera', '', '20145F90-55BD-4475-9B7F-A915AC5C9A06', NULL, '<p>Opera cam kết phục vụ cộng đồng với nguồn thực phẩm chất lượng cao cấp h&agrave;ng đầu, bằng tất cả sự tr&acirc;n trọng v&agrave; tr&aacute;ch nhiệm cao của m&igrave;nh với cuộc sống con người v&agrave; x&atilde; hội.</p>\n', NULL, 1, 2, NULL, 'category2.png', NULL),
('8D8FC659-006D-4BDF-A93F-895EFE3DFFE6', 'GTU', NULL, NULL, 'admin', '2018-04-12 09:04:02', 'Công ty cổ phần Opera', '', '20145F90-55BD-4475-9B7F-A915AC5C9A06', NULL, '<p>Th&agrave;nh lập từ năm 2011, C&ocirc;ng ty cổ phần Opera l&agrave; một trong những nh&agrave; nhập khẩu v&agrave; ph&acirc;n phối nguy&ecirc;n liệu thực phẩm h&agrave;ng đầu tại Việt Nam. Ch&uacute;ng t&ocirc;i tự h&agrave;o mang đến cho tất cả c&aacute;c Kh&aacute;ch H&agrave;ng trong ng&agrave;nh C&ocirc;ng nghiệp thực phẩm, C&ocirc;ng nghiệp lưu tr&uacute; - ẩm thực v&agrave; Nh&agrave; b&aacute;n lẻ những chủng loại thực phẩm nhập khẩu chất lượng từ c&aacute;c c&ocirc;ng ty thực phẩm h&agrave;ng đầu thế giới. Hệ thống ph&acirc;n phối đồng nhất đạt chứng nhận chất lượng được vận h&agrave;nh ở c&aacute;c chi nh&aacute;nh đảm bảo h&agrave;ng h&oacute;a ph&acirc;n phối ổn định nhanh ch&oacute;ng v&agrave; đ&uacute;ng chất lượng.</p>\n', NULL, 1, 0, NULL, 'category2.png', NULL),
('C486892A-8FF7-4337-96C8-C11A59B9C586', 'GTU', NULL, NULL, 'admin', '2018-04-12 08:51:53', 'Tầm nhìn Opera', '', '20145F90-55BD-4475-9B7F-A915AC5C9A06', NULL, '<p>Opera l&agrave; biểu tượng niềm tin về chất lượng v&agrave; l&agrave; thương hiệu h&agrave;ng đầu tại Việt Nam về thực phẩm cao cấp của thế giới.</p>\n', NULL, 1, 1, NULL, 'category1.png', NULL);

INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `requiring_id`) VALUES
('6c0c97f0-3d9b-11e8-b4f2-199023609d1b', '', 'Công ty cổ phần Opera', '', '<p>Th&agrave;nh lập từ năm 2011, C&ocirc;ng ty cổ phần Opera l&agrave; một trong những nh&agrave; nhập khẩu v&agrave; ph&acirc;n phối nguy&ecirc;n liệu thực phẩm h&agrave;ng đầu tại Việt Nam. Ch&uacute;ng t&ocirc;i tự h&agrave;o mang đến cho tất cả c&aacute;c Kh&aacute;ch H&agrave;ng trong ng&agrave;nh C&ocirc;ng nghiệp thực phẩm, C&ocirc;ng nghiệp lưu tr&uacute; - ẩm thực v&agrave; Nh&agrave; b&aacute;n lẻ những chủng loại thực phẩm nhập khẩu chất lượng từ c&aacute;c c&ocirc;ng ty thực phẩm h&agrave;ng đầu thế giới. Hệ thống ph&acirc;n phối đồng nhất đạt chứng nhận chất lượng được vận h&agrave;nh ở c&aacute;c chi nh&aacute;nh đảm bảo h&agrave;ng h&oacute;a ph&acirc;n phối ổn định nhanh ch&oacute;ng v&agrave; đ&uacute;ng chất lượng.</p>\n', 'admin', 'admin', '2018-04-11 22:17:17', '2018-04-12 09:04:02', 'GTU', 'GTU', 1, 'vi', NULL, NULL, NULL, '8D8FC659-006D-4BDF-A93F-895EFE3DFFE6'),
('6c0da960-3d9b-11e8-b4f2-199023609d1b', '', 'Opera joint stock company', '', '<p>Established in 2011, Opera Corporation is one of the leading importers and distributors of food ingredients in Vietnam. We are proud to bring all our customers in the Food Industry, Hospitality Industry - Food and Retail to quality imported food from the world&#39;s leading food companies. Uniform quality certification system is operated at branches to ensure stable and fast delivery of goods.</p>\n', 'admin', 'admin', '2018-04-11 22:17:17', '2018-04-12 09:04:02', 'GTU', 'GTU', 1, 'en', NULL, NULL, NULL, '8D8FC659-006D-4BDF-A93F-895EFE3DFFE6'),
('753f1840-3d99-11e8-a223-8b6e46b6a559', '', 'Tầm nhìn', '', '<p>Opera l&agrave; biểu tượng niềm tin về chất lượng v&agrave; l&agrave; thương hiệu h&agrave;ng đầu tại Việt Nam về thực phẩm cao cấp của thế giới.</p>\n', 'admin', 'admin', '2018-04-11 22:03:14', '2018-04-12 08:51:53', 'GTU', 'GTU', 1, 'vi', NULL, NULL, NULL, 'C486892A-8FF7-4337-96C8-C11A59B9C586'),
('753fb480-3d99-11e8-a223-8b6e46b6a559', '', 'Vision', '', '<p>Opera is a symbol of quality and is a leading brand in Vietnam for high-end food in the world.</p>\n', 'admin', 'admin', '2018-04-11 22:03:14', '2018-04-12 08:51:53', 'GTU', 'GTU', 1, 'en', NULL, NULL, NULL, 'C486892A-8FF7-4337-96C8-C11A59B9C586'),
('9f7f5850-3d93-11e8-bd01-41c82c0a0a2c', '', 'Sứ mệnh', '', '<p>1. Opera coi kh&aacute;ch h&agrave;ng l&agrave; trung t&acirc;m, phục vụ kh&aacute;ch h&agrave;ng l&agrave; sứ mệnh.</p>\n\n<p>2. Opera lu&ocirc;n thỏa m&atilde;n v&agrave; c&oacute; tr&aacute;ch nhiệm với kh&aacute;ch h&agrave;ng.</p>\n\n<p>3. Opera kinh doanh theo văn h&oacute;a B&aacute;t Trọng v&agrave; tu&acirc;n theo Luật định.</p>\n', 'admin', 'admin', '2018-04-11 21:21:28', '2018-04-12 08:49:03', 'GTU', 'GTU', 1, 'vi', NULL, NULL, NULL, '6659A28A-7A30-4CC6-95D6-7ADC415AD5B7'),
('9f80def0-3d93-11e8-bd01-41c82c0a0a2c', '', 'Mission', '', '<p>1. Opera is considered customer-centric, serving customers as a mission.</p>\n\n<p>2. Opera is responsive and responsive to customers.</p>\n\n<p>3. Opera business according to Bat Trong culture and obey the Law.</p>\n', 'admin', 'admin', '2018-04-11 21:21:28', '2018-04-12 08:49:03', 'GTU', 'GTU', 1, 'en', NULL, NULL, NULL, '6659A28A-7A30-4CC6-95D6-7ADC415AD5B7'),
('c9ec0920-3d99-11e8-a223-8b6e46b6a559', '', 'Chính sách chất lượng', '', '<p>Opera cam kết phục vụ cộng đồng với nguồn thực phẩm chất lượng cao cấp h&agrave;ng đầu, bằng tất cả sự tr&acirc;n trọng v&agrave; tr&aacute;ch nhiệm cao của m&igrave;nh với cuộc sống con người v&agrave; x&atilde; hội.</p>\n', 'admin', 'admin', '2018-04-11 22:05:36', '2018-04-12 09:15:09', 'GTU', 'GTU', 1, 'vi', NULL, NULL, NULL, '2CD0E485-122F-490C-866D-37BC3F4234A6'),
('c9ec7e50-3d99-11e8-a223-8b6e46b6a559', '', 'Quality policy', '', '<p>Opera is committed to serving the community with top quality food, with all due respect and high responsibility for human and social life.</p>\n', 'admin', 'admin', '2018-04-11 22:05:36', '2018-04-12 09:15:09', 'GTU', 'GTU', 1, 'en', NULL, NULL, NULL, '2CD0E485-122F-490C-866D-37BC3F4234A6');

-- update script 001
ALTER TABLE `ProductCategory` ADD `parent_id` VARCHAR(50) NULL AFTER `sort_order`;
ALTER TABLE `ProductCategory` ADD `image` TEXT NULL AFTER `parent_id`;
ALTER TABLE `DataDirectory` ADD `requiring_id` VARCHAR(50) NULL AFTER `meta_title`;


-- update admin password
UPDATE `User` SET `password` = 'e10adc3949ba59abbe56e057f20f883e' WHERE `User`.`id` = '719AD10A-04F9-4FD8-9351-1B2602277967';





--
-- Table structure for table `Contact`
--

CREATE TABLE `Contact` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `name` text COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Contact`
--
ALTER TABLE `Contact`
  ADD PRIMARY KEY (`id`);


-- Table structure for table `Notification`
--

CREATE TABLE `Notification` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `icon` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` text COLLATE utf8_unicode_ci,
  `is_email` bit(1) DEFAULT b'0',
  `is_notification` bit(1) DEFAULT b'0',
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `requiring_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiring_entity_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Notification`
--
ALTER TABLE `Notification`
  ADD PRIMARY KEY (`id`);




--
-- Table structure for table `ProductWishList`
--

CREATE TABLE `ProductWishList` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiring_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiring_entity_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `note` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ProductWishList`
--
ALTER TABLE `ProductWishList`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `Slider` ADD PRIMARY KEY( `id`);

ALTER TABLE `PromotionDetail` ADD `entity_code` VARCHAR(50) NULL AFTER `status`;

ALTER TABLE `Promotion` CHANGE `type` `type` INT(11) NULL DEFAULT NULL COMMENT '1: Fixed price; 2: percentage; 3: gift; 4: coupon';
ALTER TABLE `PurchaseOrder` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '1: New; 2: Confirmed; 3: Delivered; 4: Done';

ALTER TABLE `User` CHANGE `socket_id` `socket_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `WarehouseHistory` CHANGE `type` `type` INT(11) NULL DEFAULT NULL COMMENT '1: Nhap kho; 2: Xuat kho; 3: Khoi tao';



ALTER TABLE `DataDirectory` ADD `url` TEXT NULL AFTER `meta_title`;

ALTER TABLE `PurchaseOrder` ADD `code_coupon` TEXT NULL AFTER `note`;
ALTER TABLE `PurchaseOrder` ADD `price_coupon` DECIMAL(18,2) NULL AFTER `note`;
ALTER TABLE `PurchaseOrder` ADD `percent_promotion` DECIMAL(18,2) NULL AFTER `note`;

ALTER TABLE `PurchaseOrderDetail` ADD `code_promotion` TEXT NULL AFTER `status`;
ALTER TABLE `PurchaseOrderDetail` ADD `price_promotion` DECIMAL(18,2) NULL AFTER `status`;
ALTER TABLE `PurchaseOrderDetail` ADD `percent_promotion` DECIMAL(18,2) NULL AFTER `status`;
ALTER TABLE `PurchaseOrderDetail` ADD `is_promotion` bit NULL AFTER `status`;

UPDATE `Content` SET `content_category_id` = NULL WHERE `Content`.`id` = 'f0bde88c-444e-11e8-842f-0ed5f89f718b';

ALTER TABLE `Product` ADD `type` INT NOT NULL DEFAULT '1' AFTER `status`;
ALTER TABLE `Product` CHANGE `type` `type` INT(11) NOT NULL DEFAULT '1' COMMENT '1:normal; 2:Thit bo nhap khau; 3: Thit heo nhap khau; 4: thuc pham dong lanh; 5: san pham tieu dung ';


ALTER TABLE `Promotion` CHANGE `type` `type` INT(11) NULL DEFAULT NULL COMMENT '1: Fixed price; 2: percentage; 3: gift;';

ALTER TABLE `Promotion` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '1: bình thường/chưa dùng; 2: Đã sử dụng hết';

ALTER TABLE `Promotion` CHANGE `coupon` `coupon` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;


INSERT INTO `ContentCategory` (`id`, `name`, `title`, `description`, `contentHTML`, `parent_id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `status`) VALUES ('65b136dc-5122-11e8-9c2d-fa7ae01bbebc', 'Tin tức và sự kiện', 'Tin tức và sự kiện', 'Tin tức và sự kiện', 'Tin tức và sự kiện', NULL, 'CTS', '2018-04-09 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', '719AD10A-04F9-4FD8-9351-1B2602277967', '2018-04-09 00:00:00', '1');


INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `image_large`, `countview`) VALUES ('6069c602-5132-11e8-9c2d-fa7ae01bbebc', 'ETK', '2018-04-12 00:00:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, 'slider5.png', NULL, NULL);

INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('87d4e806-5133-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'vi', 'key', 'des', 'title', NULL, 'b98b9d2c-5133-11e8-9c2d-fa7ae01bbebc'), ('aaf2b458-5133-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'en', 'key', 'des', 'title', NULL, 'b98b9d2c-5133-11e8-9c2d-fa7ae01bbebc');


UPDATE `ProductCategory` SET `parent_id` = NULL WHERE `ProductCategory`.`id` = '9291ed3a-4f48-11e8-9c2d-fa7ae01bbebc';

UPDATE `ProductCategory` SET `contentHTML` = 'Quà tặng' WHERE `ProductCategory`.`id` = '9291ed3a-4f48-11e8-9c2d-fa7ae01bbebc';

INSERT INTO `User` (`id`, `user_name`, `password`, `is_confirm`, `type`, `login_fail`, `last_login`, `is_locked`, `created_date`, `updated_date`, `created_by`, `updated_by`, `entity_code`, `socket_id`) VALUES ('a23c09e8-5329-11e8-9c2d-fa7ae01bbebc', 'operamart', 'e10adc3949ba59abbe56e057f20f883e', b'1', '1', '0', '2018-04-09 00:00:00', b'0', '2018-04-09 00:00:00', '2018-04-09 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', '719AD10A-04F9-4FD8-9351-1B2602277967', 'PER', 'nC-raZDDHzvC--hGAABu');

UPDATE `user` SET `password` = 'd7d7210ba9ea0feb97d1ab5317521336' WHERE `user`.`id` = 'a23c09e8-5329-11e8-9c2d-fa7ae01bbebc';