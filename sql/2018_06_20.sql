ALTER TABLE `permission` CHANGE `id` `id` VARCHAR(50) NOT NULL;

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('dfd3da8a-7447-11e8-adc0-fa7ae01bbebc', 'Thêm bài viết', NULL, 'Thông tin chung', 'CMS', '1', '2018-06-20 00:00:00', 'duc', NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('dfd3def4-7447-11e8-adc0-fa7ae01bbebc', 'Sửa bài viết', NULL, 'Thông tin chung', 'CMS', '2', '2018-06-20 00:00:00', 'duc', NULL, NULL), ('dfd3e052-7447-11e8-adc0-fa7ae01bbebc', 'Xóa bài viết', NULL, 'Thông tin chung', 'CMS', '3', '2018-06-20 00:00:00', 'duc', NULL, NULL);

ALTER TABLE `userrole` ADD PRIMARY KEY(`id`);

ALTER TABLE `productcategory` ADD link TEXT;