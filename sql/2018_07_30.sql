INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `direct_route`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('ea692298-93d2-11e8-9eb6-529269fb1459', 'Xem', NULL, 'Danh sách khách hàng thân thiết', 'Quản lý khách hàng thân thiết', 'danh-sach-khach-hang-than-thiet', '1', NULL, NULL, NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `direct_route`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('c659a480-93d3-11e8-9eb6-529269fb1459', 'Xem', NULL, 'Script tag', 'Script', 'insert-script-tag', '20', NULL, NULL, NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `direct_route`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('70208a2e-93d4-11e8-9eb6-529269fb1459', 'Sửa', NULL, 'Script tag', 'Script', 'cong-ty-co-phan-opera', '20', NULL, NULL, NULL, NULL);

UPDATE `permission` SET `child_group` = 'Script head tag' WHERE `permission`.`id` = '70208a2e-93d4-11e8-9eb6-529269fb1459'; UPDATE `permission` SET `child_group` = 'Script head tag' WHERE `permission`.`id` = 'c659a480-93d3-11e8-9eb6-529269fb1459';

INSERT INTO `config` (`id`, `name`, `value`) VALUES (NULL, 'ship-time-text', 'aaa');

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `direct_route`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES ('aa760742-93e5-11e8-9eb6-529269fb1459', 'Sửa', NULL, 'Script edit', 'Ship', 'insert-ship-time', '22', NULL, NULL, NULL, NULL), ('aa760b34-93e5-11e8-9eb6-529269fb1459', 'Xem', NULL, 'Script edit', 'Ship', 'insert-ship-time', '22', NULL, NULL, NULL, NULL);
