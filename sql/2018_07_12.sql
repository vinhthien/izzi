CREATE TABLE script (
    id varchar(50) NOT NULL PRIMARY KEY,
    script TEXT,
    requiring_entity_code varchar(50),
    created_by varchar(50),
    created_date datetime,
    status	int(11)
);

INSERT INTO script (id, script, requiring_entity_code, created_by, created_date, status)
VALUES ('43a29440-857b-11e8-badb-f9272f11531e'
, 'script.', 'SHT', 'admin','2018-07-12 09:28:29','1');