INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'home-slider-main-speed', '10000');

ALTER TABLE `PromotionDetail` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '0: deleted,1: created_not_used,2:de_activated,3:used';

ALTER TABLE `Promotion` CHANGE `status` `status` INT(11) NULL DEFAULT NULL COMMENT '0: deleted,1: created_not_used,2:de_activated';

