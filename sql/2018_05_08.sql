CREATE TABLE `PurchaseOrderDetailGift` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_order_detail_id` varchar(50) COLLATE utf8_unicode_ci,
  `product_gift_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `PurchaseOrderDetailGift`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `Promotion` ADD `coupon_type` int(11) NULL;
