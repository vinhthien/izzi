INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `countview`) VALUES ('d90efbe6-4e7f-11e8-9c2d-fa7ae01bbebc', NULL, '2018-04-27 00:00:00', 'admin', NULL, NULL, 'mon ngon general', NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL);


INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('d90effba-4e7f-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'vi', 'key', 'des', 'title', NULL, 'd90efbe6-4e7f-11e8-9c2d-fa7ae01bbebc'), ('d90f026c-4e7f-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'en', 'key', 'des', 'title', NULL, 'd90efbe6-4e7f-11e8-9c2d-fa7ae01bbebc');



INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `countview`) VALUES ('4cc75d0a-4e83-11e8-9c2d-fa7ae01bbebc', NULL, '2018-04-27 00:00:00', 'admin', NULL, NULL, 'danh muc san pham general', NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL);


INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('4cc75fbc-4e83-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'vi', 'key', 'des', 'title', NULL, '4cc75d0a-4e83-11e8-9c2d-fa7ae01bbebc'), ('4cc763f4-4e83-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'en', 'key', 'des', 'title', NULL, '4cc75d0a-4e83-11e8-9c2d-fa7ae01bbebc');



INSERT INTO `Content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `countview`) VALUES ('1d3c3224-4e8b-11e8-9c2d-fa7ae01bbebc', NULL, '2018-04-27 00:00:00', 'admin', NULL, NULL, 'khuyen mai general', NULL, NULL, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL);


INSERT INTO `DataDirectory` (`id`, `name`, `title`, `description`, `contentHTML`, `created_by`, `updated_by`, `created_date`, `updated_date`, `entity_code`, `requiring_entity_code`, `status`, `language`, `meta_keywords`, `meta_description`, `meta_title`, `url`, `requiring_id`) VALUES ('1d3c34b8-4e8b-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'vi', 'key', 'des', 'title', NULL, '1d3c3224-4e8b-11e8-9c2d-fa7ae01bbebc'), ('1d3c3ab2-4e8b-11e8-9c2d-fa7ae01bbebc', NULL, NULL, NULL, NULL, 'admin', NULL, '2018-04-27 00:00:00', NULL, 'MTA', 'MTA', '1', 'en', 'key', 'des', 'title', NULL, '1d3c3224-4e8b-11e8-9c2d-fa7ae01bbebc');


ALTER TABLE `Content` CHANGE `type` `type` INT(11) NULL DEFAULT NULL COMMENT '2: is top slider mon ngon,NULL is normal';


ALTER TABLE `Content` ADD `image_large` TEXT NULL AFTER `image`;

