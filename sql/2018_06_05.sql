ALTER TABLE `PromotionDetail` ADD `already_used_sign_up` INT(11)  DEFAULT 0;

INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'promotion_for_user_signup_first_time', '1');

UPDATE `Config` SET `value` = '<p><img src="cid:Picture1" style="width:100%" /></p>

<h2>Tạo t&agrave;i khoản th&agrave;nh c&ocirc;ng</h2>

<div style="background:#dfdfdf; font-size:20px; height:200px; margin-top:110px; padding-left:30px; padding-right:350px; padding-top:45px; width:80%">&lt;CLIENT_NAME&gt; th&acirc;n mến,<br />
Ch&agrave;o mừng bạn đến với <strong>Opera Mart!</strong><br />
L&agrave; th&agrave;nh vi&ecirc;n của <span style="color:#e10000">Opera Mart</span> bạn sẽ l&agrave; người đầu ti&ecirc;n nhận được th&ocirc;ng tin những chương tr&igrave;nh khuyến m&atilde;i độc quyền v&agrave; những sản phẩm mới nhất. Đến với Opera Mart để:

<ul>
	<li>Mua sắm những sản phẩm gia vị, thực phẩm với gi&aacute; rẻ nhất.</li>
	<li>Mua được những chương tr&igrave;nh khuyến m&atilde;i giảm gi&aacute; đặc biệt chỉ d&agrave;nh ri&ecirc;ng cho th&agrave;nh vi&ecirc;n của Opera Mart.</li>
</ul>

<p style="text-align:center">M&atilde; giảm gi&aacute; d&agrave;nh cho th&agrave;nh vi&ecirc;n mới</p>

<p style="text-align:center">&lt;MA_GIAM_GIA&gt;</p>

<p>&nbsp;</p>
</div>

<div style="background:#dfdfdf; font-size:20px; text-align:center; width:100%">&nbsp;</div>

<div style="background:#dfdfdf; font-size:20px; height:40px; text-align:center; width:100%">&nbsp;</div>

<div style="background:#dfdfdf; font-size:20px; text-align:center; width:100%">&nbsp;</div>

<div style="background:#dfdfdf; font-size:20px; height:40px; text-align:center; width:100%">&nbsp;</div>
' WHERE `Config`.`name` = 'content_subject_editor';