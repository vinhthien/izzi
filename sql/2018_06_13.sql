DELETE FROM `config` WHERE `name` = 'email_accept_for_host' OR `name` = 'password_accept_for_host';

ALTER TABLE `warehousehistory` CHANGE `type` `type` INT(11) NULL DEFAULT NULL COMMENT '1: Nhập Xuất Kho Trên Giao Dien Backend; 2: Nhập Xuất kho qua file Excel; 3: Đơn hàng đã xuất kho / huỷ đơn hàng trả lại kho';