INSERT INTO `content` (`id`, `entity_code`, `created_date`, `created_by`, `updated_by`, `updated_date`, `title`, `tags`, `content_category_id`, `description`, `contentHTML`, `type`, `status`, `sort_order`, `rating`, `image`, `image_large`, `countview`, `link`) VALUES
('a1566e68-09d3-11e9-ab14-d663bd873d93', 'BRF', '2018-04-12 00:00:00', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nuoc_mam_phan_thiet.jpg', NULL, NULL, NULL);

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES
('e462320e-09d9-11e9-ab14-d663bd873d93', 'Sửa', NULL, 'Cập nhật banner chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL),
('e462352e-09d9-11e9-ab14-d663bd873d93', 'Xem', NULL, 'Cập nhật banner chi nhánh', 'Quản lý chi nhánh (để sau)', NULL, NULL, NULL, NULL, NULL);

UPDATE `permission` SET `direct_route` = 'chinh-sua-banner-chi-nhanh', `sort_order` = '11' WHERE `permission`.`id` = 'e462320e-09d9-11e9-ab14-d663bd873d93';

UPDATE `permission` SET `direct_route` = 'chinh-sua-banner-chi-nhanh', `sort_order` = '11' WHERE `permission`.`id` = 'e462352e-09d9-11e9-ab14-d663bd873d93';

INSERT INTO `permission` (`id`, `name`, `desciption`, `child_group`, `master_group`, `sort_order`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES
('a9d74900-0a59-11e9-ab14-d663bd873d93', 'Sửa', NULL, 'Thông tin truyền thông', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL),
('a9d74b94-0a59-11e9-ab14-d663bd873d93', 'Xem', NULL, 'Thông tin truyền thông', 'Quản lý footer', NULL, NULL, NULL, NULL, NULL);

UPDATE `permission` SET `direct_route` = 'quan-ly-thong-tin-truyen-thong', `sort_order` = '2' WHERE `permission`.`id` = 'a9d74900-0a59-11e9-ab14-d663bd873d93';

UPDATE `permission` SET `direct_route` = 'quan-ly-thong-tin-truyen-thong', `sort_order` = '2' WHERE `permission`.`id` = 'a9d74b94-0a59-11e9-ab14-d663bd873d93';

DELETE FROM `permission` WHERE id = '8326ee70-7e70-11e8-adc0-fa7ae01bbebc';

DELETE FROM `permission` WHERE id = 'b66b44b6-7e70-11e8-adc0-fa7ae01bbebc';

DELETE FROM `permission` WHERE `id` = '13f34d8a-7e81-11e8-adc0-fa7ae01bbebc';

DELETE FROM `permission` WHERE `id` = 'aa760b34-93e5-11e8-9eb6-529269fb1459';

DELETE FROM `permission` WHERE `id` = 'aa760742-93e5-11e8-9eb6-529269fb1459';
