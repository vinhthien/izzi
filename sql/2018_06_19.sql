INSERT INTO `role` (`id`, `name`, `description`, `created_date`,`created_by`, `updated_by`, `updated_date`)
VALUES ('783688d0-739f-11e8-adc0-fa7ae01bbebc', 'Administrator', 'Administrator',NOW(),'admin',null ,null );

INSERT INTO `role` (`id`, `name`, `description`, `created_date`,`created_by`, `updated_by`, `updated_date`)
VALUES ('78368c7c-739f-11e8-adc0-fa7ae01bbebc', 'Accounting', 'Accounting',NOW(),'admin',null ,null );

INSERT INTO `role` (`id`, `name`, `description`, `created_date`,`created_by`, `updated_by`, `updated_date`)
VALUES ('78368dc6-739f-11e8-adc0-fa7ae01bbebc', 'Supervisor', 'Supervisor',NOW(),'admin',null ,null );

INSERT INTO `role` (`id`, `name`, `description`, `created_date`,`created_by`, `updated_by`, `updated_date`)
VALUES ('78368ef2-739f-11e8-adc0-fa7ae01bbebc', 'Founder', 'Founder',NOW(),'admin',null ,null );


CREATE TABLE userrole (
    id varchar(50),
    role_id varchar(50),
    user_id varchar(50),
    created_date datetime ,
    created_by varchar(255),
    updated_by varchar(255),
    updated_date datetime
);