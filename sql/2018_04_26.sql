CREATE TABLE `Config` (
  `id` int(11) NOT NULL ,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL ,
  `value` varchar(300) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


ALTER TABLE `Config`
ADD PRIMARY KEY (`id`);

ALTER TABLE `Config`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `Config` (`id`, `name`, `value`) VALUES (NULL, 'spbc', '1');


CREATE TABLE `BestSellProduct` (
  `id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiring_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `requiring_entity_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `BestSellProduct`
ADD PRIMARY KEY (`id`);

