ALTER TABLE `User` CHANGE `socket_id` `socket_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;

ALTER TABLE `Person` CHANGE `birthday` `birthday` VARCHAR(50) NULL DEFAULT NULL;


INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df027a54-4223-11e8-842f-0ed5f89f718b', 'Hồ Chí Minh', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, NULL, 'TTP', 'TTP', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02a498-4223-11e8-842f-0ed5f89f718b', 'Quận 1', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028012-4223-11e8-842f-0ed5f89f718b', 'Quận 12', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df0281d4-4223-11e8-842f-0ed5f89f718b', 'Quận Thủ Đức', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02831e-4223-11e8-842f-0ed5f89f718b', 'Quận 9', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028440-4223-11e8-842f-0ed5f89f718b', 'Quận Gò Vấp', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02879c-4223-11e8-842f-0ed5f89f718b', 'Quận Bình Thạnh', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df0288e6-4223-11e8-842f-0ed5f89f718b', 'Quận Tân Bình', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028a1c-4223-11e8-842f-0ed5f89f718b', 'Quận Tân Phú', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028b48-4223-11e8-842f-0ed5f89f718b', 'Quận Phú Nhuận', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028c60-4223-11e8-842f-0ed5f89f718b', 'Quận 2', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028e18-4223-11e8-842f-0ed5f89f718b', 'Quận 3', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df028fc6-4223-11e8-842f-0ed5f89f718b', 'Quận 10', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02917e-4223-11e8-842f-0ed5f89f718b', 'Quận 11', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029336-4223-11e8-842f-0ed5f89f718b', 'Quận 4', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df0294d0-4223-11e8-842f-0ed5f89f718b', 'Quận 5', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df0295fc-4223-11e8-842f-0ed5f89f718b', 'Quận 6', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029854-4223-11e8-842f-0ed5f89f718b', 'Quận 8', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029980-4223-11e8-842f-0ed5f89f718b', 'Quận Bình Tân', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029c0a-4223-11e8-842f-0ed5f89f718b', 'Quận 7', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029d40-4223-11e8-842f-0ed5f89f718b', 'Huyện Củ Chi', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029e6c-4223-11e8-842f-0ed5f89f718b', 'Huyện Hóc Môn', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df029f98-4223-11e8-842f-0ed5f89f718b', 'Huyện Bình Chánh', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02a16e-4223-11e8-842f-0ed5f89f718b', 'Huyện Nhà Bè', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

INSERT INTO `LookupData` (`id`, `title`, `description`, `contentHTML`, `created_date`, `created_by`, `updated_date`, `updated_by`, `parent_id`, `entity_code`, `requiring_entity_code`, `status`) VALUES ('df02a29a-4223-11e8-842f-0ed5f89f718b', 'Huyện Cần Giờ', NULL, NULL, '2018-04-17 00:00:00', '719AD10A-04F9-4FD8-9351-1B2602277967', NULL, NULL, 'df027a54-4223-11e8-842f-0ed5f89f718b', 'QUH', 'QUH', '1');

