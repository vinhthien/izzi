ALTER TABLE `PurchaseOrder` CHANGE `price_coupon` `price_coupon` DECIMAL(18,2) NULL DEFAULT NULL COMMENT 'coupon_fixed_price_promotion';

ALTER TABLE `PurchaseOrder` CHANGE `percent_promotion` `percent_promotion` DECIMAL(18,2) NULL DEFAULT NULL COMMENT 'coupon_percentage_promotion';

ALTER TABLE `PurchaseOrder` ADD `coupon_promotion_name` TEXT NOT NULL AFTER `code_coupon`;

ALTER TABLE `PurchaseOrder` ADD `coupon_promotion_id` VARCHAR(50) NOT NULL AFTER `coupon_promotion_name`;